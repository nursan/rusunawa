<?php
	ob_start();
	include '../../config/koneksi.php';
	$tahun=$_GET['tahun'];
	$status=$_GET['status'];
?>
<html>
<table style="text-align: center; width: 100%;" border="0" cellpadding="2" cellspacing="2">
<title>Laporan Anggota</title>
  <tbody>
    <tr>
      <td style="vertical-align: top; width: 170px; text-align: center;"><img style="width: 80px; height: 100px;" src="../../images/lojo2.png"></td>
      <td style="vertical-align: top; width: 625px;">
		<div style="text-align: center;"><h1>KEMENTERIAN AGAMA<br></H1><H2> UNIVERSITAS ISLAM NEGERI ALAUDDIN MAKASSAR<br></h2><h2>P2B UNIT ASRAMA MAHASISWA<br></h2><h3> Kampus II Jl. HM. Yasin Limpo No. 36 Romangpolong-Gowa Telp. (0411) 5068236</h3></div>
      </td>
      <td style="vertical-align: top; width: 170px;"><br>
      </td>
    </tr>
	<tr>
		<td colspan=3><hr></td>
	</tr>
  </tbody>
</table>
<h3 align="center">LAPORAN DATA ANGGOTA</h3>
				<table style="text-align: center; width: 100%;border-collapse: collapse;font-size: 9px;" cellpadding="8" cellspacing="2" border=1>
					<thead>
						<tr>
							<th>NIM</th>
							<th>NAMA </th>
							<th>TTL</th>
							<th>ALAMAT</th>
							<th>J KELAMIN</th>
							<th>FAKULTAS</th>
							<th>JURUSAN</th>
							<th>HP</th>
						</tr>
					</thead>
					<tbody>
					<?php
						$query=mysqli_query($conn,"SELECT * FROM pendaftaran  WHERE YEAR(tgl_pendaftaran)='$tahun' AND status='$status' ORDER BY id_pendaftaran");
						while ($r=mysqli_fetch_array($query)){
					?>
						<tr>
							<td><?=$r['nim'];?></td>
							<td><?=$r['nama'];?></td>
							<td><?=$r['tgl_lahir'];?></td>
							<td><?=$r['alamat'];?></td>
							<td><?=$r['jenis_kelamin'];?></td>
							<td><?=$r['fakultas'];?></td>
							<td><?=$r['jurusan'];?></td>
							<td><?=$r['telp'];?></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
<br><br><br><br><Br>
				<table style="margin-left: 500px;">
				<tr>
				<td>Menyetujui, </td>
				</tr>
				<tr>
				<td>Gowa, <?php echo date("d M Y"); ?></td>
				</tr>
				<tr>
				<td><br><br><br><br>UINAM DEV</td>
				</tr>
				</table>
</html>

<?php
	error_reporting(0);
	$out = ob_get_contents();
	ob_end_clean();
	include("../../print_layout/mpdf/mpdf.php");
	$mpdf = new mPDF('c','A4','');
	$mpdf->SetDisplayMode('fullpage');
	$stylesheet = file_get_contents('../../print_layout/mpdf/mpdf.css');
	$mpdf->WriteHTML($stylesheet,1);
	$mpdf->WriteHTML($out);
	$mpdf->Output();
?>
