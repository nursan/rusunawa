<?php
	ob_start();
	include '../../config/koneksi.php';
	$tahun=$_GET['tahun'];
	$bulan=$_GET['bulan'];
?>
<html>
<table style="text-align: center; width: 100%;" border="0" cellpadding="2" cellspacing="2">
<title>Laporan Keuangan</title>
  <tbody>
    <tr>
      <td style="vertical-align: top; width: 170px; text-align: center;"><img style="width: 80px; height: 100px;" src="../../images/lojo2.png"></td>
      <td style="vertical-align: top; width: 625px;">
		<div style="text-align: center;"><h1>KEMENTERIAN AGAMA<br></H1><H2> UNIVERSITAS ISLAM NEGERI ALAUDDIN MAKASSAR<br></h2><h2>P2B UNIT ASRAMA MAHASISWA<br></h2><h3> Kampus II Jl. HM. Yasin Limpo No. 36 Romangpolong-Gowa Telp. (0411) 5068236</h3></div>
      </td>
      <td style="vertical-align: top; width: 170px;"><br>
      </td>
    </tr>
	<tr>
		<td colspan=3><hr></td>
	</tr>
  </tbody>
</table>
<h3 align="center">LAPORAN DATA PEMBAYARAN</h3>
				<table style="text-align: center; width: 100%;border-collapse: collapse;font-size: 9px;" cellpadding="8" cellspacing="2" border=1>
					<thead>
						<tr>
							<th>Nim</th>
          <th>Nama</th>
          <th>Tanggal Transaksi</th>
		  <th>Jumlah Kontrak/bln</th>
          <th>Jumlah Bayar</th>
						</tr>
					</thead>
					<tbody>
					<?php
						$query=mysqli_query($conn,"SELECT
a.*,b.nama
FROM `pembayaran` a
INNER JOIN
pendaftaran b
ON a.id_mahasiswa=b.nim
WHERE month(a.tanggal_transaksi)=$bulan AND year(a.tanggal_transaksi)=$tahun");
						while ($r=mysqli_fetch_array($query)){
					?>
						<tr>

							<td><?=$r['id_mahasiswa'];?></td>
							<td><?=$r['nama'];?></td>
							<td><?=$r['tanggal_transaksi'];?></td>
							<td><?=$r['jumlah_bulan'];?></td>
							<td><?=$r['jumlah_bayar'];?></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
<br><br><br><br><Br>
				<table style="margin-left: 500px;">
				<tr>
				<td>Menyetujui, </td>
				</tr>
				<tr>
				<td>Gowa, <?php echo date("d M Y"); ?></td>
				</tr>
				<tr>
				<td><br><br><br><br>UINAM DEV</td>
				</tr>
				</table>
</html>

<?php
error_reporting(0);

	$out = ob_get_contents();
	ob_end_clean();
	include("../../print_layout/mpdf/mpdf.php");
	$mpdf = new mPDF('c','A4','');
	$mpdf->SetDisplayMode('fullpage');
	$stylesheet = file_get_contents('../../print_layout/mpdf/mpdf.css');
	$mpdf->WriteHTML($stylesheet,1);
	$mpdf->WriteHTML($out);
	$mpdf->Output();
?>
