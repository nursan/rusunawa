<?php
define('FPDF_FONTPATH', 'font/');
require('fpdf.php');

include "../config/koneksi.php";
include "../config/library.php";

$sql = mysqli_query($conn,"
SELECT a.*,b.jurusan as jurusan_,c.fakultas as fakultas_ FROM `pendaftaran` a
INNER JOIN tbl_jurusan b
ON a.jurusan=b.id_jurusan
INNER JOIN tbl_fakultas c
ON a.fakultas=c.id_fakultas
WHERE a.id_pendaftaran='$_GET[no_pendaftaran]'");

$i=0;
while($r=mysqli_fetch_array($sql))
{
$lahir   = tgl_indo($r['tgl_lahir']);

$pdf=new FPDF('P','mm','A5');
$pdf->AddPage();
$pdf->SetFont('Arial','',9);
//ambil Gambar Header
//$pdf->Image("../images/banner-300x250.jpg", 0, 0, '2', 'left');
//Judul Laporan PDF
$pdf->SetFont('Arial','B','11');
$pdf->Cell(130,0,'KARTU TES UJIAN ',0,0,'C');


$pdf->Ln(3);
$pdf->SetFont('Arial','',7);
$pdf->Cell(50,10,'No Induk Mahasiswa',1,0,'L');
$pdf->Cell(4,10,':',1,0,'C');
$pdf->Cell(70,10,$r['nim'],1,0,'L');
$pdf->Ln();
$pdf->Cell(50,10,'Nama',1,0,'L');
$pdf->Cell(4,10,':',1,0,'C');
$pdf->Cell(70,10,$r['nama'],1,0,'L');

$pdf->Ln();
$pdf->Cell(50,10,'Tempat Lahir',1,0,'L');
$pdf->Cell(4,10,':',1,0,'C');
$pdf->Cell(70,10,$r['tempat'].', '.$lahir,1,0,'L');


$pdf->Ln();
$pdf->Cell(50,10,'Jenis Kelamin',1,0,'L');
$pdf->Cell(4,10,':',1,0,'C');
$pdf->Cell(70,10,$r['jenis_kelamin'],1,0,'L');


$pdf->Ln();
$pdf->Cell(50,10,'Fakultas',1,0,'L');
$pdf->Cell(4,10,':',1,0,'C');
$pdf->Cell(70,10,$r['fakultas_'],1,0,'L');

$pdf->Ln();
$pdf->Cell(50,10,'Jurusan',1,0,'L');
$pdf->Cell(4,10,':',1,0,'C');
$pdf->Cell(70,10,$r['jurusan_'],1,0,'L');

$pdf->Ln();
$pdf->Cell(50,10,'No Telp',1,0,'L');
$pdf->Cell(4,10,':',1,0,'C');
$pdf->Cell(70,10,$r['telp'],1,0,'L');

$pdf->SetFont('Arial','B','6');
$pdf->Ln();
$pdf->Cell(70,11,"Note : ",0,0,'L');

$pdf->SetFont('Arial','B','6');
$pdf->Ln(3);
$pdf->Cell(70,11," *Ini adalah Bukti Pendaftaran yang akan digunakan untuk melaksanakan tes masuk rusunawa UINAM",0,0,'L');

$pdf->SetFont('Arial','B','6');
$pdf->Ln(3);
$pdf->Cell(70,11," *Pelaksanan Tes akan diumumkan di website rusunawauiam.com",0,0,'L');

$pdf->SetFont('Arial','B','6');
$pdf->Ln(3);
$pdf->Cell(70,11," *Jika ingin informasi tambahan, Hub : 085241511600 ",0,0,'L');

$pdf->SetFont('Arial','',7);
$pdf->Ln(9);
$pdf->Text(10,200,'Dicetak pada tanggal: ' . date( 'd-m-Y, H:i:s'),1,0,'L');
}


$pdf->Output();
?>
