<?php
session_start();
if (empty($_SESSION['username']) AND empty($_SESSION['passuser']))
{
  echo "<link href='style.css' rel='stylesheet' type='text/css'><center>Untuk mengakses modul, Anda harus login <br>";
  echo "<a href=../../index.php><b>LOGIN</b></a></center>";
}
else
{
  include "../../../config/koneksi.php";
  include "../../../config/library.php";
  include "../../../config/fungsi_thumb.php";
  include "../../../config/fungsi_seo.php";
  $module=$_GET['module'];
  $act=$_GET['act'];

// Hapus gedung
  if ($module=='halamanstatis' AND $act=='hapus')
  {
    $data=mysqli_fetch_array(mysqli_query($conn,"SELECT foto FROM fasilitas WHERE id='$_GET[id]'"));
    if ($data[foto]!='')
    {
       mysqli_query($conn,"DELETE FROM fasilitas WHERE id ='$_GET[id]'");
       unlink("../../../foto_statis/$_GET[namafile]");
       unlink("../../../foto_statis/small_$_GET[namafile]");
    }
    else
    {
       mysqli_query($conn,"DELETE FROM fasilitas WHERE id ='$_GET[id]'");
    }
    header('location:../../media.php?module='.$module);
  }


// Input gedung
  elseif ($module=='gedung' AND $act=='input')
  {
    $lokasi_file    = $_FILES['fupload']['tmp_name'];
    $tipe_file      = $_FILES['fupload']['type'];
    $nama_file      = $_FILES['fupload']['name'];
    $acak           = rand(1,99);
    $nama_file_unik = $acak.$nama_file;
    $judul_seo      = seo_title($_POST['nama']);

  // Apabila ada gambar yang diupload
    if (!empty($lokasi_file))
    {
      UploadStatis($nama_file_unik);
      mysqli_query($conn,"INSERT INTO fasilitas VALUES('','$_POST[jenis]','$_POST[nama]','$_POST[jumlah]','$_POST[lokasi]','$_POST[deskripsi]','$nama_file_unik')") or die(mysqli_error());
      header('location:../../media.php?module='.$module);
    }
    else {
      echo "string";
    }
  }
// Update halamanstatis
elseif ($module=='halamanstatis' AND $act=='update'){
  $lokasi_file    = $_FILES['fupload']['tmp_name'];
  $tipe_file      = $_FILES['fupload']['type'];
  $nama_file      = $_FILES['fupload']['name'];
  $acak           = rand(1,99);
  $nama_file_unik = $acak.$nama_file;

  $judul_seo      = seo_title($_POST[judul]);

  // Apabila gambar tidak diganti
  if (empty($lokasi_file)){
    mysqli_query($conn,"UPDATE halamanstatis SET judul        = '$_POST[judul]',
                                        judul_seo    = '$judul_seo',
                                        isi_halaman  = '$_POST[isi_halaman]'
                                  WHERE id_halaman   = '$_POST[id]'");
  header('location:../../media.php?module='.$module);
  }
  else{
   $data_gambar = mysqli_query($conn,"SELECT gambar FROM halamanstatis WHERE id_halaman='$_POST[id]'");
	$r    	= mysqli_fetch_array($data_gambar);
	@unlink('../../../foto_statis/'.$r['gambar']);
	@unlink('../../../foto_statis/'.'small_'.$r['gambar']);
    UploadStatis($nama_file_unik ,'../../../foto_statis/');
    mysqli_query($conn,"UPDATE halamanstatis SET judul        = '$_POST[judul]',
                                          judul_seo    = '$judul_seo',
                                          isi_halaman  = '$_POST[isi_halaman]',
                                          gambar       = '$nama_file_unik'
                                    WHERE id_halaman   = '$_POST[id]'");
    header('location:../../media.php?module='.$module);
  }
}
}
?>
