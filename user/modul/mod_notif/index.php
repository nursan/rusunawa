<div class='rightpanel'>

    <ul class='breadcrumbs'>
        <li>
            <a href='contributor.html'>
                <i class='iconfa-home'></i>
            </a>
            <span class='separator'></span>
        </li>
        <li>
            <a href='asrama.html'>asrama</a>
            <span class='separator'></span>
        </li>
        <li>Tambah asrama</li>


    </ul>

    <div class='pageheader'>

        <div class='pageicon'>
            <span class='iconfa-pencil'></span>
        </div>
        <div class='pagetitle'>
            <h5>Forms</h5>
            <h1>Kirim notif</h1>
        </div>
    </div>
    <!--pageheader-->

    <div class='maincontent'>
        <div class='maincontentinner'>
            <div class='widgetbox box-inverse'>
                <h4 class='widgettitle'>Form Bordered</h4>
                <div class='widgetcontent nopadding'>
                    <div id="status">
                    </div>
                    <form class='stdform stdform2' id="formNotif" enctype='multipart/form-data'>


                        <p>
                            <label>Kirim Berdasarkan</label>
                            <span class='field'>
                                <input type="radio" onchange="activateSelect()" name="filter" value="all">Semua
                                <input type="radio" onchange="activateSelect()" name="filter" value="dorm">Asrama
                                <input type="radio" onchange="activateSelect()" name="filter" value="room">Kamar
                                <input type="radio" onchange="activateSelect()" name="filter" value="person">Perorangan
                            </span>
                        </p>
                        <p>
                            <label>Asrama</label>
                            <span class='field'>
                                <select id="asrama" onchange="fillKamar()" disabled name="asrama"></select>
                            </span>
                        </p>
                        <p>
                            <label>Kamar</label>
                            <span class='field'>
                                <select id="kamar" onchange="fillAnggota()" disabled name="kamar"></select>
                            </span>
                        </p>
                        <p>
                            <label>Anggota</label>
                            <span class='field'>
                                <select id="person" disabled name="person"></select>
                            </span>
                        </p>
                        <p>
                            <label>Judul</label>
                            <span class='field'>
                                <input type="text" name="title" id="title">
                            </span>
                        </p>
                        <p>
                            <label>Pesan</label>
                            <span class='field'>
                                <textarea  name="msg" id="msg"></textarea>
                            </span>
                        </p>
                        <p class='stdformbutton'>
                            <input type="button" onclick="sendNotif()" class='btn btn-primary' value="Kirim"> |
                            <input type=button value=Batal onclick=self.history.back() class='btn btn-warning btn-rounded'>
                        </p>
                    </form>
                </div>
                <!--widgetcontent-->
            </div>
            <!--widget-->";
            <script type="text/javascript" src="./js/jquery.min.js"></script>
            
            <script>
                var asrama;
                $(document).ready(() => {
                    $('#msg').show();
                    $('#mce_editor_0_parent').remove();
                    $("#formNotif").submit((e) => {
                        return false;
                    })
                })
                function activateSelect(){
                    let filter = $('input[name="filter"]:checked').val();
                    switch (filter) {
                        case 'dorm':
                                fillDorm();
                                $("#asrama").attr('disabled',false);
                                $("#kamar").attr('disabled', true);
                                $("#person").attr('disabled',true);
                            break;
                        case 'person':
                            fillDorm();
                            fillKamar();                            
                            $("#asrama").attr('disabled', false);
                            $("#kamar").attr('disabled', false);
                            $("#person").attr('disabled', false);
                            break;
                        case 'room':
                                fillDorm(); 
                                fillKamar();                       
                            $("#asrama").attr('disabled', false);
                            $("#kamar").attr('disabled', false);
                            $("#person").attr('disabled', true);
                            break;
                        default:
                             $("#asrama").attr('disabled', true);
                            $("#kamar").attr('disabled', true);
                            $("#person").attr('disabled', true);
                            break;
                    }
                }

                function fillDorm() {
                    $.get("../appapi/index.php?/ajax/asrama",(data) => {
                        let option = '';
                        data.forEach((data,key) => {
                            option += '<option value="'+key+'" >'+data.nama_asrama+'</option>'
                        })
                        $("#asrama").html(option);
                        asrama = data;
                        

                    })   
                }
                function fillKamar(){
                     $.ajax({
                        url: "../appapi/index.php?/ajax/kamar",
                        type: "POST",
                        data: JSON.stringify({ asrama: asrama[$("#asrama").val()].id_asrama })
                    }).done(data => {
                        let option = '';
                        data.forEach(data => {
                            option += '<option value="'+data.no_kamar +'" >' + data.no_kamar  +'</option>'
                        })
                       $("#kamar").html(option);
                    })
                }

                function fillAnggota() {
                    
                    $.ajax({
                        url: "../appapi/index.php?/ajax/anggota",
                        type: "POST",
                        data: JSON.stringify({ asrama: asrama[$("#asrama").val()].id_asrama, kamar: $("#kamar").val() })
                    }).done(data => {
                        let option = '';
                        data.forEach(data => {
                            option += '<option value="'+data.id_pendaftar+'" >' + data.id_pendaftar +' - '+data.nama+'</option>'
                        })
                        $("#person").html(option)
                    })
                }
                function sendNotif(e) {
                        $("#status").html('<div class="alert alert-info" role="alert"><strong>Loading</strong>Mohon Tunggu</div>')                    
                    let data = {
                        filter: $('input[name="filter"]:checked').val(),
                        asrama : (asrama == null) ? null : asrama[$("#asrama").val()].id_asrama,
                        kamar: $("#kamar").val(),
                        person : $('#person').val(),
                        title : $('#title').val(),
                        msg : $('#msg').val()
                    }
                     $.ajax({
                        url: "../appapi/index.php?/ajax/sendNotif",
                        type: "POST",
                        data: JSON.stringify(data)
                    }).done(data => {
                        $("#status").html('<div class="alert alert-success" role="alert"><strong>Sukses</strong>Pesan terkirim</div>');
                        setTimeout(() => {
                            location.reload();
                        },3000)
                    })
                }
            </script>
            <?php include "footer.php"; ?>

        </div>
        <!--maincontentinner-->
    </div>
    <!--maincontent-->

</div>
<!--rightpanel-->";