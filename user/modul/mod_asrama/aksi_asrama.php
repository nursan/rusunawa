<?php
session_start();
$image_path = "../../../logo/imagomedia.png";
$font_path = "RIZAL.TTF";
$font_size = 14;       // in pixcels
//$water_mark_text_1 = "9";
$water_mark_text_2 = "SwaraKalibata";

function watermark_image($oldimage_name){
    global $image_path;
    list($owidth,$oheight) = getimagesize($oldimage_name);
    $width = $owidth;
	$height = $oheight;
    $im = imagecreatetruecolor($width, $height);
    $img_src = imagecreatefromjpeg($oldimage_name);
    imagecopyresampled($im, $img_src, 0, 0, 0, 0, $width, $height, $owidth, $oheight);
    $watermark = imagecreatefrompng($image_path);
    list($w_width, $w_height) = getimagesize($image_path);
     $pos_x = $width - $w_width -5;
    $pos_y = $height - $w_height - 5;
    imagecopy($im, $watermark, $pos_x, $pos_y, 0, 0, $w_width, $w_height);
    imagejpeg($im, $oldimage_name, 100);
    imagedestroy($im);
	return true;
}

if (empty($_SESSION['username']) AND empty($_SESSION['passuser'])){

  echo "<link href='../../css/zalstyle.css' rel='stylesheet' type='text/css'>
  <link rel='shortcut icon' href='../../favicon.png' />

  <body class='special-page'>
  <div id='container'>
  <section id='error-number'>
  <img src='../../img/lock.png'>
  <h1>MODUL TIDAK DAPAT DIAKSES</h1>
  <p><span class style=\"font-size:14px; color:#ccc;\">Untuk mengakses modul, Anda harus login dahulu!</p></span><br/>
  </section>
  <section id='error-text'>
  <p><a class='button' href='../../index.php'> <b>LOGIN DI SINI</b> </a></p>
  </section>
  </div>";}

else{
include "../../../config/koneksi.php";
include "../../../config/library.php";
include "../../../config/fungsi_thumb.php";
include "../../../config/fungsi_seo.php";

$module=$_GET['module'];
$act=$_GET['act'];

// Hapus asrama
if ($module=='asrama' AND $act=='hapus'){
     mysqli_query($conn,"DELETE FROM asrama WHERE id_asrama='$_GET[id]'");
  header('location:../../media.php?module='.$module);
}


// Input asrama
elseif ($module=='asrama' AND $act=='input'){
   $judul_seo      = seo_title($_POST['asrama']);

   mysqli_query($conn,"INSERT INTO asrama VALUES('',
								'$_POST[pembina]',
							  '$_POST[asrama]',
							  $_POST[lantai],
                                   $_POST[kamar],
                                   $_POST[ranjang],
								   '$_POST[fasilitas]',
								   '$_POST[gender]',
								   '$_POST[lokasi]',
                                   '$judul_seo')");

	header('location:../../media.php?module='.$module);

}

elseif ($module=='asrama' AND $act=='kamar'){
  //  $judul_seo      = seo_title($_POST['asrama']);
   mysqli_query($conn,"INSERT INTO `kamar` (`no_kamar`, `id_asrama`) VALUES ('".$_POST['no_kamar']."', '".$_POST['id_asrama']."');");

	header('location:../../media.php?module='.$module);

}

// Update asrama
elseif ($module=='asrama' AND $act=='update'){

  $judul_seo      = seo_title($_POST[asrama]);

    mysqli_query($conn,"UPDATE asrama  SET pembina       = '$_POST[pembina]',
	                                nama_asrama  = '$_POST[asrama]',
							         jumlah_lantai   = '$_POST[lantai]',
                                   jumlah_kamar   = '$_POST[kamar]',
                                 jumlah_ranjang   = '$_POST[ranjang]',
                                   fasilitas    = '$_POST[fasilitas]',
								     gender     = '$_POST[gender]',
							  lokasi    = '$_POST[lokasi]',
								     seo_asrama     = '$judul_seo'
                             WHERE id_asrama   = '$_POST[id]'");
  header('location:../../media.php?module='.$module);


}
}
?>
