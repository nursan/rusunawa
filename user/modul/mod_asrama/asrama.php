<script>
function confirmdelete(delUrl) {
   if (confirm("Anda yakin ingin menghapus?")) {
      document.location = delUrl;
   }
}
</script>
<?php
function GetCheckboxes($table, $key, $Label, $Nilai='') {
  $s = "select * from $table order by nama_tag";
  $r = mysqli_query($conn,$s);
  $_arrNilai = explode(',', $Nilai);
  $str = '';
  while ($w = mysqli_fetch_array($r)) {
    $_ck = (array_search($w[$key], $_arrNilai) === false)? '' : 'checked';
    $str .= "<input type=checkbox name='".$key."[]' value='$w[$key]' $_ck>$w[$Label] ";
  }
  return $str;
}

$aksi="modul/mod_asrama/aksi_asrama.php";
switch($_GET[act]){
  // Tampil asrama
  default:
  echo"<div class='rightpanel'>

        <ul class='breadcrumbs'>
            <li><a href='contributor.html'><i class='iconfa-home'></i></a> <span class='separator'></span></li>

            <li>asrama</li>


        </ul>

        <div class='pageheader'>

            <div class='pageicon'><span class='iconfa-table'></span></div>
            <div class='pagetitle'>
                <h5>asrama</h5>
                <h1>Asrama</h1>
            </div>
        </div><!--pageheader-->

        <div class='maincontent'>
            <div class='maincontentinner'>


                <h4 class='widgettitle'>
				<a href='?module=asrama&act=tambahasrama' class='btn btn-warning btn-rounded'><i class='icon-plus icon-white'></i>Tambah Asrama</a>
				<a href='?module=asrama&act=tambahkamar' class='btn btn-warning btn-rounded'><i class='icon-plus icon-white'></i>Tambah Kamar</a>
				</h4>
            	<table id='dyntable' class='table table-bordered'>
                    <colgroup>
                        <col class='con0' style='align: center; width: 4%' />
                        <col class='con1' />
                        <col class='con0' />
                        <col class='con1' />
                        <col class='con0' />
                        <col class='con1' />
                    </colgroup>
                    <thead>
                        <tr>
                          	<th class='head0 nosort'>No</th>
                            <th class='head0'>Nama Asrama</th>
							<th class='head1'>Kategori</th>
                            <th class='head1'>Jumlah Penghuni</th>
							<th class='head1'>Aksi</th>

                        </tr>
                    </thead><tbody>";
					if ($_SESSION[leveluser]=='admin'){
    $tampil = mysqli_query($conn,"SELECT * FROM asrama");}


    else{
    $tampil=mysqli_query($conn,"SELECT * FROM asrama");}
						   $no = 1;
   							 while($r=mysqli_fetch_array($tampil)){
     					 $tgl_posting=tgl_indo($r[tanggal]);

                    echo"<tr class='gradeX'>
                                                    <td class='aligncenter'><span class='center'>
                            <input type='checkbox' />
                          </span></td>
                			<td width='270'>$r[nama_asrama]</td>
							<td>$r[gender]</td>
							<td> $r[klik]</td>
							<td>
							<a href='?module=asrama&act=editasrama&id=$r[id_asrama]' class='btn btn-primary btn-circle'><i class='iconfa-search'></i></a>
							<a href='?module=asrama&act=editasrama&id=$r[id_asrama]' class='btn btn-info btn-circle'><i class='iconfa-pencil'></i></a>
							<a href=javascript:confirmdelete('$aksi?module=asrama&act=hapus&id=$r[id_asrama]') class='btn btn-danger btn-circle'><span class='iconfa-remove'></span></a>
							</td>
                        </tr>";
					}
                echo"</tbody></table>";


                include "footer.php";
                echo"

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->";

    break;
    case "tambahkamar":
        echo"<div class='rightpanel'>

        <ul class='breadcrumbs'>
            <li><a href='contributor.html'><i class='iconfa-home'></i></a> <span class='separator'></span></li>
            <li><a href='asrama.html'>asrama</a> <span class='separator'></span></li>
             <li>Tambah asrama</li>


        </ul>

        <div class='pageheader'>

            <div class='pageicon'><span class='iconfa-pencil'></span></div>
            <div class='pagetitle'>
                <h5>Forms</h5>
                <h1>Tambah Kamar</h1>
            </div>
        </div><!--pageheader-->

        <div class='maincontent'>
            <div class='maincontentinner'>
               <div class='widgetbox box-inverse'>
                <h4 class='widgettitle'>Form Bordered</h4>
                <div class='widgetcontent nopadding'>
				<form class='stdform stdform2' method=POST action='$aksi?module=asrama&act=kamar' enctype='multipart/form-data'>

							<p>
                                <label>No kamar</label>
                                <span class='field'><input type='number' name='no_kamar' id='firstname2' class='input-xxlarge' /></span>
                            </p>

							<p>
                                <label>Asrama</label>
								<span class='field'><select id='selection2' name='id_asrama' class='uniformselect'>";
                               $list = mysqli_query($conn,"SELECT * FROM asrama");

                                while ( $data = mysqli_fetch_array($list)) {
                                    
                                    echo"<option value='".$data['id_asrama']."'>".$data['nama_asrama']."</option>";
                                }


								echo "</select></p>";
   echo "
							<p class='stdformbutton'>
                                <button class='btn btn-primary'>Simpan</button> |
								<input type=button value=Batal onclick=self.history.back() class='btn btn-warning btn-rounded'>
                            </p>
                    </form>
                </div><!--widgetcontent-->
            </div><!--widget-->";


                include "footer.php";
                echo"

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->";
    break;
	case "tambahasrama":
	echo"<div class='rightpanel'>

        <ul class='breadcrumbs'>
            <li><a href='contributor.html'><i class='iconfa-home'></i></a> <span class='separator'></span></li>
            <li><a href='asrama.html'>asrama</a> <span class='separator'></span></li>
             <li>Tambah asrama</li>


        </ul>

        <div class='pageheader'>

            <div class='pageicon'><span class='iconfa-pencil'></span></div>
            <div class='pagetitle'>
                <h5>Forms</h5>
                <h1>Tambah Asrama</h1>
            </div>
        </div><!--pageheader-->

        <div class='maincontent'>
            <div class='maincontentinner'>
               <div class='widgetbox box-inverse'>
                <h4 class='widgettitle'>Form Bordered</h4>
                <div class='widgetcontent nopadding'>
				<form class='stdform stdform2' method=POST action='$aksi?module=asrama&act=input' enctype='multipart/form-data'>


							<p>
                                <label>Nama Asrama</label>
                                <span class='field'><input type='text' name='asrama' id='firstname2' class='input-xxlarge' /></span>
                            </p>

							<p>
                                <label>Pembina Asrama</label>
                                <span class='field'><input type='text' name='pembina' id='firstname2' class='input-xxlarge' /></span>
                            </p>

							<p>
                                <label>Select</label>
								<span class='field'><select name='gender' id='selection2' class='uniformselect'>";
   								echo"<option value=0 selected>Pilih Kategori Asrama</option>";
   								echo"<option value='Laki-laki'>Laki-laki</option>";
   								echo"<option value='Perempuan'>Perempuan</option>";

								echo "</select></p>
							<p>
                                <label>Jumlah Lantai</label>
                                <span class='field'><input type='text' name='lantai' id='firstname2' class='input' /></span>
                            </p>

							<p>
                                <label>Jumlah Kamar</label>
                                <span class='field'><input type='text' name='kamar' id='firstname2' class='input' /></span>
                            </p>

							<p>
                                <label>Jumlah Ranjang</label>
                                <span class='field'><input type='text' name='ranjang' id='firstname2' class='input' /></span>
                            </p>

							<p>
                                <label>Lokasi Asrama</label>
                                <span class='field'><input type='text' name='lokasi' id='firstname2' class='input-xxlarge' /></span>
                            </p>

							<p>
                                <label>Fasilitas</label>
                                <span class='field'><textarea name='fasilitas'  id='tinyeditor'  placeholder='Enter text ...' style='width: 750px; height: 400px'></textarea></span>
                            </p>
							";
   echo "
								</span>
                            </p>
							<p class='stdformbutton'>
                                <button class='btn btn-primary'>Simpan</button> |
								<input type=button value=Batal onclick=self.history.back() class='btn btn-warning btn-rounded'>
                            </p>
                    </form>
                </div><!--widgetcontent-->
            </div><!--widget-->";


                include "footer.php";
                echo"

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->";


    break;
	case "editasrama":
 $edit = mysqli_query($conn,"SELECT * FROM asrama WHERE id_asrama='$_GET[id]'");
    $r    = mysqli_fetch_array($edit);
   echo"<div class='rightpanel'>

        <ul class='breadcrumbs'>
            <li><a href='contributor.html'><i class='iconfa-home'></i></a> <span class='separator'></span></li>
            <li><a href='asrama.html'>asrama</a> <span class='separator'></span></li>
             <li>Edit asrama</li>


        </ul>

        <div class='pageheader'>

            <div class='pageicon'><span class='iconfa-pencil'></span></div>
            <div class='pagetitle'>
                <h5>Forms</h5>
                <h1>Edit asrama</h1>
            </div>
        </div><!--pageheader-->

        <div class='maincontent'>
            <div class='maincontentinner'>
               <div class='widgetbox box-inverse'>
                <h4 class='widgettitle'>Edit asrama</h4>
                <div class='widgetcontent nopadding'>
				<form class='stdform stdform2' method=POST action='$aksi?module=asrama&act=update' enctype='multipart/form-data'>
				<input type=hidden name=id value=$r[id_asrama]>
							<p>
                                <label>Nama Asrama</label>
                                <span class='field'><input type='text' value='$r[nama_asrama]' name='asrama' id='firstname2' class='input-xxlarge' /></span>
                            </p>

							<p>
                                <label>Pembina Asrama</label>
                                <span class='field'><input type='text' name='pembina' value=$r[pembina] id='firstname2' class='input-xxlarge' /></span>
                            </p>

							<p>
                                <label>Select</label>
								<span class='field'><select name='gender' id='selection2' class='uniformselect'>";
                $laki = '';
                $p = '';
                if ($r['gender'] == 'Laki-laki')
                {
                  $laki = "selected";
                  $p='';
                }
                else
                {
                  $laki = '';
                  $p="selected";
                }
   								echo"<option $laki value='Laki-laki'>Laki-laki</option>";
								echo"<option $p value='Perempuan'>Perempuan</option>";

								echo "</select></p>
							<p>
                                <label>Jumlah Lantai</label>
                                <span class='field'><input type='text' value='$r[jumlah_lantai]' name='lantai' id='firstname2' class='input' /></span>
                            </p>

							<p>
                                <label>Jumlah Kamar</label>
                                <span class='field'><input type='text' name='kamar' value='$r[jumlah_kamar]' id='firstname2' class='input' /></span>
                            </p>

							<p>
                                <label>Jumlah Ranjang</label>
                                <span class='field'><input type='text' name='ranjang' value='$r[jumlah_ranjang]' id='firstname2' class='input' /></span>
                            </p>

							<p>
                                <label>Lokasi Asrama</label>
                                <span class='field'><input type='text' name='lokasi' value='$r[lokasi]' id='firstname2' class='input-xxlarge' /></span>
                            </p>

							<p>
                                <label>Fasilitas</label>
                                <span class='field'><textarea name='fasilitas'  id='tinyeditor'  placeholder='Enter text ...' style='width: 750px; height: 400px'>$r[fasilitas]</textarea></span>
                            </p>
							";
   							echo "$d

							<p class='stdformbutton'>
                                <button class='btn btn-primary'>Update</button>
								<input type=button value=Batal onclick=self.history.back() class='btn btn-warning btn-rounded'>

                            </p>
                    </form>
                </div><!--widgetcontent-->
            </div><!--widget-->";


                include "footer.php";
                echo"

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->";

}
?>
