<?php
if($_SESSION[leveluser]=='admin')
{
?>
<div class="leftpanel">

        <div class="leftmenu">
            <ul class="nav nav-tabs nav-stacked">
            	<li class="nav-header">Navigation</li>
                <?php
				 //Menu Home
				if ($_GET['module']=='home'){
				echo"<li class='active'><a href='media.php?module=home'><span class='iconfa-laptop'></span> Dashboard</a></li>";
				}else{
				echo"<li><a href='media.php?module=home'><span class='iconfa-laptop'></span> Dashboard</a></li>";
				}


				//Menu Informasi
				 $module = $_GET['module'];
				 $a='berita';
				 $b='pengumuman';
				 $c='agenda';
				 $d='galerifoto';


				 if( ($module==$a || $module==$b || $module==$c || $module==$d) )
					{
					echo"<li class='dropdown active'><a href='#'><span class='iconfa-pencil'></span> Informasi </a>
                	 <ul style='display: block'>

                        <li class='active'><a href='media.php?module=berita'>Berita</a></li>
                        <li><a href='media.php?module=pengumuman'>Pengumuman</a></li>
                        <li><a href='media.php?module=agenda'>Agenda</a></li>
						<li><a href='media.php?module=galerifoto'>Gallery Photo</a></li>
                    </ul>
                	</li>";
				 }else
				 {
				 echo"<li class='dropdown'><a href='#'><span class='iconfa-pencil'></span> Informasi </a>
                	<ul>
                    	<li><a href='media.php?module=berita'>Berita</a></li>
                        <li><a href='media.php?module=pengumuman'>Pengumuman</a></li>
                        <li><a href='media.php?module=agenda'>Agenda</a></li>
						<li><a href='media.php?module=galerifoto'>Gallery Photo</a></li>

                    </ul>
					</li>";
					}

					//Menu PPDB
				 $module = $_GET['module'];
				 $a='asrama';
				 $b='anggota';

				 if( ($module==$a || $module==$b || $module==$c || $module==$d || $module==$e) )
					{
					echo"<li class='dropdown active'><a href='#'><span class='iconfa-book'></span> Manajement Asrama</a>
                	 <ul style='display: block'>

                        <li class='active'><a href='media.php?module=asrama'>Informasi Asrama</a></li>
						<li><a href='media.php?module=anggota'>Anggota</a></li>
                     </ul>
                	</li>";
				 }else
				 {
				 echo"<li class='dropdown'><a href='#'><span class='iconfa-book'></span> Manajement Asrama</a>
                	<ul>
                    	<li><a href='media.php?module=asrama'>Informasi Asrama</a></li>
						<li><a href='media.php?module=anggota'>Anggota</a></li>

                    </ul>
					</li>";
					}

				//Menu Keuangan
				 $module = $_GET['module'];
				 $a='pembayaran';
				 $b='databayar';



				 if( ($module==$a || $module==$b) )
					{
					echo"<li class='dropdown active'><a href='#'><span class='iconfa-book'></span> Manajement Keuangan</a>
                	 <ul style='display: block'>

                        <li class='active'><a href='media.php?module=pembayaran'>Pembayaran</a></li>
						<li><a href='media.php?module=databayar'>Data Pemabayaran</a></li>
                    </ul>
                	</li>";
				 }else
				 {
				 echo"<li class='dropdown'><a href='#'><span class='iconfa-book'></span> Manajement Keuangan</a>
                	<ul>
                    	<li><a href='media.php?module=pembayaran'>Pembayaran</a></li>
						<li><a href='media.php?module=databayar'>Data Pemabayaran</a></li>
                    </ul>
					</li>";
				}

				//Menu Fasilitas
				 $module = $_GET['module'];
				 $a='gedung';
				 $b='olahraga';
				 $c='asrama';

				 if( ($module==$a || $module==$b || $module==$c) )
					{
					echo"<li class='dropdown active'><a href='#'><span class='iconfa-book'></span> Fasilitas Asrama</a>
                	 <ul style='display: block'>

                        <li class='active'><a href='media.php?module=gedung'>Gedung</a></li>
						<li><a href='media.php?module=olahraga'>Olahraga</a></li>
						<li><a href='media.php?module=pendidikan'>Asrama</a></li>
                    </ul>
                	</li>";
				 }else
				 {
				 echo"<li class='dropdown'><a href='#'><span class='iconfa-book'></span> Fasilitas Asrama</a>
                	<ul>
                    	<li><a href='media.php?module=gedung'>Gedung</a></li>
						<li><a href='media.php?module=olahraga'>Olahraga</a></li>
						<li><a href='media.php?module=pendidikan'>Asrama</a></li>
                    </ul>
					</li>";
				}

				if($_GET['module'] == "notif"){
					echo "<li class='dropdown active'><a href='media.php?module=notif'><span class='iconfa-book'></span>Kirim Notifikasi</a>
						<ul>
						<li><a href='media.php?module=notif'>Kirim Notif</a></li>							
						</ul>
					</li>";

				}else{
					echo "<li class='dropdown'><a href='media.php?module=notif'><span class='iconfa-book'></span>Kirim Notifikasi</a>
						<ul>
						<li><a href='media.php?module=notif'>Kirim Notif</a></li>							
						</ul>
					</li>";

				}

				//Menu Pendaftaran
				if ($_GET['module']=='pendaftaran'){
				echo"<li class='active'><a href='media.php?module=pendaftaran'><span class=' iconfa-file'></span> Data Pendaftar</a></li>";
				}else{
				echo"<li><a href='media.php?module=pendaftaran'><span class=' iconfa-file'></span> Data Pendaftar</a></li>";
				}

				//Menu Hasil tes
				if ($_GET['module']=='hasil_tes'){
				echo"<li class='active'><a href='media.php?module=hasil_tes'><span class=' iconfa-file'></span> Hasil Tes</a></li>";
				}else{
				echo"<li><a href='media.php?module=hasil_tes'><span class=' iconfa-file'></span> Hasil Tes</a></li>";
				}

				//Menu tata cara pendaftaran
				if ($_GET['module']=='info')
        {
				      echo"<li class='active'><a href='media.php?module=info'><span class=' iconfa-file'></span> Tata Cara Pendaftar</a></li>";
        }
        else
        {
              echo"<li ><a href='media.php?module=info'><span class=' iconfa-file'></span> Tata Cara Pendaftar</a></li>";
				}
        if ($_GET['module'] == 'alumni')
        {
          echo"<li class='active'><a href='media.php?module=alumni'><span class=' iconfa-file'></span> Daftar Alumni</a></li>";
        }
        else {
          echo"<li><a href='media.php?module=alumni'><span class=' iconfa-file'></span> Daftar Alumni</a></li>";
        }
         if( ($module=='rasrama' || $module=='ranggota' || $module=='rkeuangan' ) )
					{
					echo"<li class='dropdown active'><a href='#'><span class='iconfa-book'></span> Laporan</a>
                	 <ul style='display: block'>

                        <li class='active'><a href='media.php?module=rasrama'> Asrama</a></li>
						<li><a href='media.php?module=ranggota'>Anggota</a></li>
						<li><a href='media.php?module=rkeuangan'>Keuangan</a></li>
                     </ul>
                	</li>";
				 }else
				 {
				 echo"<li class='dropdown'><a href='#'><span class='iconfa-book'></span> Laporan</a>
                	<ul>
                    	<li><a href='media.php?module=rasrama'> Asrama</a></li>
						<li><a href='media.php?module=ranggota'>Anggota</a></li>
						<li><a href='media.php?module=rkeuangan'>Keuangan</a></li>
                    </ul>
					</li>";
					}

				//Menu Halaman
				if ($_GET['module']=='halamanstatis'){
				echo"<li class='active'><a href='media.php?module=halamanstatis'><span class=' iconfa-bell'></span> Halaman</a></li>";
				}else{
				echo"<li><a href='media.php?module=halamanstatis'><span class=' iconfa-bell'></span> Halaman</a></li>";
				}
					//Menu Profil
				 /*$module = $_GET['module'];
				 $a='bukutamu';
				 $b='hubungi';

				 if( ($module==$a || $module==$b) )
					{
					echo"<li class='dropdown active'><a href='#'><span class='iconfa-retweet'></span> Interaksi</a>
                	 <ul style='display: block'>

                        <li class='active'><a href='media.php?module=bukutamu'>Buku Tamu</a></li>
                        <li><a href='media.php?module=hubungi'>Hubungi</a></li>

                    </ul>
                	</li>";
				 }else
				 {
				 echo"<li class='dropdown'><a href='#'><span class='iconfa-retweet'></span> Interaksi </a>
                	<ul>
                    	<li><a href='media.php?module=bukutamu'>Buku Tamu</a></li>
                        <li><a href='media.php?module=hubungi'>Hubungi</a></li>


                    </ul>
					</li>";
        }*/
					?>
                 <!--    //Menu Modul
				 $module = $_GET['module'];

				 $a='kategori';
				  if( ($module==$a ) )
					{
					echo"<li class='dropdown active'><a href='#'><span class='iconfa-random'></span>Modul Web </a>
                	 <ul style='display: block'>

                        <li class='active'><a href='media.php?module=kategori'>Kategori</a></li>

                    </ul>
                	</li>";
				 }else
				 {
				 echo"<li class='dropdown'><a href='#'><span class='iconfa-random'></span> Modul Web </a>
                	<ul>

                        <li><a href='media.php?module=kategori'>Kategori</a></li>

                    </ul>
					</li>";
					}
                    leftmenu-->
					 <?php
					//Menu Setting
				 $module = $_GET['module'];
				 $a='identitas';
				 $b='cse';
				 $c='menu';
				 $d='submenu';
				 $e='tagline';
				 $f='banner';
				 $g='blogsiswa';
				 if( ($module==$a || $module==$b || $module==$c || $module==$d || $module==$e || $module==$f || $module==$g) )
					{
            ?>
				        <li class='dropdown active'><a href='#'><span class='iconfa-wrench'></span> Setting </a>
                	 <ul style='display: block'>
                    	<!--<li class='active'><a href='media.php?module=identitas'>Identitas</a></li>
                        <li><a href='media.php?module=tagline'>Tag Line</a></li>
						<li><a href='media.php?module=banner'>Banner</a></li>
						<li><a href='media.php?module=blogsiswa'>Blog Siswa</a></li>
                        <li><a href='media_non.php?module=cse'>Google Search</a></li>-->
                        <li><a href='media_non.php?module=menu'>Menu</a></li>
                        <li><a href='media_non.php?module=submenu'>Sub Menu</a></li>
                    </ul>
                	</li>
            <?php
				 }else
				 {?>
				 <li class='dropdown'><a href='#'><span class='iconfa-wrench'></span> Setting </a>
                	<ul>
                    	<!--<li><a href='media.php?module=identitas'>Identitas</a></li>
                        <li><a href='media.php?module=tagline'>Tag Line</a></li>
						<li><a href='media.php?module=banner'>Banner</a></li>
						<li><a href='media.php?module=blogsiswa'>Blog Siswa</a></li>
                        <li><a href='media_non.php?module=cse'>Google Search</a></li>-->
                        <li><a href='media_non.php?module=menu'>Menu</a></li>
                        <li><a href='media_non.php?module=submenu'>Sub Menu</a></li>
                    </ul>
					</li>
          <?php
					}
					 //Menu Lay out
				 /*$module = $_GET['module'];
				 $a='sidebar';
				 $b='content';
				 if( ($module==$a || $module==$b) )
					{
					echo"<li class='dropdown active'><a href='#'><span class='iconfa-tasks'></span> Layout </a>
                	 <ul style='display: block'>
                    	<li class='active'><a href='media.php?module=sidebar'>Sidebar</a></li>
                        <li><a href='media.php?module=content'>Content</a></li>
                    </ul>
                	</li>";
				 }else
				 {
				 echo"<li class='dropdown'><a href='#'><span class='iconfa-tasks'></span> Layout </a>
                	<ul>
                    	<li><a href='media.php?module=sidebar'>Sidebar</a></li>
                        <li><a href='media.php?module=content'>Content</a></li>
                    </ul>
					</li>";
        }*/
				//Menu Halaman
				if ($_GET['module']=='users'){
				echo"<li class='active'><a href='media.php?module=users'><span class=' iconfa-user'></span> User</a></li>";
				}else{
				echo"<li><a href='media.php?module=users'><span class=' iconfa-user'></span> User</a></li>";
				}
					?>


                 <li><a href="media.php?module=komentar"><span class="iconfa-random"></span>Komentar</a></li>


                <li><a href="logout.php"><span class="iconfa-share"></span> Log Out</a></li>

            </ul>
        </div><!--leftmenu-->

    </div><!-- leftpanel -->

    <?php
}else
{

?>
<div class="leftpanel">

        <div class="leftmenu">
            <ul class="nav nav-tabs nav-stacked">
            	<li class="nav-header">Navigation</li>
                <li><a href="dashboard.html"><span class="iconfa-laptop"></span> Dashboard</a></li>


                 <li><a href="media.php?module=komentar"><span class="iconfa-random"></span>Komentar</a></li>


                <li><a href="users.html"><span class="iconfa-user"></span> User</a></li>
                <li><a href="logout.php"><span class="iconfa-share"></span> Log Out</a></li>


            </ul>
        </div><!--leftmenu-->

    </div><!-- leftpanel -->

        <?php
}

?>
