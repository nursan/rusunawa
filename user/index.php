<!DOCTYPE html>
<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>...:::Rusunawa UIN | Login::......</title>
<link rel="stylesheet" href="css/style.default.css" type="text/css" />
<link rel="shortcut icon" href="../favicon.png" type="image/x-icon" />




<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#login').submit(function(){
            var u = jQuery('#username').val();
            var p = jQuery('#password').val();
            if(u == '' && p == '') {
                jQuery('.login-alert').fadeIn();
                return false;
            }
        });
    });
</script>
<style>
	.balok{
		border:3px solid #999;
		padding:30px;
		background:#3399ff;
	}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body class="loginpage">

<div class="loginpanel">
    <div class="loginpanelinner balok  animate0 bounceIn">
        <div class="logo animate0 bounceIn"><img width="150" src="images/lojo.png"></div>
        <form id="login" action="cek_login.php" method="post" />
            <div class="inputwrapper login-alert">
                <div class="alert alert-error">Invalid username or password</div>
            </div>
            <div class="inputwrapper animate1 bounceIn">
                <input type="text" name="username" id="username" placeholder="Enter any username" />
            </div>
            <div class="inputwrapper animate2 bounceIn">
                <input type="password" name="password" id="password" placeholder="Enter any password" />
            </div>
            <div class="inputwrapper animate3 bounceIn">
                <button name="submit">Sign In</button>
            </div>


        </form>
    </div><!--loginpanelinner-->
</div>
<!--loginpanel-->

<div class="loginfooter">
    <p>&copy; 2018. UINAM DEV</p>
</div>

</body>
</html>
