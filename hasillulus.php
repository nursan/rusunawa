<!DOCTYPE html>
<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<link rel="stylesheet" href="css/style.default.css" type="text/css" />
<script type="text/javascript" src="jscript/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="jscript/js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="jscript/js/jquery.uniform.min.js"></script>
<script type="text/javascript" src="jscript/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="jscript/js/jquery.cookie.js"></script>
<script type="text/javascript" src="jscript/js/responsive-tables.js"></script>
<script type="text/javascript" src="jscript/js/custom.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        // dynamic table
        jQuery('#dyntable').dataTable({
            "sPaginationType": "full_numbers",
            "aaSortingFixed": [[0,'asc']],
            "fnDrawCallback": function(oSettings) {
                jQuery.uniform.update();
            }
        });
        
        jQuery('#dyntable2').dataTable( {
            "bScrollInfinite": true,
            "bScrollCollapse": true,
            "sScrollY": "300px"
        });
        
    });
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>

<div class="mainwrapper">


        <div class="maincontent">
            <div class="maincontentinner">
				<P>Di harapkan segera meluna biaya administrasi di rektorat</p>
               <br>
                <table id="dyntable" class="table table-bordered">
                    <colgroup>
                        <col class="con0" style="align: center; width: 4%" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                    </colgroup>
                    <thead>
                        <tr>
                          	<th class="head0 nosort">No</th>
                            <th class="head0">NIM</th>
                            <th class="head1">Nama Mahasiswa</th>
                            <th class="head1">Fakultas</th>
                            <th class="head0">Jurusan</th>
                            <th class="head0">Lulus</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
					$tampil = mysqli_query($conn,"
					SELECT
					b.id_hasil,a.nim,a.id_pendaftaran,a.nama,a.tempat,a.tgl_lahir,b.total,c.fakultas,d.jurusan
					FROM
					pendaftaran a
					INNER JOIN hasil_tes b
					ON a.nim=b.id_pendaftaran
					INNER JOIN 
					tbl_fakultas c
					ON a.fakultas=c.id_fakultas
					INNER JOIN tbl_jurusan d
					ON d.id_jurusan=a.jurusan
					");
					
  				    $no = 1;   
					while($r=mysqli_fetch_array($tampil)){
					//$total = $r['wawancara'] + $r['tulis'] + $r['skhun'];
					echo"<tr class='gradeA'>
                          <td class='aligncenter'>$no</td>
                            <td>$r[nim]</td>
                            <td>$r[nama]</td>
                            <td>$r[fakultas]</td>
                            <td>$r[jurusan]</td>
                            <td class='center'>";
							if($r['total'] > 210) echo "<b>LULUS</b>"; else echo "TIDAK LULUS";
							echo"</td>";
							
                        	echo"</tr>";
					//}
	//			}
				
				
      $no++;
    }
		  

?>
                    
                        
                    </tbody>
                </table>
                
              
              
               
            </div><!--maincontentinner-->
        </div><!--maincontent-->
</div><!--mainwrapper-->
</body>
</html>
