/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : kosuin

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2016-08-12 20:09:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for agenda
-- ----------------------------
DROP TABLE IF EXISTS `agenda`;
CREATE TABLE `agenda` (
  `id_agenda` int(5) NOT NULL AUTO_INCREMENT,
  `tema` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `tema_seo` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `isi_agenda` text COLLATE latin1_general_ci NOT NULL,
  `tempat` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `pengirim` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `tgl_posting` date NOT NULL,
  `jam` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `dibaca` int(5) NOT NULL DEFAULT '1',
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_agenda`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of agenda
-- ----------------------------
INSERT INTO `agenda` VALUES ('65', 'Kemah Penerimaan Tamu Ambalan & Pelantikan Bantara', 'kemah-penerimaan-tamu-ambalan--pelantikan-bantara', '<div>\r\nKegiatan ini terselenggara atas dasar program kerja tahunan dan rapat dewan ambalan, selain itu Gerakan Pramuka merupakan satu-satunya organisasi yang direkomendasikan pemerintah sebagai gerakan kepanduan bagi para generasi muda khususnya, sehingga pada hari Kamis hingga Minggu, tanggal 5 s/d 8 September 2013 terselenggara kegiatan yang menumbuhkan karakter bagi siswa-siswi SMA\r\n</div>\r\n<div>\r\n&nbsp;\r\n</div>\r\n', 'Alun - alun', '', '382049scout.JPG', '2014-08-17', '2014-08-24', '2014-08-24', '08.00', '1', 'admin');
INSERT INTO `agenda` VALUES ('66', 'Try Out UN Dinas Pendidikan Kab. Pacitan', 'try-out-un-dinas-pendidikan-kab-pacitan', '<span style=\"color: #202020; font-family: Arial, Tahoma, Verdana; font-size: 12px; line-height: 18px\">Sebagai sarana uji coba Ujian Nasional 2014, Dinas Pendidikan Kabupaten Pacitan menggelar try out UN SMA/SMK/MA 2014 serentak se kabupaten Pacitan yang bertujuan untuk mempersiapkan siswa dalam menghadapi UN 2014.</span>\r\n', 'Dinas Pendidikan', '', '785766try.jpg', '2014-07-20', '2014-07-22', '2014-08-24', '08.00', '1', 'admin');

-- ----------------------------
-- Table structure for anggota_asrama
-- ----------------------------
DROP TABLE IF EXISTS `anggota_asrama`;
CREATE TABLE `anggota_asrama` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_asrama` varchar(255) DEFAULT NULL,
  `id_pendaftar` varchar(255) DEFAULT NULL,
  `no_kamar` int(255) DEFAULT NULL,
  `jumlah_bulan` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of anggota_asrama
-- ----------------------------
INSERT INTO `anggota_asrama` VALUES ('4', '1', '3', '3', '6');

-- ----------------------------
-- Table structure for asrama
-- ----------------------------
DROP TABLE IF EXISTS `asrama`;
CREATE TABLE `asrama` (
  `id_asrama` int(5) NOT NULL AUTO_INCREMENT,
  `pembina` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nama_asrama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `jumlah_lantai` int(1) NOT NULL,
  `jumlah_kamar` int(1) NOT NULL,
  `jumlah_ranjang` int(1) NOT NULL,
  `fasilitas` text CHARACTER SET latin2 NOT NULL,
  `gender` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `lokasi` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `seo_asrama` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_asrama`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of asrama
-- ----------------------------
INSERT INTO `asrama` VALUES ('1', 'jojo', 'jojo', '2', '14', '2', 'jojo\r\n', 'pria', 'jojo', 'jojo');

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `id_banner` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `tgl_posting` date NOT NULL,
  PRIMARY KEY (`id_banner`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of banner
-- ----------------------------
INSERT INTO `banner` VALUES ('17', 'UIN Alauddin', 'http://uin-alauddin.ac.id', 'uin.jpg', '2011-06-26');
INSERT INTO `banner` VALUES ('14', 'LPSE', '#', 'lpse.JPG', '2011-06-22');
INSERT INTO `banner` VALUES ('18', 'Departement Agama', '#', 'depag.jpg', '2011-06-26');

-- ----------------------------
-- Table structure for berita
-- ----------------------------
DROP TABLE IF EXISTS `berita`;
CREATE TABLE `berita` (
  `id_berita` int(5) NOT NULL AUTO_INCREMENT,
  `id_kategori` int(5) NOT NULL,
  `username` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `judul` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `sub_judul` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `youtube` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `download` varchar(100) CHARACTER SET latin2 NOT NULL,
  `judul_seo` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `headline` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `utama` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `isi_berita` text COLLATE latin1_general_ci NOT NULL,
  `keterangan_gambar` text COLLATE latin1_general_ci NOT NULL,
  `hari` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `gambar` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `klik` int(5) NOT NULL DEFAULT '1',
  `tag` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `breaking_news` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `download_time` int(5) NOT NULL,
  PRIMARY KEY (`id_berita`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of berita
-- ----------------------------
INSERT INTO `berita` VALUES ('2', '2', 'admin', 'Melirik Canggihnya Nokia Lumia 630 dan 635', '', '', '', 'melirik-canggihnya-nokia-lumia-630-dan-635', 'Y', 'N', 'Y', '<p style=\"margin: 10px 0px 5px; padding: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 19.200000762939453px; text-align: justify\">\r\nSelain Lumia 930, Nokia juga menghadirkan Lumia 630 dan 635 di atas panggung Microsoft Build, di San Francisco, Amerika Serikat. Ini adalah duo smartphone Windows Phone 8.1 dengan harga terjangkau.\r\n</p>\r\n<p style=\"margin: 10px 0px 5px; padding: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 19.200000762939453px; text-align: justify\">\r\n&ldquo;Lumia 630 dan Lumia 635 memang ditujukan untuk mengakomodir pasar entry level untuk merasakan pengalaman menggunakan Windows Phone,&rdquo; terang Head of Microsoft Devices and Studios Stephen Elop, yang dikutip dari The Verge, Kamis (3/4/2014).&nbsp;\r\n</p>\r\n<p style=\"margin: 10px 0px 5px; padding: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 19.200000762939453px; text-align: justify\">\r\nElop menjanjikan bahwa kedua smartphone terbarunya tersebut akan dijual dengan harga di bawah USD 200 atau Rp 2 jutaan. Kendati murah, tak sekadar murahan fitur yang akan ditawarkan di kedua Lumia ini.\r\n</p>\r\n<p style=\"margin: 10px 0px 5px; padding: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 19.200000762939453px; text-align: justify\">\r\nBaik Lumia 630 dan 635 akan mengusung layar 4,5 inch dengan prosesor 1.2 Ghz dan sistem operasi Windows Phone 8.1.\r\n</p>\r\n<p style=\"margin: 10px 0px 5px; padding: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 19.200000762939453px; text-align: justify\">\r\nPerbedaan kedua ponsel cerdas ini adalah Lumia 630 dihadirkan dengan dua versi single SIM dan dual-SIM 3G. Sementara Lumia 635 sudah disokong jaringan 4G.\r\n</p>\r\n<p style=\"margin: 10px 0px 5px; padding: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 19.200000762939453px; text-align: justify\">\r\nSecara spesifik, Lumia 630 akan dibanderol dengan harga USD 169 atau Rp 1,7 jutaan untuk versi dual-SIM3G dan USD 159 yang versi single SIM. Sementara Lumia 635 dihargai USD 189 atau Rp 1,9 jutaan. Keduanya dilempar ke Asia pada bulan Mei 2014.\r\n</p>\r\n', '', 'Minggu', '2014-08-24', '10:37:25', '2031nokia.jpg', '6', 'pendidikan, ppdb, agenda', 'N', '0');
INSERT INTO `berita` VALUES ('3', '1', 'admin', 'Real Madrid Juara Copa Del Rey 2014: Final Barcelona vs Real Madrid El Clasico di Mestalla', '', '', '', 'real-madrid-juara-copa-del-rey-2014-final-barcelona-vs-real-madrid-el-clasico-di-mestalla', 'Y', 'N', 'Y', '<p style=\"margin: 0px 0px 2.1em; padding: 0px; color: #444444; font-size: 15px; line-height: 20.695499420166016px; font-family: Roboto, sans-serif\">\r\nFinal Piala Raja Spanyol alias Copa de Rey antara Barcelona vs Real Madrid yang kerap disebut El Clasico berlangsung kurang greget di babak 1. Di tengah absennya Ronaldo, Lionel Messi seperti tampil malas-malasan. Untuk sementara Los Blancos unggul 1-0 lewat gol Angel di Maria pada menit 11.\r\n</p>\r\n<p style=\"margin: -0.8em 0px 2.1em; padding: 0px; color: #444444; font-size: 15px; line-height: 20.695499420166016px; font-family: Roboto, sans-serif\">\r\nPertarungan kurang greget di final Copa del Rey diperparah dengan banyak sandiwara yang dilakukan para pemain Real Madrid dan Barcelona. El Clasico sudah berkurang gaharnya ketika Cristiano Ronaldo dipasatikan absen. Tambahan lagi, aksi Messi tidak terlihat sama sekali di babak 1. Untung ada gol di Maria yang mengubah skor Real Madrid vs Barcelona menjadi 1-0 sebagai penghibur di babak pertama.\r\n</p>\r\n<p style=\"margin: -0.8em 0px 2.1em; padding: 0px; color: #444444; font-size: 15px; line-height: 20.695499420166016px; font-family: Roboto, sans-serif\">\r\nLionel Messi hanya beberapa kali saja menerima bola selama 45 menit babak 1 final Copa Del Rey. Dia kebanyakan diam atau berlari tidak jelas&nbsp;<em style=\"font-family: Georgia, Times, serif\">juntrungan</em>. Tidak muncul aksinya menggoreng bola sambil meliuk-liuk menggocek lawan. Ada apa dengan Messi?\r\n</p>\r\n<p style=\"margin: -0.8em 0px 2.1em; padding: 0px; color: #444444; font-size: 15px; line-height: 20.695499420166016px; font-family: Roboto, sans-serif\">\r\nBarcelona sebenarnya mendikte jalan dan tempo pertandingan menyusul gol Real Madrid pada menit 10 babak pertama.\r\n</p>\r\n<p style=\"margin: -0.8em 0px 2.1em; padding: 0px; color: #444444; font-size: 15px; line-height: 20.695499420166016px; font-family: Roboto, sans-serif\">\r\nMalapetaka terjadi ketika bek kanan Barcelona Dani Alves kehilangan bola sehingga membuat Real mengambil keuntungan untuk melancarkan serangan balik nan cepat.\r\n</p>\r\n<p style=\"margin: -0.8em 0px 2.1em; padding: 0px; color: #444444; font-size: 15px; line-height: 20.695499420166016px; font-family: Roboto, sans-serif\">\r\nKarim Benzema berduet dengan Angel Di Maria menusuk jantung pertahanan Barca dan pemain asal Argentina ini melepaskan tembakan ke sudut bawah gawang Barca yang dijaga Jose Manuel Pinto. Gol ini berbau&nbsp;<em style=\"font-family: Georgia, Times, serif\">offside,</em>&nbsp;namun wasit tetap menyatakan sah gol ini.\r\n</p>\r\n<p style=\"margin: -0.8em 0px 2.1em; padding: 0px; color: #444444; font-size: 15px; line-height: 20.695499420166016px; font-family: Roboto, sans-serif\">\r\nKendati Barcelona membombardir pertahanan Madrid, namun juara Liga Utama Spanyol ini tak bisa menciptakan gol balasan sehingga kedudukan tetap 1-0 untuk Madrid sampai babak pertama usai.\r\n</p>\r\n<p style=\"margin: -0.8em 0px 2.1em; padding: 0px; color: #444444; font-size: 15px; line-height: 20.695499420166016px; font-family: Roboto, sans-serif\">\r\nPenampilan Lionel Messi belakangan ini memang menjadi perhatian karena tidak setajam pada musim-musim sebelumnya. Meski begitu, legenda Barcelona asal Belanda, Johan Cruyff mengkritik mantan tim asuhannya, Barcelona, sebagai tim tanpa rencana yang jelas.\r\n</p>\r\n<p style=\"margin: -0.8em 0px 2.1em; padding: 0px; color: #444444; font-size: 15px; line-height: 20.695499420166016px; font-family: Roboto, sans-serif\">\r\nKritik itu terlontar setelah Barcelona tersingkir dari Liga Champions dan kalah di Liga melawan Granada.\r\n</p>\r\n<p style=\"margin: -0.8em 0px 2.1em; padding: 0px; color: #444444; font-size: 15px; line-height: 20.695499420166016px; font-family: Roboto, sans-serif\">\r\nCruyff justru membela Lionel Messi yang dianggap tidak bermain baik ketika&nbsp;Blaugrana&nbsp;disingkirkan Atletico di perempatfinal Liga Champions.\r\n</p>\r\n<p style=\"margin: -0.8em 0px 2.1em; padding: 0px; color: #444444; font-size: 15px; line-height: 20.695499420166016px; font-family: Roboto, sans-serif\">\r\n&quot;Bagaimana bisa Messi tidak bermain menarik (pada pertandingan melawan Atletico) jika 10 hari sebelumnya di&nbsp;El Clasico&nbsp;dia menjadi pemain terbaik di lapangan?&quot; kata Cruyff dilansir dari laman AS, Senin (14/4/2014).\r\n</p>\r\n<p style=\"margin: -0.8em 0px 2.1em; padding: 0px; color: #444444; font-size: 15px; line-height: 20.695499420166016px; font-family: Roboto, sans-serif\">\r\n&quot;Dia membuat segalanya terjadi, ia mencetak tiga gol, dia bertanggung jawab dan bermain luar biasa,&quot; kata Cruyff. &quot;Dan melawan Atletico, ketika ia terlihat tidak menarik, justru ia pemain paling berbahaya bagi Barcelona.&quot;\r\n</p>\r\n<p style=\"margin: -0.8em 0px 2.1em; padding: 0px; color: #444444; font-size: 15px; line-height: 20.695499420166016px; font-family: Roboto, sans-serif\">\r\nMenurut Cruyff, Barcelona rapuh karena selama empat atau lima tahun terakhir terjadi kesalahan pola manajemen. Ia mengaku tidak heran jika Barcelona tidak sebaik ketika memenangkan tiga gelar Liga Champions periode 2006 sampai 2011.\r\n</p>\r\n<p style=\"margin: -0.8em 0px 2.1em; padding: 0px; color: #444444; font-size: 15px; line-height: 20.695499420166016px; font-family: Roboto, sans-serif\">\r\n&quot;Tidak ada rencana yang jelas, tidak ada aturan yang tetap,&quot; katanya . &quot;Dalam lingkungan yang stabil penampilan tim harus ditingkatkan melalui pertandingan besar yang datang bersama-sama, dan di sini hal itu tidak dilakukan.&quot;&nbsp;\r\n</p>\r\n<p style=\"margin: -0.8em 0px 2.1em; padding: 0px; color: #444444; font-size: 15px; line-height: 20.695499420166016px; font-family: Roboto, sans-serif\">\r\n&quot;Itu semua merupakan konsekuensi pengaruh dari luar ruang ganti, di situlah masalah bermula,&quot; kata Cruyff.\r\n</p>\r\n', '', 'Minggu', '2014-08-24', '10:39:01', '1047copa.jpg', '9', 'pendidikan, ppdb, agenda', 'N', '0');
INSERT INTO `berita` VALUES ('4', '3', 'admin', 'Cara Instal XAMPP di Windows 7', '', 'http://www.youtube.com/embed/mz4MJyD4oUE', '', 'cara-instal-xampp-di-windows-7', 'N', 'N', 'Y', '<p>\r\n<font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">Sebelum masuk pada inti pembahasan &quot;<strong>Cara Install XAMPP Pada Windows 7</strong>&quot;. Perkenankan saya coba sedikit menguraikan tentang XAMPP.<br />\r\nBagi Developper Web/programer pastinya sudah tidak asing lagi dengan Program yang satu ini&nbsp;<strong>XAMPP</strong>. Yang dimana sering di gunakan Sebagai &quot;SERVER&quot; dalam pembuatan Program ataupun Pembangunan Website secara Professional.<br />\r\n<br />\r\n<strong>XAMPP</strong>&nbsp;adalah &quot;Perangkat lunak bebas (&quot;FREE&quot;) yang mendukung&nbsp; banyak System Operasi.<br />\r\nKompilasi dari berbagai program dalam satu Paket.<br />\r\n<span class=\"hps\">terutama terdiri</span>&nbsp;<span class=\"hps\">dari</span>&nbsp;<span class=\"hps\">Apache</span>&nbsp;<span class=\"hps\">HTTP</span>&nbsp;<span class=\"hps\">Server</span>,&nbsp;<span class=\"hps\">database MySQL</span>,&nbsp;<span class=\"hps\">dan</span>&nbsp;<span class=\"hps\">interpreter untuk</span>&nbsp;<span class=\"hps\">script yang ditulis</span>&nbsp;<span class=\"hps\">dalam</span>&nbsp;<span class=\"hps\">PHP</span>&nbsp;<span class=\"hps\">dan bahasa pemrograman</span>&nbsp;<span class=\"hps\">Perl</span>.<br />\r\n<br />\r\n<u>XAMPP</u>&nbsp;singkatan dari :<br />\r\n</font>\r\n</p>\r\n<ul>\r\n	<li><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">X (<span class=\"hps\">yang berarti</span>&nbsp;<span class=\"hps\">cross-platform)</span></font></li>\r\n	<li><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">A (Apache)</font></li>\r\n	<li><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">M (MySql)</font></li>\r\n	<li><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">P (php)</font></li>\r\n	<li><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">P (Perl)</font></li>\r\n</ul>\r\n<p>\r\n<font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">XAMPP ini bisa kita Download dari situs resminya di&nbsp;<a style=\"color: #ed292a; outline: none\" href=\"http://adf.ly/249011/http://www.apachefriends.org/en/xampp-windows.html\" target=\"_blank\">apachefriends &nbsp;&nbsp;</a>&nbsp;&nbsp;gratis .<br />\r\n&nbsp;Cara installnyapun simple dan mudah di jalankan.<br />\r\nOk !! demikian sedikit penjelasan tentang XAMPP.untuk anda yang pengin tahu lebih tentang XAMPP anda bisa baca di&nbsp;<a style=\"color: #ed292a; outline: none\" href=\"http://adf.ly/249011/http://en.wikipedia.org/wiki/XAMPP\" target=\"_blank\">Wikipedia</a>&nbsp;atau situs-situs yang membahas lebih dalam XAMPP.<br />\r\n<br />\r\nKita kembali pada Topik Pembahasan tentang<strong>&nbsp;Cara Install XAMPP Pada Windows 7</strong>.<br />\r\ncaranya adalah sbb:<br />\r\n</font>\r\n</p>\r\n<ol>\r\n	<li><span class=\"hps\"><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">Download terlebih dahulu Program XAMPP di&nbsp;<a style=\"color: #ed292a; outline: none\" href=\"http://adf.ly/249011/http://www.apachefriends.org/en/xampp-windows.html\" target=\"_blank\">ApacheFriends</a></font></span></li>\r\n	<li><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">Jalankan intallasi program&nbsp;</font></li>\r\n</ol>\r\n<p>\r\n<font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">Perhatikan gambar Berikut:<br />\r\n<br />\r\n</font>\r\n</p>\r\n<ul>\r\n	<li><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">Pada baianiniadalah Pilihan Bahasa. kemudian klik &quot;OK&quot;</font></li>\r\n</ul>\r\n<div class=\"separator\" style=\"clear: both; text-align: center\">\r\n<a style=\"color: #ed292a; outline: none; margin-left: 1em; margin-right: 1em\" href=\"http://adf.ly/249011/http://3.bp.blogspot.com/-q3bYpVi-kfs/UHL2Ma32uII/AAAAAAAAAsQ/_OIlKGXhPPk/s1600/install+xampp.jpg\"><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"><img style=\"border-width: 0px\" src=\"http://3.bp.blogspot.com/-q3bYpVi-kfs/UHL2Ma32uII/AAAAAAAAAsQ/_OIlKGXhPPk/s1600/install+xampp.jpg\" border=\"0\" alt=\"\" /></font></a>\r\n</div>\r\n<p>\r\n<font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"><br />\r\n</font>\r\n</p>\r\n<ul>\r\n	<li><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">&nbsp;jika muncul Jendela seperti berikut</font></li>\r\n</ul>\r\n<p>\r\n<a style=\"color: #ed292a; outline: none; clear: right; float: right; margin-bottom: 1em; margin-left: 1em\" href=\"http://adf.ly/249011/http://3.bp.blogspot.com/-Lx5Ge8c0XYw/UHL3H9uzOwI/AAAAAAAAAsY/kF85bxJIXls/s1600/install+xampp_1.jpg\"><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"><img style=\"border-width: 0px\" src=\"http://3.bp.blogspot.com/-Lx5Ge8c0XYw/UHL3H9uzOwI/AAAAAAAAAsY/kF85bxJIXls/s1600/install+xampp_1.jpg\" border=\"0\" alt=\"\" /></font></a><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">&nbsp;<br />\r\n</font>\r\n</p>\r\n<div class=\"separator\" style=\"clear: both; text-align: center\">\r\n<a style=\"color: #ed292a; outline: none; margin-left: 1em; margin-right: 1em\" href=\"http://adf.ly/249011/http://1.bp.blogspot.com/-KzcLWahyF3A/UHL4imh-ZkI/AAAAAAAAAsg/e6Ki5d8yIS0/s1600/UAC.jpg\"><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"><br />\r\n</font></a>\r\n</div>\r\n<ul>\r\n	<li><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">jika muncul jendela seperti di atas.anda periksa terlebih dahulu &quot;<span style=\"color: red\">UAC</span>&quot; (<strong style=\"color: red\"><u>User Account Control</u></strong>) pada windows anda.</font></li>\r\n	<li><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">caranya =&gt;&gt; pada<strong style=\"color: blue\">&nbsp;Star</strong>t=&gt;&gt; kemudian pada&nbsp; Search anda ketik<strong><span style=\"color: red\">&nbsp;UAC</span></strong>&nbsp;=&gt;&gt; kemudian tekan&nbsp;<strong style=\"color: red\">Enter</strong></font></li>\r\n	<li><span class=\"hps\"><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">dan akan muncul jendela baru.Kemudian Turunkan UAC hingga sampai Bawah. Kemudian klik<strong style=\"color: red\">&nbsp;OK</strong></font></span></li>\r\n</ul>\r\n<div class=\"separator\" style=\"clear: both; text-align: center\">\r\n<a style=\"color: #ed292a; outline: none; margin-left: 1em; margin-right: 1em\" href=\"http://adf.ly/249011/http://1.bp.blogspot.com/-KzcLWahyF3A/UHL4imh-ZkI/AAAAAAAAAsg/e6Ki5d8yIS0/s1600/UAC.jpg\"><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"><img style=\"border-width: 0px\" src=\"http://1.bp.blogspot.com/-KzcLWahyF3A/UHL4imh-ZkI/AAAAAAAAAsg/e6Ki5d8yIS0/s400/UAC.jpg\" border=\"0\" alt=\"\" width=\"400\" height=\"292\" /></font></a>\r\n</div>\r\n<p>\r\n<font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"><br />\r\n</font>\r\n</p>\r\n<ul>\r\n	<li><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">setelah selesai menurunkan<strong style=\"color: red\">&nbsp;UAC</strong>. kemudian lanjutkan Installasi XAMPP tadi.</font></li>\r\n</ul>\r\n<div class=\"separator\" style=\"clear: both; text-align: center\">\r\n<a style=\"color: #ed292a; outline: none; margin-left: 1em; margin-right: 1em\" href=\"http://adf.ly/249011/http://4.bp.blogspot.com/-QnC5W4S2WR8/UHL51U924bI/AAAAAAAAAso/K1TAlssSkmo/s1600/xampp_1.jpg\"><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"><img style=\"border-width: 0px\" src=\"http://4.bp.blogspot.com/-QnC5W4S2WR8/UHL51U924bI/AAAAAAAAAso/K1TAlssSkmo/s320/xampp_1.jpg\" border=\"0\" alt=\"\" width=\"320\" height=\"248\" /></font></a>\r\n</div>\r\n<p>\r\n<font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">#&nbsp; klik Next<br />\r\n<br />\r\n</font>\r\n</p>\r\n<div class=\"separator\" style=\"clear: both; text-align: center\">\r\n<a style=\"color: #ed292a; outline: none; margin-left: 1em; margin-right: 1em\" href=\"http://adf.ly/249011/http://3.bp.blogspot.com/-uQd7bNSWaek/UHL6U6QNSdI/AAAAAAAAAsw/Fd3FOecRJ2s/s1600/xampp_2.jpg\"><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"><img style=\"border-width: 0px\" src=\"http://3.bp.blogspot.com/-uQd7bNSWaek/UHL6U6QNSdI/AAAAAAAAAsw/Fd3FOecRJ2s/s320/xampp_2.jpg\" border=\"0\" alt=\"\" width=\"320\" height=\"248\" /></font></a>\r\n</div>\r\n<p>\r\n<font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"># Perhatikan gambar yang saya kasih tanda<strong style=\"color: red\">&nbsp;no:3</strong><br />\r\njangan lupa Kasih tanda Centang pada ketiga Kotak. kemudian<strong style=\"color: red\">&nbsp;Klik Install</strong><br />\r\n<br />\r\n</font>\r\n</p>\r\n<div class=\"separator\" style=\"clear: both; text-align: center\">\r\n<a style=\"color: #ed292a; outline: none; margin-left: 1em; margin-right: 1em\" href=\"http://adf.ly/249011/http://2.bp.blogspot.com/-S3iGbOdfrlI/UHL7W_jzjyI/AAAAAAAAAs4/Vtwf8goN_FU/s1600/Prosess+install+xampp.jpg\"><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"><img style=\"border-width: 0px\" src=\"http://2.bp.blogspot.com/-S3iGbOdfrlI/UHL7W_jzjyI/AAAAAAAAAs4/Vtwf8goN_FU/s320/Prosess+install+xampp.jpg\" border=\"0\" alt=\"\" width=\"320\" height=\"248\" /></font></a>\r\n</div>\r\n<p>\r\n<font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"># Gambar di atas adalah Prosess installasi. tunggu sampai proses installasi selesai hngga muncul confirmasi installasi finiss<br />\r\n</font>\r\n</p>\r\n<div class=\"separator\" style=\"clear: both; text-align: center\">\r\n<a style=\"color: #ed292a; outline: none; margin-left: 1em; margin-right: 1em\" href=\"http://adf.ly/249011/http://1.bp.blogspot.com/-WbW98ZleiwM/UHL8DnHGTCI/AAAAAAAAAtA/88vSWeXVEAA/s1600/install+xampp+finiss.jpg\"><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"><img style=\"border-width: 0px\" src=\"http://1.bp.blogspot.com/-WbW98ZleiwM/UHL8DnHGTCI/AAAAAAAAAtA/88vSWeXVEAA/s320/install+xampp+finiss.jpg\" border=\"0\" alt=\"\" width=\"320\" height=\"248\" /></font></a>\r\n</div>\r\n<p>\r\n<font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"># jika installasi telah succes. Buka XAMPP yang telah anda install tadi.<br />\r\nTampilan cPanelnya sbb:<br />\r\n</font>\r\n</p>\r\n<div class=\"separator\" style=\"clear: both; text-align: center\">\r\n<a style=\"color: #ed292a; outline: none; margin-left: 1em; margin-right: 1em\" href=\"http://adf.ly/249011/http://3.bp.blogspot.com/-CIN7AwPCbWw/UHL8e4n16UI/AAAAAAAAAtI/WaSTl9Sf-ZY/s1600/cPanel+xampp.jpg\"><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"><img style=\"border-width: 0px\" src=\"http://3.bp.blogspot.com/-CIN7AwPCbWw/UHL8e4n16UI/AAAAAAAAAtI/WaSTl9Sf-ZY/s320/cPanel+xampp.jpg\" border=\"0\" alt=\"\" width=\"320\" height=\"201\" /></font></a>\r\n</div>\r\n<p>\r\n<font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"><br />\r\n<br />\r\n<br />\r\n# Pastikan Apache dan MySql telah Running.<br />\r\n# untuk mengetest Sever XAMPP anda. Buka Broser =&gt;&gt; kemudian ketik &quot;Http://localhost&quot; pada Browser anda.dan tampilanya sbb:<br />\r\n<br />\r\n#Tampilan Localhost&nbsp;<br />\r\n</font>\r\n</p>\r\n<div class=\"separator\" style=\"clear: both; text-align: center\">\r\n<a style=\"color: #ed292a; outline: none; margin-left: 1em; margin-right: 1em\" href=\"http://adf.ly/249011/http://1.bp.blogspot.com/-nkN15CA9gXk/UHL9JgglpKI/AAAAAAAAAtQ/FV3XK571iE8/s1600/tampilan+Localhost.jpg\"><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"><img style=\"border-width: 0px\" src=\"http://1.bp.blogspot.com/-nkN15CA9gXk/UHL9JgglpKI/AAAAAAAAAtQ/FV3XK571iE8/s320/tampilan+Localhost.jpg\" border=\"0\" alt=\"\" width=\"320\" height=\"231\" /></font></a>\r\n</div>\r\n<p>\r\n<font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"><br />\r\n# Tampilan PhpMyadmin<br />\r\n</font>\r\n</p>\r\n<div class=\"separator\" style=\"clear: both; text-align: center\">\r\n<a style=\"color: #ed292a; outline: none; margin-left: 1em; margin-right: 1em\" href=\"http://adf.ly/249011/http://2.bp.blogspot.com/-uIOEFqN3m0c/UHL9dWMfTcI/AAAAAAAAAtY/iIjHWBnqlUw/s1600/Tampilan_phpyadmin.jpg\"><font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\"><img style=\"border-width: 0px\" src=\"http://2.bp.blogspot.com/-uIOEFqN3m0c/UHL9dWMfTcI/AAAAAAAAAtY/iIjHWBnqlUw/s320/Tampilan_phpyadmin.jpg\" border=\"0\" alt=\"\" width=\"320\" height=\"231\" /></font></a>\r\n</div>\r\n<p>\r\n<font face=\"tahoma, arial, helvetica, sans-serif\" size=\"3\">Jika pada Browser telah bisa menampilkan seperti pada Gambar di atas. Berarti penginstalan XAMPP telah Berhasil dan siap untuk digunakan sebagai&nbsp;<strong style=\"color: red\">Server Localhost</strong>&nbsp;pada komputer.<br />\r\nDemikian sedikit Shared dari saya.semoga bermanfaat.dan jika ada kesalahan atau kekurangan dalam penulisan artikel saya ini.Mohon maaf dan harap maklum karena masih Belajar.<br />\r\n<br />\r\n</font>\r\n</p>\r\n', '', 'Minggu', '2014-08-24', '10:41:50', '7448xamp.jpg', '1', 'pendidikan, ppdb, agenda', 'N', '0');
INSERT INTO `berita` VALUES ('5', '2', 'admin', 'Pemrogaman PHP Bagaimana dan apa keunggulannya', '', '', '', 'pemrogaman-php-bagaimana-dan-apa-keunggulannya', 'N', '', '', '<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n&nbsp; Setelah kita punya&nbsp;<a href=\"http://blog.imagomedia.co.id/2014/07/cara-install-xampp-untuk-server-lokal.html\">Server lokal (Localhost</a>) yang sudah kita bahas pada artikel sebelumnya. Selanjutnya hal yang perlu kita pelajari tentunya berkaitan dengan PHP itu sendiri.\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n<strong><br />\r\n</strong><strong>Apa itu PHP?</strong>&nbsp;Sebelum anda belajar PHP, ada baiknya anda sudah mempelajari HTML, karena mau tidak mau ketika kita belajar PHP kita pasti menggunakan Script HTML.Singkatan dari PHP: Hypertext PreprocessorServer-side scripting language, seperti ASP Skrip PHP dieksekusi di server. PHP mendukung banyak database (MySQL, Informix, Oracle, Sybase, Solid, PostgreSQL, Generic ODBC, dll),PHP merupakan perangkat lunak open source, PHP Gratis untuk didownload dan digunakan. Jika anda ingin belajar PHP, pastikan komputer anda sudah terinstal web server seperti Wamp atau&nbsp;<a href=\"http://blog.imagomedia.co.id/2014/07/cara-install-xampp-untuk-server-lokal.html\">Xampp.</a>\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n&nbsp;\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n&nbsp;<strong>Keunggulan PHP</strong>&nbsp;Dapat dijalankan pada berbagai Operating System, Misalnya: Windows, LINUX, dan Mac OS. Dapat dijalankan diberbagai Webserver Misalnya: Apache, Microsoft IIS, Caudium, PWS. Database yang dapat digunakan: MySQL, PostgreSQL, Oracle, Microsoft Access, Interbase Untuk PHP 5 support Object Oriented Programming (OOP) PHP adalah pemrogaman yang sangat handal dimana dapat dikembangkan secara offline maupun online.\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n&nbsp;\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\nJika kita bandingkan dengan bahasa pemograman VB, maka VB hanya bisa digunakan di operating system windows. maka tak heran pemograman PHP lebih mahal dibandingkan dengan pemograman VB. Nah sekarang kita tau bahwa jika bisa menjadi Programmer PHP, kita lebih punya Prestise so kita harus semangat mempelajari PHP :)\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n&nbsp;\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\nOk. Lanjut guys.\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n&nbsp;\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n<strong>Syntaq Dasar PHP</strong>&nbsp;Pada pembelajaran kali ini dibahas Syntaq Dasar PHP. Setelah anda Berkenalan dengan PHP dan mengetahui Keunggulan PHP dibandingkan dengan bahasa pemograman yang lain, Kini saatnya kita mengetahui.\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n&nbsp;\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\nSemua kode php selalu di awali dengan :\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n&nbsp;\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n........\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n........\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n?&gt;\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n&nbsp;....... adalah tempat untuk menuliskan kode php.\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n&nbsp;\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\nAda dua hal mendasar dalam pemrograman PHP Yaitu\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n1. Syarat/Kondisi apa yang diproses oleh PHP atau dikenal dengan Variabel.\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n2. Menampilkan dalam browser hasil dari pengolahan script PHP yaitu &quot;Echo&quot;\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n&nbsp;\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\nContoh :\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n1. Menampilkan data tanpa variabel keterangan : echo&quot;....&quot;; adalah perintah untuk menampilkan data diantara tanda petik &quot;&quot; yang ditutup dengan ;.\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\nPerintah ini menghasilkan Tulisan Selamat Datang di Imago Media pada browser.\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n&nbsp;\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n2. Menampilkan Dengan variabel keterangan : perintah ini memproses lebih dulu kedua variable yaitu ba dan $b kemudian menampilkan nya dengan Echo&quot;$a dan $b yang menghasilkan tampilan pada browser Selamat Datang Imago Media.\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\n&nbsp;\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\nCatatan :\r\n</p>\r\n<p style=\"margin: 0px; font-family: &#39;Times New Roman&#39;; font-size: medium\">\r\nUntuk menampilkan hasil script PHP kita tidak bisa menampilkannya seperti hasil scrip HTML. agar bisa tampil dalam browser hasil script harus berada dibawah root folder htdocs pada server lokal anda (localhost)\r\n</p>\r\n', '', 'Minggu', '2014-08-24', '10:44:30', '79imago_php.JPG', '4', '', 'N', '0');
INSERT INTO `berita` VALUES ('6', '2', 'admin', 'Mengapa Responsive Website adalah Pilihan Terbaik untuk Strategi SEO?', '', '', '', 'mengapa-responsive-website-adalah-pilihan-terbaik-untuk-strategi-seo', 'Y', '', '', '<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\neperti yang telah kita ketahui bahwa dominasi Smartphone telah menjadi sorotan utama yang harus kita perhatikan untuk mengambil keuntungan dalam meningkatkan Strategi&nbsp;<font color=\"#0088cc\">SEO</font>.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nBanyak pembahasan sebelumnya yang saya tulis untuk menegaskan bahwa Desain Website yang Responsive memiliki keuntungan dari berbagai sisi salah satunya adalah SEO.\r\n</p>\r\n<blockquote style=\"padding: 0px 0px 0px 15px; margin: 20px 0px; border-left-width: 5px; border-left-style: solid; border-left-color: #eeeeee; color: #666666; font-style: italic; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\n	<span style=\"color: #333333\">Seperti tulisan saya sebelumnya yang berhubungan dengan topik ini yang berisi tentang&nbsp;</span><font color=\"#0088cc\">Prediksi Web Marketing di Tahun 2014</font><span style=\"color: #333333\">&nbsp;</span><span style=\"color: #333333\">yang juga mengemukakan bahwa Website Responsive adalah salah satu komponen yang terdapat didalam persaingan hebat tersebut. Apalagi jika Anda salah satu pemilik bisnis online yang mencari cara untuk meningkatkan strategi SEO Anda, Website Responsive adalah pilihan yang terbaik.</span>\r\n</blockquote>\r\n<blockquote style=\"padding: 0px 0px 0px 15px; margin: 20px 0px; border-left-width: 5px; border-left-style: solid; border-left-color: #eeeeee; color: #666666; font-style: italic; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\n	<span style=\"color: #333333\">Pengguna internet pada tahun 2014 akan mulai beralih menggunakan Smartphone kesayangan mereka untuk mencari informasi di Internet. Hal ini karena kemudahan dan mobilitas yang telah teruji, mungkin ini salah satu alasan yang membuat pasar Smartphone sangat melejit dengan berbagai karakteristik</span><span style=\"color: #333333\">&nbsp;</span><font color=\"#0088cc\">teknologi&nbsp;</font><span style=\"color: #333333\">yang ditawarkannya dari ukuran yang kecil hingga besar.</span>\r\n</blockquote>\r\n<h4 style=\"margin: 20px 0px; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-weight: 300; line-height: 20px; color: #333333; text-rendering: optimizelegibility; font-size: 17.5px\">Apakah Anda sudah menggunakan Desain Website Responsive?</h4>\r\n<blockquote style=\"padding: 0px 0px 0px 15px; margin: 20px 0px; border-left-width: 5px; border-left-style: solid; border-left-color: #eeeeee; color: #666666; font-style: italic; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\n	&nbsp;&quot;Website Responsive adalah Desain Website yang dapat digunakan secara fleksibel dibuka dari Komputer, Tablet, maupun Handphone yang menyesuaikan ukuran dan desain secara otomatis membuat pengguna lebih nyaman dengan Website Responsive&quot;.\r\n</blockquote>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nJika Anda telah menggunakan Website yang Responsive, perhatikan pada&nbsp;<em>Traffic-Source</em>&nbsp;dan juga device yang mereka gunakan. 67% diantaranya adalah menggunakan Mobile, jadi dengan memberikan desain website yang responsive akan memberikan kemudahan kepada mereka sehingga mereka &quot;ter-manjakan&quot; dan menumbuhkan fanatisme dalam membaca informasi yang ada di website atau blog Anda.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nJadi, website responsive memiliki banyak keunggulan yang menguntungkan untuk meningkatkan strategi SEO Anda.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\n<strong><span style=\"color: #c0504d\">Tapi apa alasan yang mendukung website responsive dapat meningkatkan kualitas SEO?</span></strong>\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nOk, sekarang kita fokus pada topik yang akan memberitahukan Anda tentang &quot;Mengapa Website Responsive adalah Pilihan Terbaik untuk Strategi SEO&quot;\r\n</p>\r\n<h3 style=\"margin: 20px 0px; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-weight: 300; line-height: 40px; color: #333333; text-rendering: optimizelegibility; font-size: 24.5px\">#1 - Direkomendasikan oleh Google</h3>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nAlasan pertama dan sangat penting adalah Website Responsive direkomendasikan oleh Google. Anda tahu siapa dan apa itu Google?.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nGoogle merekomendasikan pembuatan website dengan teknologi responsive dan dikonfigurasi baik tidak hanya untuk Dektop View, tetapi juga dirancang untuk Tablet dan Mobile seperti salah satu halaman Google Developer yang merekomendasikan para webmaster dan marketer untuk mengetahuinya. --<em style=\"color: #0088cc; -webkit-transition: all 0.5s ease-out; transition: all 0.5s ease-out\">Building Mobile-Optimized Websites</em><font color=\"#0088cc\">.</font>\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nRekomendasi dari Google ini karena desain website yang responsive memiliki URL yang konsisten dan juga memiliki kode HTML yang sama pula. Beda halnya dengan website yang memiliki mobile version dengan struktur dan code yang berbeda pada setiap halamannya. Hal ini yang membuat Google menilai bahwa website responsive lebih efisien untuk kebutuhan dan kemudahancrawling, indexing, dan juga pengelolaan content. Tidak seperti pada website yang memiliki versi berbeda, Google harus melakukan index dan beberapa hal lain secara berulang pada halaman yang memiliki informasi yang sama.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nDisisi lain, Google lebih mengutamakan desain website responsive karena konten dapat dengan mudah digunakan oleh user dengan URL yang konsisten seperti yang telah dijelaskan, hal ini membuat user lebih mudah untuk&nbsp;<em>sharing</em>&nbsp;di&nbsp;<font color=\"#0088cc\">social media</font>, dan juga mudah berinteraksi dengan informasi yang terkandung didalamnya.\r\n</p>\r\n<blockquote style=\"padding: 0px 0px 0px 15px; margin: 20px 0px; border-left-width: 5px; border-left-style: solid; border-left-color: #eeeeee; color: #666666; font-style: italic; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\n	&nbsp;Baca Juga :&nbsp;<font color=\"#0088cc\">Pilihan CSS Framework untuk Membuat Website Responsive</font>\r\n</blockquote>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nMari kita coba ambil beberapa contoh atau sample lebih banyak mana pengguna Social Media yang sharing atau membagi link dari website yang dibacanya dengan teman-teman yang ada di Facebook. Hal ini dapat mengoptimalkan&nbsp;<em>user-experience</em>&nbsp;terhadap ketertarikan terhadap sebuah website yang membuat Google membuat prioritas terhadap Website Responsive, tentu saja Ranking website Anda akan diperhitungkan oleh Google.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nJadi, Responsive website sudah dinyatakan oleh Google sangat direkomendasikan karena lebih optimal sehingga meningkatkan kualitas SEO.\r\n</p>\r\n<h3 style=\"margin: 20px 0px; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-weight: 300; line-height: 40px; color: #333333; text-rendering: optimizelegibility; font-size: 24.5px\">#2 - Satu Website dapat digunakan Semua devices</h3>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nSalah satu aspek menarik dari desain website responsive adalah peningkatkan kualitas website serta&nbsp;<em>user-experience</em>&nbsp;yang digunakan dari berbagai macam device dengan ukuran yang berbeda-beda. Ini adalah karakteristik yang penting untuk diketahui, apalagi kita tidak mungkin membatasi pengguna kita dengan device yang mereka gunakan.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nPengguna yang penasaran atau menggunakan multiple-device baik PC, tablet, ataupun mobile, setelah mereka mendapatkan ketertarikan positif terhadap desain responsive yang kita sajikan, tidak menutup kemungkinan mereka akan membuka dari komputer setelah mereka menggunakan dari device mobile.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nKeuntungan yang dapat kita ambil adalah&nbsp;<em>Active Visitors</em>&nbsp;yang akan menjadi pembaca setia dari setiap informasi yang disajikan.\r\n</p>\r\n<h3 style=\"margin: 20px 0px; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-weight: 300; line-height: 40px; color: #333333; text-rendering: optimizelegibility; font-size: 24.5px\">#3 - Lebih mudah dalam pengelolaan</h3>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nJika Anda masih menggunakan dua versi yang berbeda (Desktop dan Mobile) pasti Anda akan kesulitan untuk mengelola konten apalagi SEO. Hal ini akan menguras tenaga dan waktu untuk dapat mengoptimalkan website yang tidak dirancang memiliki desain responsive.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nInilah keunggulan desain website responsive, kita akan menyimpan banyak waktu dan tenaga untuk melakukan hal-hal yang seharusnya tidak lakukan secara berulang-ulang.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nDari segi pengelolaan, Anda tidak perlu menyediakan fitur untuk&nbsp;<em>device-detecting&nbsp;</em>untuk mengalihkan user ke versi mobile seperti pada website yang memiliki dua versi yang berbeda.\r\n</p>\r\n<hr />\r\n<h4 style=\"margin: 20px 0px; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-weight: 300; line-height: 20px; color: #333333; text-rendering: optimizelegibility; font-size: 17.5px\">Jadi kesimpulannya adalah</h4>\r\n<p style=\"margin: 0px 0px 10px; color: #333333; font-family: &#39;Open Sans&#39;, Arial, Helvetica, sans-serif; font-size: 14px; line-height: 25px\">\r\nDesain website responsive direkomendasikan oleh Google untuk diterapkan oleh para pengembang website (Web Developers) karena website responsive dapat memberikan ketertarikan khusus dan&nbsp;<em>user-experience</em>&nbsp;yang akan meningkatkan pengunjung aktif pada website sehingga kita dapat terus menambah pembaca setia kita dengan pengelolaan yang sangat mudah.\r\n</p>\r\n', '', 'Minggu', '2014-08-24', '11:01:56', '7240respon.jpg', '15', '', 'N', '0');
INSERT INTO `berita` VALUES ('7', '2', 'admin', 'Script PHP membuat autopost ke facebook', '', '', '', 'script-php-membuat-autopost-ke-facebook', 'N', '', '', '<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\n&nbsp; Bagaimanapun untuk mendukung dan mempublikasikan konten di Facebook sebagai saluran paralel ke website &nbsp;normal dapat meningkatkan kinerja bagi banyak perusahaan.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nNamun posting di facebook tidak harus menjadi tugas manual yang membutuhkan kerja ekstra. Postingan saya ini akan membantu Anda pada sebuah solusi yang dapat dengan mudah diintegrasikan dalam sistem CMS yang sudah ada, dan memungkinkan lintas posting konten ke Facebook.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nFacebook fan page sepertinya sekarang menjadi suatu keharusan dimiliki oleh website/portal yang mempunyai banyak fans atau penggemar.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nSelain untuk menampung para fan, facebook fan page juga berguna untuk memberikan info singkat atau berita singkat sebagai manifestasi dari berita di website/portal resmi.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nMelihat kecendrungan para pengguna facebook di indonesia yang semakin meningkat, sebagian dari mereka menggunakan facebook untuk mengupdate informasi dengan cepat.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nFacebook terus menyempurnakan SDK dan metode untuk berkomunikasi dengan Facebook. Script di posting ini mendukung perubahan otentikasi terbaru Facebook yang akan dilaksanakan 1 Oktober 2012.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nPosting ini memandu Anda melalui pembuatan aplikasi berbasis PHP Facebook yang secara otomatis dapat memposting pesan dan jenis-jenis konten di dinding Facebook Anda.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nNah, Bagaimana caranya jika kita ingin ketika memposting berita/informasi di website kita maka otomatis terposting juga di facebook fans page? tanpa repot-repot membuka facebook dan mempostingnya kembali.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nBerikut ini tutorial singkat bagaimana membuat autopost di facebook fanspage dan atau timeline facebook kita.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\n<strong style=\"margin: 0px; padding: 0px\">1.&nbsp;Download Facebook PHP SDK</strong>\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nLangkah pertama, adalah buatlah folder baru di webserver anda untuk menyimpan aplikaisi facebook kita kali ini. Misalnya nya kita beri nama folder &ldquo;facebook_sdk&rdquo;\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nDownload Facebook PHP SDK dari GitHub, unzip &nbsp;and upload ke folder &nbsp;&ldquo;facebook_sdk&rdquo; yang telah kita buat tadi .\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nfile Facebook SDK bisa anda download dari&nbsp;<a style=\"margin: 0px; padding: 0px; color: #2970a6; text-decoration: none\" href=\"https://github.com/facebook/facebook-php-sdk\" target=\"_blank\" title=\"facebook autopost\">sini</a>.<a style=\"margin: 0px; padding: 0px; color: #2970a6; text-decoration: none\" href=\"https://github.com/facebook/facebook-php-sdk\" target=\"_blank\"><br />\r\n</a>\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\n<strong style=\"margin: 0px; padding: 0px\">2. Mendaftar Aplikasi Facebook</strong>\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nFacebook memungkinkan mendaftarkan aplikasi untuk berinteraksi dengan akun Facebook dan halaman. Langkah selanjutnya adalah mendaftarkan aplikasi Anda (Jika anda belum mendaftarkannya).\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nAnda dapat mendaftarkan aplikasi Facebook baru pada<a style=\"margin: 0px; padding: 0px; color: #2970a6; text-decoration: none\" href=\"http://developers.facebook.com/setup/\" target=\"_blank\">&nbsp;halaman ini</a>\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nUntuk mendaftar aplikasi Facebook, &nbsp;Anda perlu terlebih dahulu untuk memberikan nama app yang unik.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\n<img style=\"margin: 0px; padding: 0px; border: 0px; max-width: 600px\" src=\"http://cdn.tips4php.net/wp-content/uploads/2010/12/registration_step1.jpg\" alt=\"\" title=\"Daftar Aplikasi Facebook\" width=\"550\" height=\"182\" />\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nJika semuanya OK, Anda bisa melihat &nbsp;layar konformasi pendaftaran, di mana Anda mendapatkan dua informasi penting:\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\n<strong style=\"margin: 0px; padding: 0px\">a. App ID / API Key</strong><br />\r\n<strong style=\"margin: 0px; padding: 0px\">b.&nbsp;App Secret</strong>\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nInformasi tersebut diperlukan untuk aplikasi yang akan diizinkan untuk berkomunikasi dengan Facebook, tetapi juga harus disimpan sebagai rahasia.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\n<strong style=\"margin: 0px; padding: 0px\">3. Membuat script php untuk autopost ke facebook</strong>\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nSekarang saatnya untuk membuat script PHP untuk posting ke fans page /timeline Facebook.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nsaya mengasumsikan bahwa Anda telah men-download dan sudah mengekstrak &nbsp;Facebook PHP SDK (dengan struktur file dan folder &nbsp;asli) &nbsp;dalam subfolder bernama &ldquo;facebook_sdk&rdquo; yang telah kita create di atas.\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\nBerikut ini script lengkapnya:&nbsp;\r\n</p>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\n&nbsp;\r\n</p>\r\n<div class=\"line number1 index0 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n</div>\r\n<div class=\"line number2 index1 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n//yang sudah kita dowload dari gtihub\r\n</div>\r\n<div class=\"line number3 index2 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;\r\n</div>\r\n<div class=\"line number4 index3 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\nrequire &#39;facebook_sdk/src/facebook.php&#39;;\r\n</div>\r\n<div class=\"line number5 index4 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;\r\n</div>\r\n<div class=\"line number6 index5 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n// configuration\r\n</div>\r\n<div class=\"line number7 index6 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;$appid = &#39;App Id anda&#39;;\r\n</div>\r\n<div class=\"line number8 index7 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;$appsecret = &#39;App Secret Anda&#39;&#39;;\r\n</div>\r\n<div class=\"line number9 index8 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;\r\n</div>\r\n<div class=\"line number10 index9 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n//deret angka ini &nbsp;&#39;309586582470813 &#39;&nbsp;merupakan id dari salah satu facebook fans page;\r\n</div>\r\n<div class=\"line number11 index10 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n//Anda bisa menggantinya dengan facebook id Anda sendiri atau id dari fans page lain\r\n</div>\r\n<div class=\"line number12 index11 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;\r\n</div>\r\n<div class=\"line number13 index12 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n//untuk mendapatkannya silahkan buka&nbsp;<a style=\"text-decoration: none; margin: 0px !important; padding: 0px !important; color: #008200 !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; line-height: 1.1em !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; font-size: 1em !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-image: none !important\" href=\"http://rieglerova.net/how-to-get-a-facebook-fan-page-id/\">http://rieglerova.net/how-to-get-a-facebook-fan-page-id/</a>\r\n</div>\r\n<div class=\"line number14 index13 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;$pageId = &#39;309586582470813&#39;;\r\n</div>\r\n<div class=\"line number15 index14 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;$msg = $dataProduk[&#39;judul&#39;];\r\n</div>\r\n<div class=\"line number16 index15 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;$title = $dataProduk[&#39;judul&#39;];\r\n</div>\r\n<div class=\"line number17 index16 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;$uri = &#39;<a style=\"text-decoration: none; margin: 0px !important; padding: 0px !important; color: blue !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; line-height: 1.1em !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; font-size: 1em !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-image: none !important\" href=\"http://xxxx.com/index.php?id=\">http://xxxx.com/index.php?id=</a>&#39;.$dataProduk[&#39;id&#39;];\r\n</div>\r\n<div class=\"line number18 index17 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;$desc = limit_words(strip_tags($dataProduk[&#39;deskripsi&#39;]), 25);\r\n</div>\r\n<div class=\"line number19 index18 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;$pic = &quot;<a style=\"text-decoration: none; margin: 0px !important; padding: 0px !important; color: blue !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; line-height: 1.1em !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; font-size: 1em !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-image: none !important\" href=\"http://xxxxx.com/images/products/thumbnail/t11_\">http://xxxxx.com/images/products/thumbnail/t11_</a>&quot;.$dataProduk[&#39;image1&#39;];\r\n</div>\r\n<div class=\"line number20 index19 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;\r\n</div>\r\n<div class=\"line number21 index20 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n$facebook = new Facebook(array(\r\n</div>\r\n<div class=\"line number22 index21 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;&#39;appId&#39; =&gt;$appid,\r\n</div>\r\n<div class=\"line number23 index22 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;&#39;secret&#39; =&gt;$appsecret,\r\n</div>\r\n<div class=\"line number24 index23 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;&#39;cookie&#39; =&gt; true,\r\n</div>\r\n<div class=\"line number25 index24 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;));\r\n</div>\r\n<div class=\"line number26 index25 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;\r\n</div>\r\n<div class=\"line number27 index26 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n//untuk mendapatkan access_token silahkan baca di&nbsp;<a style=\"text-decoration: none; margin: 0px !important; padding: 0px !important; color: #008200 !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; line-height: 1.1em !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; font-size: 1em !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; background-image: none !important\" href=\"http://www.damnsemicolon.com/php/auto-post-facebook-with-facebook-sdk\">http://www.damnsemicolon.com/php/auto-post-facebook-with-facebook-sdk</a>\r\n</div>\r\n<div class=\"line number28 index27 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;\r\n</div>\r\n<div class=\"line number29 index28 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;try {\r\n</div>\r\n<div class=\"line number30 index29 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;$attachment = array(\r\n</div>\r\n<div class=\"line number31 index30 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;&#39;access_token&#39; =&gt; &#39;AAAHYC3Pw0bIBAJ6b1pRZCsOQuZBbDXqyH1U4ZCMnfkxCH7B3oDbSYTjK9ihavuhg9ZA5vtUV31RALFMUm2BD5xt7y8Yxr0Vqn96EAZDZD&#39;,\r\n</div>\r\n<div class=\"line number32 index31 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;&#39;message&#39; =&gt; $msg,\r\n</div>\r\n<div class=\"line number33 index32 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;&#39;name&#39; =&gt; $title,\r\n</div>\r\n<div class=\"line number34 index33 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;&#39;link&#39; =&gt; $uri,\r\n</div>\r\n<div class=\"line number35 index34 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;&#39;description&#39; =&gt; $desc,\r\n</div>\r\n<div class=\"line number36 index35 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;&#39;picture&#39;=&gt;$pic,\r\n</div>\r\n<div class=\"line number37 index36 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;//&#39;actions&#39; =&gt; json_encode(array(&#39;name&#39; =&gt; $action_name,&#39;link&#39; =&gt; $action_link))\r\n</div>\r\n<div class=\"line number38 index37 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;);\r\n</div>\r\n<div class=\"line number39 index38 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;\r\n</div>\r\n<div class=\"line number40 index39 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n$status = $facebook-&gt;api(&quot;/$pageId/feed&quot;, &quot;post&quot;, $attachment);\r\n</div>\r\n<div class=\"line number41 index40 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;} catch (FacebookApiException $e) {\r\n</div>\r\n<div class=\"line number42 index41 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;error_log($e);\r\n</div>\r\n<div class=\"line number43 index42 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;$user = null;\r\n</div>\r\n<div class=\"line number44 index43 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;}\r\n</div>\r\n<div class=\"line number45 index44 alt2\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\n&nbsp;\r\n</div>\r\n<div class=\"line number46 index45 alt1\" style=\"line-height: 13.199999809265137px; font-family: Consolas, &#39;Bitstream Vera Sans Mono&#39;, &#39;Courier New&#39;, Courier, monospace; font-size: 12px; color: #555555; margin: 0px !important; padding: 0px 1em !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important; border: 0px !important; bottom: auto !important; float: none !important; height: auto !important; left: auto !important; outline: 0px !important; overflow: visible !important; position: static !important; right: auto !important; top: auto !important; vertical-align: baseline !important; width: auto !important; box-sizing: content-box !important; direction: ltr !important; -webkit-box-shadow: none !important; box-shadow: none !important; white-space: pre !important; background-image: none !important\">\r\necho &quot;Post to Facebook success.....&quot;;\r\n</div>\r\n<p style=\"margin: 0px 0px 10px; padding: 0px; color: #555555; font-family: Verdana, &#39;BitStream vera Sans&#39;, Helvetica, sans-serif; font-size: 12px; line-height: 17.399999618530273px\">\r\n&nbsp;\r\n</p>\r\n<pre>\r\n ?&gt;&nbsp;\r\n<p>\r\n&nbsp;\r\n</p>\r\n</pre>\r\n', '', 'Minggu', '2014-08-24', '11:09:48', '6642fb.jpg', '23', '', 'N', '0');
INSERT INTO `berita` VALUES ('12', '1', 'admin', 'Pantai Klayar, Keindahan Pesisir Pacitan', '', '', '', 'pantai-klayar-keindahan-pesisir-pacitan', 'N', 'N', 'Y', '<span style=\"font-size: 14px; line-height: 21px\"><font face=\"opensanssemibold, Arial, Helvetica, sans-serif\"><strong>Pacitan- </strong>Perjalanan<strong>&nbsp;</strong></font></span><span style=\"color: #2a2a2a; font-family: Lucida, helvetica, sans-serif; font-size: 14px; line-height: 21px\">darat menyusuri jalur lintas selatan Kabupaten Pacitan, Jawa Timur, seketika terhenti manakala pandangan tertuju pada sekerumunan wisatawan bermain air tawar di muara Pantai Klayar, salah satu destinasi wisata paling menakjubkan di daerah ini.</span><br />\r\n<br />\r\n<span style=\"color: #2a2a2a; font-family: Lucida, helvetica, sans-serif; font-size: 14px; line-height: 21px\">Niat untuk terus melaju di jalur bebas hambatan sepanjang pesisir selatan kota terpencil tempat kelahiran Presiden Susilo Bambang Yudhoyono ini pun pupus sudah.</span><br />\r\n<br />\r\n<span style=\"color: #2a2a2a; font-family: Lucida, helvetica, sans-serif; font-size: 14px; line-height: 21px\">Dengan wajah berbinar dan hati diliputi rasa penasaran, mobil diparkir darurat di tepi jalur lintas selatan dan bergegas menyusuri jalan setapak, melewati satu area parkir &rdquo;dadakan&rdquo; untuk kendaraan mobil dan sepeda motor, kemudian turun tepat di ujung muara Sungai Soge.</span><br />\r\n<br />\r\n<span style=\"color: #2a2a2a; font-family: Lucida, helvetica, sans-serif; font-size: 14px; line-height: 21px\">&rdquo;Takjub!&rdquo; Mungkin itu kata pertama yang paling pas untuk menggambarkan betapa indahnya eksotika lanskap pantai yang terbentang di kawasan Samudra Hindia itu yang berjarak 276 kilometer barat daya Kota Surabaya, ibu kota Jawa Timur.</span><br />\r\n<br />\r\n<span style=\"color: #2a2a2a; font-family: Lucida, helvetica, sans-serif; font-size: 14px; line-height: 21px\">Puluhan wisatawan lokal dari sejumlah daerah, mulai anak-anak hingga orang dewasa, begitu asyik bermain air di muara Sungai Soge yang jernih dengan pasir putihnya.</span><br />\r\n<br />\r\n<span style=\"color: #2a2a2a; font-family: Lucida, helvetica, sans-serif; font-size: 14px; line-height: 21px\">Alur muara sungai berkelok mengitari pasir putih yang membentang di tengah hingga jarak radius 150-an meter sehingga memberi ruang cukup luas bagi wisatawan untuk bermain apa saja dan menikmati keindahan wahana alami pantai.</span><br />\r\n<br />\r\n<span style=\"color: #2a2a2a; font-family: Lucida, helvetica, sans-serif; font-size: 14px; line-height: 21px\">Sementara debur ombak terus bergulung dan memecah di tepian Pantai Klayar, sekelompok wisatawan remaja lain asyik bermain sepak bola plastik atau sejenis futsal hingga puas.</span><br />\r\n<br />\r\n<span style=\"color: #2a2a2a; font-family: Lucida, helvetica, sans-serif; font-size: 14px; line-height: 21px\">Masih ada satu lagi wahana hiburan yang bisa dinikmati oleh pengunjung selain sekadar berdiri memandangi ombak di tepian pantai, bermain air, ataupun sekadar nongkrong di bawah pohon kelapa sembari menikmati sajian es kelapa muda. Wahana itu adalah &rdquo;danau kecil&rdquo; yang terbentuk karena air laut tidak sepenuhnya tertarik ke tengah laut saat terjadi fenomena air surut, dan terpisah oleh gundukan pasir putih yang membentang di tengah.</span>\r\n', '', 'Minggu', '2014-08-24', '11:30:36', '70kal.jpg', '10', '', 'N', '0');
INSERT INTO `berita` VALUES ('13', '1', 'admin', 'Pesisir Pacitan Surganya Wisata Pantai', '', '', '', 'pesisir-pacitan-surganya-wisata-pantai', 'N', '', '', '<span style=\"color: #2a2a2a; font-family: Lucida, helvetica, sans-serif; font-size: 14px; line-height: 21px\">Menurut data dinas pariwisata setempat, total ada 17 wahana wisata pantai di sepanjang pesisir selatan Pacitan yang lokasinya dilewati atau berdekatan dengan jalur lintas selatan. Mulai dari Pantai Banyu Tibo, Buyutan, Karang Bolong, Srau, Klayar, dan Watu Karung yang ada di sebelah barat Kota Pacitan, Pantai Teleng Ria, Tamperan Gung, Kali Uluh, dan Wawaran yang ada di sekitar Kota Pacitan, hingga Pantai Pidakan, Soge, Tawang, Taman, dan Kunir yang ada di sebelah timur daerah ini.</span><br />\r\n<br />\r\n<span style=\"color: #2a2a2a; font-family: Lucida, helvetica, sans-serif; font-size: 14px; line-height: 21px\">Setiap pantai memiliki panorama khas yang akan membuat siapa saja yang berkunjung ke daerah ini berdecak kagum. Sebab, pada dasarnya, pantai-pantai di daerah ini masih sangat alami, atau orang menyebutnya sebagai destinasi wisata yang masih â€perawanâ€.</span><br />\r\n<br />\r\n<span style=\"color: #2a2a2a; font-family: Lucida, helvetica, sans-serif; font-size: 14px; line-height: 21px\">Bahkan, banyak orang Pacitan sendiri, konon, belum mengetahui atau menginjakkan kaki di sebagian besar obyek wisata pantai tersebut karena lokasinya, dulu belum memiliki akses kendaraan.</span><br />\r\n<br />\r\n<span style=\"color: #2a2a2a; font-family: Lucida, helvetica, sans-serif; font-size: 14px; line-height: 21px\">Surga di belahan pesisir selatan Kabupaten Pacitan yang terletak di ujung barat daya Jawa Timur ini baru terkuak dan menjadi perbincangan banyak bloger dan wisatawan setelah pemerintah membangun jalur lintas selatan sepanjang 70-an kilometer di daerah ini, mulai dari Kabupaten Wonogiri (Jawa Tengah) di sisi barat hingga Kabupaten Trenggalek yang ada di ujung timur Pacitan.</span><br />\r\n<br />\r\n<span style=\"color: #2a2a2a; font-family: Lucida, helvetica, sans-serif; font-size: 14px; line-height: 21px\">Mereka yang ingin sekadar menjelajah di jalur cincin pantai Pacitan ini hanya butuh waktu sekitar 1,5 jam untuk mencapainya.</span><br />\r\n<br />\r\n<span style=\"color: #2a2a2a; font-family: Lucida, helvetica, sans-serif; font-size: 14px; line-height: 21px\">Menuju Pantai Soge dari Pacitan, kota berjarak sekitar 24 kilometer yang mampu ditempuh kendaraan umum (bus) sekitar 30 menit, sementara dari Surabaya waktu tempuh sekitar 7 jam, dari Tulungagung 3,5 jam, sedangkan dari Solo 4 jam.</span><br />\r\n<br />\r\n<span style=\"color: #2a2a2a; font-family: Lucida, helvetica, sans-serif; font-size: 14px; line-height: 21px\">Namun, jika ingin sekaligus menikmati setiap destinasi pantai berikut aneka panorama keindahan lainnya, kebutuhan waktu bergantung pada setiap pelancong untuk mengatur ritme perjalanan masing-masing.</span><br />\r\n<br />\r\n', '', 'Minggu', '2014-08-24', '11:36:24', '83soge2.jpg', '46', '', 'N', '0');
INSERT INTO `berita` VALUES ('18', '0', 'admin', '', '', '', '', '', 'N', '', '', '', '', 'Senin', '2016-01-04', '03:48:49', '', '0', '', 'N', '0');

-- ----------------------------
-- Table structure for blogsiswa
-- ----------------------------
DROP TABLE IF EXISTS `blogsiswa`;
CREATE TABLE `blogsiswa` (
  `id_blogsiswa` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `kelas` varchar(40) COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `screenshoot` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `hit` int(5) NOT NULL,
  `tgl_posting` date NOT NULL,
  PRIMARY KEY (`id_blogsiswa`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of blogsiswa
-- ----------------------------
INSERT INTO `blogsiswa` VALUES ('1', 'Blog Imago Media', '3 IPA 1', 'http://blog.imagomedia.co.id', 'imago.jpg', '55', '2011-06-26');
INSERT INTO `blogsiswa` VALUES ('12', 'Lukman ', '2 IPA 2', 'http://bukulokomedia.com', '229980lokomedia.JPG', '0', '2014-09-06');
INSERT INTO `blogsiswa` VALUES ('13', 'Agus Hariyanto', '3 IPS 1', 'http://imagomedia.co.id', '694610web_imago.JPG', '0', '2014-09-06');

-- ----------------------------
-- Table structure for bukutamu
-- ----------------------------
DROP TABLE IF EXISTS `bukutamu`;
CREATE TABLE `bukutamu` (
  `id_bukutamu` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `subjek` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `pesan` text COLLATE latin1_general_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `dibaca` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id_bukutamu`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of bukutamu
-- ----------------------------
INSERT INTO `bukutamu` VALUES ('2', 'Niken', 'azzamrabbanirizaldi@gmail.com', 'tes', 'Masukkan pesan anda sebagai tamu\r\n', '2012-05-12', '00:00:00', 'Y');
INSERT INTO `bukutamu` VALUES ('3', 'Agus Hariyanto', 'a_printgoes@yahoo.com', '', 'Masukkan Ide Anda pada form input buku tamu', '2014-08-20', '00:00:00', 'N');
INSERT INTO `bukutamu` VALUES ('57', 'Agus Hariyanto', 'agus_pacitan@yahoo.co.id', '', 'Coba komentar', '2015-01-04', '00:00:00', 'N');
INSERT INTO `bukutamu` VALUES ('7', 'Ratna Rahmawati', 'a_printgoes@yahoo.com', '', 'Masukkan Ide Anda pada form input buku tamu', '2014-08-20', '00:00:00', 'N');
INSERT INTO `bukutamu` VALUES ('9', 'Tris Santosa', 'a_printgoes@yahoo.com', '', 'Tempat input buku tamu pada PPDB Imago Media 2015', '2014-08-20', '00:00:00', 'N');
INSERT INTO `bukutamu` VALUES ('10', 'Aru Mohamad', 'agus_pacitan@yahoo.co.id', '', 'Buku tamu PPDB ONline', '2014-08-20', '00:00:00', 'N');
INSERT INTO `bukutamu` VALUES ('4', 'Yusuf Achsanul Azzam', 'a_printgoes@yahoo.com', '', 'Tempat input buku tamu pada PPDB Imago Media 2015', '2014-08-20', '00:00:00', 'N');
INSERT INTO `bukutamu` VALUES ('5', 'Setyawati Dwi Susanti', 'agus_pacitan@yahoo.co.id', '', 'Buku tamu PPDB ONline', '2014-08-20', '00:00:00', 'N');
INSERT INTO `bukutamu` VALUES ('58', 'Syamsul', 'imamsyr@yahoo.co.id', '', 'Coba Kirim Buku tamu', '2015-01-31', '00:00:00', 'N');

-- ----------------------------
-- Table structure for content
-- ----------------------------
DROP TABLE IF EXISTS `content`;
CREATE TABLE `content` (
  `id_content` int(5) NOT NULL AUTO_INCREMENT,
  `nama_content` varchar(50) NOT NULL,
  `code` text NOT NULL,
  `posisi` int(5) NOT NULL,
  `aktif` enum('Y','N') NOT NULL,
  PRIMARY KEY (`id_content`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of content
-- ----------------------------
INSERT INTO `content` VALUES ('1', 'slider', 'slider.php', '1', 'Y');
INSERT INTO `content` VALUES ('2', 'artikel', 'artikel.php', '2', 'Y');
INSERT INTO `content` VALUES ('13', 'Gallery Photo', 'galeri.php', '1', 'Y');
INSERT INTO `content` VALUES ('12', 'Pengumuman', 'pengumuman.php', '5', 'N');
INSERT INTO `content` VALUES ('11', 'Agenda', 'agenda.php', '2', 'Y');
INSERT INTO `content` VALUES ('10', 'Video', 'video.php', '4', 'N');
INSERT INTO `content` VALUES ('14', 'Berita Terbaru', 'berita.php', '2', 'Y');

-- ----------------------------
-- Table structure for cse
-- ----------------------------
DROP TABLE IF EXISTS `cse`;
CREATE TABLE `cse` (
  `id_cse` int(5) NOT NULL AUTO_INCREMENT,
  `search_box` text NOT NULL,
  `search_result` text NOT NULL,
  PRIMARY KEY (`id_cse`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cse
-- ----------------------------
INSERT INTO `cse` VALUES ('1', '<script>\r\n  (function() {\r\n    var cx = \'007830500037901093298:z0uuh1jkeyy\';\r\n    var gcse = document.createElement(\'script\');\r\n    gcse.type = \'text/javascript\';\r\n    gcse.async = true;\r\n    gcse.src = (document.location.protocol == \'https:\' ? \'https:\' : \'http:\') +\r\n        \'//www.google.com/cse/cse.js?cx=\' + cx;\r\n    var s = document.getElementsByTagName(\'script\')[0];\r\n    s.parentNode.insertBefore(gcse, s);\r\n  })();\r\n</script>\r\n<gcse:searchbox-only></gcse:searchbox-only>', '<script>\r\n  (function() {\r\n    var cx = \'007830500037901093298:z0uuh1jkeyy\';\r\n    var gcse = document.createElement(\'script\');\r\n    gcse.type = \'text/javascript\';\r\n    gcse.async = true;\r\n    gcse.src = (document.location.protocol == \'https:\' ? \'https:\' : \'http:\') +\r\n        \'//www.google.com/cse/cse.js?cx=\' + cx;\r\n    var s = document.getElementsByTagName(\'script\')[0];\r\n    s.parentNode.insertBefore(gcse, s);\r\n  })();\r\n</script>\r\n<gcse:searchresults-only></gcse:searchresults-only>');

-- ----------------------------
-- Table structure for daya_tampung
-- ----------------------------
DROP TABLE IF EXISTS `daya_tampung`;
CREATE TABLE `daya_tampung` (
  `id_daya_tampung` int(1) NOT NULL,
  `kapasitas` varchar(5) NOT NULL,
  `nilai_minimal` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of daya_tampung
-- ----------------------------
INSERT INTO `daya_tampung` VALUES ('1', '2', '170');

-- ----------------------------
-- Table structure for fasilitas
-- ----------------------------
DROP TABLE IF EXISTS `fasilitas`;
CREATE TABLE `fasilitas` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `fasilitas` varchar(50) DEFAULT NULL,
  `nama_fasilitas` varchar(50) DEFAULT NULL,
  `jumlah` int(1) DEFAULT NULL,
  `lokasi` varchar(100) DEFAULT NULL,
  `deskripsi` text,
  `foto` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of fasilitas
-- ----------------------------

-- ----------------------------
-- Table structure for gallery
-- ----------------------------
DROP TABLE IF EXISTS `gallery`;
CREATE TABLE `gallery` (
  `id_gallery` int(5) NOT NULL AUTO_INCREMENT,
  `id_album` int(5) NOT NULL,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `jdl_gallery` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `gallery_seo` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `keterangan` text COLLATE latin1_general_ci NOT NULL,
  `gbr_gallery` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_gallery`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of gallery
-- ----------------------------
INSERT INTO `gallery` VALUES ('11', '0', 'admin', 'Kegiatan Belajar Bahasa Asing', 'kegiatan-belajar-bahasa-asing', '', '6920140603_131124.jpg');
INSERT INTO `gallery` VALUES ('10', '0', 'admin', 'Toilet Asrama', 'toilet-asrama', '', '934826812_20160219102616.jpg');
INSERT INTO `gallery` VALUES ('9', '0', 'admin', 'Kamar Rumah UINAM', 'kamar-rumah-uinam', '', '38DM.1.jpg');
INSERT INTO `gallery` VALUES ('4', '0', 'admin', 'Pantai Watukarung', 'pantai-watukarung', '', '70watu karung.jpg');
INSERT INTO `gallery` VALUES ('8', '0', 'admin', 'Asrama Aly', 'asrama-aly', '', '34mahad aly.jpg');
INSERT INTO `gallery` VALUES ('7', '0', 'admin', 'Asrama Rusunawa', 'asrama-rusunawa', '', '45UIN-Rusunawa-UIN-Alauddin-Makassar.jpg');
INSERT INTO `gallery` VALUES ('12', '0', 'admin', 'Asrama Rumah UINAM', 'asrama-rumah-uinam', '', '67UIN Alauddin Makassar 254530462 IMG_20150917_091438.jpg');

-- ----------------------------
-- Table structure for halamanstatis
-- ----------------------------
DROP TABLE IF EXISTS `halamanstatis`;
CREATE TABLE `halamanstatis` (
  `id_halaman` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `judul_seo` varchar(100) NOT NULL,
  `isi_halaman` text NOT NULL,
  `tgl_posting` date NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `dibaca` int(5) NOT NULL DEFAULT '1',
  `jam` time NOT NULL,
  `hari` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_halaman`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of halamanstatis
-- ----------------------------
INSERT INTO `halamanstatis` VALUES ('1', 'Profil Singkat', 'profil-singkat', 'isi ki juga profil singkatnya\r\n', '2012-03-08', '66lambang.png', 'admin', '767', '20:08:00', 'Kamis');
INSERT INTO `halamanstatis` VALUES ('3', 'Visi Misi', 'visi-misi', 'Visi dan Misi Asrama Mahasiswa\r\n', '2014-08-19', '1UIN Alauddin Makassar 254530462 IMG_20150917_091438.jpg', 'admin', '51', '14:35:33', 'Selasa');
INSERT INTO `halamanstatis` VALUES ('4', 'Alur Pendaftaran', 'alur-pendaftaran', '', '2014-08-19', '98alur.png', 'admin', '15', '23:56:05', 'Selasa');
INSERT INTO `halamanstatis` VALUES ('5', 'halam sembarang', 'halam-sembarang', 'buat sendiri mi halaman nya\r\n', '2014-08-20', '', 'admin', '54', '00:08:26', 'Rabu');
INSERT INTO `halamanstatis` VALUES ('2', 'Sejarah Rumah UINAM', 'sejarah-rumah-uinam', 'Disini isi sejarah uinam isi ki darul\r\n', '2014-08-19', '13history.jpg', '', '21', '00:00:00', '');

-- ----------------------------
-- Table structure for hasil_tes
-- ----------------------------
DROP TABLE IF EXISTS `hasil_tes`;
CREATE TABLE `hasil_tes` (
  `id_hasil` int(5) NOT NULL AUTO_INCREMENT,
  `id_pendaftaran` int(10) NOT NULL,
  `skhun` varchar(5) NOT NULL,
  `tulis` varchar(5) NOT NULL,
  `wawancara` varchar(5) NOT NULL,
  `total` varchar(10) NOT NULL,
  PRIMARY KEY (`id_hasil`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hasil_tes
-- ----------------------------
INSERT INTO `hasil_tes` VALUES ('1', '1', '90', '80', '60', '230');
INSERT INTO `hasil_tes` VALUES ('2', '2', '70', '70', '70', '210');
INSERT INTO `hasil_tes` VALUES ('3', '3', '70', '70', '70', '210');
INSERT INTO `hasil_tes` VALUES ('4', '4', '70', '40', '50', '160');
INSERT INTO `hasil_tes` VALUES ('5', '0', '', '', '', '0');

-- ----------------------------
-- Table structure for hubungi
-- ----------------------------
DROP TABLE IF EXISTS `hubungi`;
CREATE TABLE `hubungi` (
  `id_hubungi` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `subjek` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `pesan` text COLLATE latin1_general_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `dibaca` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id_hubungi`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of hubungi
-- ----------------------------
INSERT INTO `hubungi` VALUES ('46', 'Sofyan Wibowo', 'aprintgoes82@gmail.com', 'Ajax login', 'fdsfdsdf', '2014-08-20', '00:00:00', 'N');
INSERT INTO `hubungi` VALUES ('54', 'Syamsul', 'agus_pacitan@yahoo.co.id', 'Percobaan', 'cobae69909', '2015-01-31', '00:00:00', 'N');

-- ----------------------------
-- Table structure for identitas
-- ----------------------------
DROP TABLE IF EXISTS `identitas`;
CREATE TABLE `identitas` (
  `id_identitas` int(5) NOT NULL AUTO_INCREMENT,
  `nama_website` varchar(100) NOT NULL,
  `pembuka` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `facebook` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `google` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `meta_deskripsi` varchar(250) NOT NULL,
  `meta_keyword` varchar(250) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  PRIMARY KEY (`id_identitas`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of identitas
-- ----------------------------
INSERT INTO `identitas` VALUES ('1', 'Imago Media | PPDB Online', 'Selamat Datang Pada Sekolah Masa Kini', './', 'sdsfdsfdsd', 'https://www.facebook.com/pages/Imago-MEdia/342949069169274', '109259042428517677618', 'sadasasda', 'aASAsasasda', ' berita, info, politik, nasional, budaya, sepakbola, hiburan, kuliner, dunia islam', 'logo.png');

-- ----------------------------
-- Table structure for info
-- ----------------------------
DROP TABLE IF EXISTS `info`;
CREATE TABLE `info` (
  `id_info` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `judul_seo` varchar(100) NOT NULL,
  `isi_info` text NOT NULL,
  `tgl_posting` date NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `jam` time NOT NULL,
  `icon` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_info`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of info
-- ----------------------------
INSERT INTO `info` VALUES ('1', 'Jadwal PPDB', 'jadwal-ppdb', '<p>\r\nPenerimaan Peserta Didik Baru Dilaksanakan pada :\r\n</p>\r\n<p>\r\nGelombang 1 : 1-7 Juli 2015\r\n</p>\r\n<p>\r\nGelombang II : 8-10 Juli 2015&nbsp;\r\n</p>\r\n<p>\r\nGelombang III : 11-12 Juli 2015.\r\n</p>\r\n', '2012-03-08', '14logo.jpg', 'admin', '20:08:00', 'jadwal.png');
INSERT INTO `info` VALUES ('3', 'Aturan PPDB', 'aturan-ppdb', 'Atruan PPDB Online\r\n', '2014-08-19', '', 'admin', '14:35:33', 'aturan.png');
INSERT INTO `info` VALUES ('4', 'Ketentuan Lain', 'ketentuan-lain', '', '2014-08-19', '', 'admin', '23:56:05', 'lain.png');
INSERT INTO `info` VALUES ('2', 'Alur PPDB', 'alur-ppdb', '<p>\r\nAlur PPDB\r\n</p>\r\n<p>\r\n&nbsp;\r\n</p>\r\n', '2014-08-19', '', '', '00:00:00', 'alur.png');

-- ----------------------------
-- Table structure for katajelek
-- ----------------------------
DROP TABLE IF EXISTS `katajelek`;
CREATE TABLE `katajelek` (
  `id_jelek` int(11) NOT NULL AUTO_INCREMENT,
  `kata` varchar(60) COLLATE latin1_general_ci DEFAULT NULL,
  `ganti` varchar(60) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_jelek`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of katajelek
-- ----------------------------
INSERT INTO `katajelek` VALUES ('4', 'sex', 's**');
INSERT INTO `katajelek` VALUES ('2', 'bajingan', 'b*******');
INSERT INTO `katajelek` VALUES ('3', 'bangsat', 'b******');

-- ----------------------------
-- Table structure for kategori
-- ----------------------------
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori` (
  `id_kategori` int(5) NOT NULL AUTO_INCREMENT,
  `id_parent` int(5) NOT NULL,
  `nama_kategori` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `kategori_seo` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `diklik` int(5) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of kategori
-- ----------------------------
INSERT INTO `kategori` VALUES ('1', '101', 'Berita', 'admin', 'berita', 'Y', '195');
INSERT INTO `kategori` VALUES ('2', '105', 'Artikel', 'admin', 'artikel', 'Y', '200');
INSERT INTO `kategori` VALUES ('3', '105', 'Video', 'admin', 'video', 'Y', '153');
INSERT INTO `kategori` VALUES ('4', '105', 'Karya Siswa', 'admin', 'karya-siswa', 'Y', '153');

-- ----------------------------
-- Table structure for komentar
-- ----------------------------
DROP TABLE IF EXISTS `komentar`;
CREATE TABLE `komentar` (
  `id_komentar` int(5) NOT NULL AUTO_INCREMENT,
  `id_berita` int(5) NOT NULL,
  `nama_komentar` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `url` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `isi_komentar` text COLLATE latin1_general_ci NOT NULL,
  `tgl` date NOT NULL,
  `jam_komentar` time NOT NULL,
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_komentar`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of komentar
-- ----------------------------
INSERT INTO `komentar` VALUES ('2', '7', 'jojo kampret', '', 'Pasti  Bisa   \r\n', '2015-01-04', '21:37:25', 'Y', 'agus_pacitan@yahoo.co.id');
INSERT INTO `komentar` VALUES ('3', '14', 'Agus Hariyanto', '', 'Pemandangan Yang Bagus', '2015-01-31', '09:31:20', 'Y', 'agus_pacitan@yahoo.co.id');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id_menu` int(5) NOT NULL AUTO_INCREMENT,
  `nama_menu` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `atribut` text NOT NULL,
  `link` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `aktif` enum('Y','N') NOT NULL DEFAULT 'Y',
  `adminmenu` enum('Y','N') NOT NULL,
  `parent` enum('Y','N') NOT NULL,
  `top` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id_menu`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('8', 'Informasi', 'background:#338aa6;color:#338aa6;', '#', 'Y', 'Y', 'Y', 'N');
INSERT INTO `menu` VALUES ('7', 'Profil', 'background:#9f3819;color:#9f3819;', '#', 'Y', 'Y', 'Y', 'N');
INSERT INTO `menu` VALUES ('9', 'Fasilitas', 'background:#6d8b13;color:#6d8b13;', '#', 'Y', 'Y', 'Y', 'N');
INSERT INTO `menu` VALUES ('10', 'Pendaftaran Online', 'background:#264c84;color:#264c84;', '#', 'Y', 'Y', 'Y', 'N');
INSERT INTO `menu` VALUES ('11', 'Interaksi', 'background:#FF6600;color:#FF6600;', '#', 'N', 'Y', 'Y', 'N');
INSERT INTO `menu` VALUES ('12', 'Galeri Photo', 'background:#6d8b13;color:#6d8b13;', 'galeri-photo.html', 'Y', 'Y', 'N', 'N');

-- ----------------------------
-- Table structure for pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE `pembayaran` (
  `no_kwitansi` varchar(15) NOT NULL,
  `id_mahasiswa` varchar(15) DEFAULT NULL,
  `jumlah_bulan` int(2) DEFAULT NULL,
  `tanggal_transaksi` date DEFAULT NULL,
  `jumlah_bayar` bigint(255) DEFAULT NULL,
  `tahun` char(4) DEFAULT NULL,
  PRIMARY KEY (`no_kwitansi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembayaran
-- ----------------------------
INSERT INTO `pembayaran` VALUES ('2016/1/KOSUIN', '1', '12', '2016-08-09', '6000', '2016');
INSERT INTO `pembayaran` VALUES ('2016/3/KOSUIN', '3', '6', '2016-08-09', '2000', '2016');

-- ----------------------------
-- Table structure for pendaftaran
-- ----------------------------
DROP TABLE IF EXISTS `pendaftaran`;
CREATE TABLE `pendaftaran` (
  `id_pendaftaran` int(10) NOT NULL AUTO_INCREMENT,
  `kontrak` int(1) NOT NULL,
  `nim` char(11) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `email` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tempat` varchar(20) DEFAULT NULL,
  `agama` varchar(10) DEFAULT NULL,
  `jenis_kelamin` enum('L','P') DEFAULT NULL,
  `fakultas` varchar(50) NOT NULL,
  `jurusan` varchar(50) NOT NULL,
  `semester` int(1) NOT NULL,
  `asal_sekolah` varchar(50) NOT NULL,
  `wali` varchar(25) NOT NULL,
  `pekerjaan` varchar(25) NOT NULL,
  `telp` varchar(15) NOT NULL,
  `tgl_pendaftaran` date NOT NULL,
  `status` enum('B','S') NOT NULL,
  PRIMARY KEY (`id_pendaftaran`),
  KEY `agama` (`agama`),
  KEY `agama_2` (`agama`),
  KEY `agama_3` (`agama`),
  KEY `agama_4` (`agama`),
  KEY `agama_5` (`agama`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pendaftaran
-- ----------------------------
INSERT INTO `pendaftaran` VALUES ('6', '6', '60900111002', 'baso', 'immim', '2016-07-12', 'asd@yahoo.com', 'sorowako', 'islam', 'L', 'sains dan teknologi', 'sistem informasi', '1', 'immim', 'ismi', 'bangunan', '080283', '2016-07-12', 'B');
INSERT INTO `pendaftaran` VALUES ('5', '12', '60900111001', 'jojo', 'immim', '1994-06-28', 'adhi@yahoo.com', 'makassar', 'islam', 'L', 'sains dan teknologi', 'sistem informasi', '2', 'immim', 'yusuf', 'guru', '081231', '2016-07-12', 'B');
INSERT INTO `pendaftaran` VALUES ('4', '12', '60900111001', 'jojo', 'immim', '1994-06-28', 'adhi@yahoo.com', 'makassar', 'islam', 'L', 'sains dan teknologi', 'sistem informasi', '2', 'immim', 'yusuf', 'guru', '081231', '2016-07-12', 'B');
INSERT INTO `pendaftaran` VALUES ('3', '12', '60900111001', 'dulle', 'immim', '1994-06-28', 'adhi@yahoo.com', 'makassar', 'islam', 'L', 'sains dan teknologi', 'sistem informasi', '2', 'immim', 'yusuf', 'guru', '081231', '2016-07-12', 'S');
INSERT INTO `pendaftaran` VALUES ('2', '12', '60900111001', 'darul', 'immim', '1994-06-28', 'adhi@yahoo.com', 'makassar', 'islam', 'L', 'sains dan teknologi', 'sistem informasi', '2', 'immim', 'yusuf', 'guru', '081231', '2016-07-12', 'B');
INSERT INTO `pendaftaran` VALUES ('1', '12', '60900111001', 'aco', 'immim', '1994-06-28', 'adhi@yahoo.com', 'makassar', 'islam', 'P', 'sains dan teknologi', 'sistem informasi', '2', 'immim', 'yusuf', 'guru', '081231', '2016-07-12', 'S');
INSERT INTO `pendaftaran` VALUES ('8', '12', '60900111001', 'sinta', 'immim', '1994-06-28', 'adhi@yahoo.com', 'makassar', 'islam', 'P', 'sains dan teknologi', 'sistem informasi', '2', 'immim', 'yusuf', 'guru', '081231', '2016-07-12', 'B');
INSERT INTO `pendaftaran` VALUES ('10', '0', '897', 'kjh', 'lkj', '2016-01-01', 'adhiariyadi@gmail.com', 'kjh', 'islam', 'L', 'Sains dan Teknologi', 'sistem informasi', '2', '3', 'hj', '', '98798', '2016-08-10', 'B');
INSERT INTO `pendaftaran` VALUES ('11', '0', '45645', 'sundala', 'df', '2016-01-01', 'adhiariyadi@gmail.com', 'laso', 'islam', 'P', 'Sains dan Teknologi', 'teknik informatika', '2', '3', 'd', '', '34', '2016-08-11', 'B');
INSERT INTO `pendaftaran` VALUES ('12', '0', '234', 'jojo', 'asd', '2016-01-01', 'adhiariyadi@gmail.com', 'makassar', 'islam', 'P', 'Sains dan Teknologi', 'teknik informatika', '1', '3', 'asd', '', '3234', '2016-08-12', 'B');

-- ----------------------------
-- Table structure for pengumuman
-- ----------------------------
DROP TABLE IF EXISTS `pengumuman`;
CREATE TABLE `pengumuman` (
  `id_pengumuman` int(5) NOT NULL AUTO_INCREMENT,
  `tema` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `tema_seo` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `isi_pengumuman` text COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `tgl_posting` date NOT NULL,
  `dibaca` int(5) NOT NULL DEFAULT '1',
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_pengumuman`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of pengumuman
-- ----------------------------
INSERT INTO `pengumuman` VALUES ('67', 'Kalender Pendidikan 2015', 'kalender-pendidikan-2015', '<div>\r\nDalam implementasi Kurikulum 2013 ada beberapa perangkat pembelajaran yang sudah dibuatkan pusat dan ada perangkat pembelajaran yang harus dikembangkan guru sendiri. Tahun ajaran baru harus disambut dengan semangat baru, dengan perencanaan serta target yang ingin dicapai.\r\n</div>\r\n<div>\r\n<br />\r\n</div>\r\n<div>\r\nKalender pendidikan tahun pelajaran 2014/2015 sebagai dasar panduan atau dasar dalam proses pembelajaran. Dapat diketahui waktu pelaksanaan ulangan semester, libur semester, dan hari efektif.\r\n</div>\r\n<div>\r\n<br />\r\n</div>\r\n<div>\r\n<br />\r\n</div>\r\n', '989440kaee.jpg', '2014-08-24', '1', 'admin');
INSERT INTO `pengumuman` VALUES ('66', 'Jadwal Pelaksanaan Tes Peserta Didik Baru', 'jadwal-pelaksanaan-tes-peserta-didik-baru', 'Tes akan dilaksanakan pada tanggal 17-19 Agustus 2015\r\n', '278167buku.jpg', '2014-08-19', '1', 'admin');

-- ----------------------------
-- Table structure for sekilasinfo
-- ----------------------------
DROP TABLE IF EXISTS `sekilasinfo`;
CREATE TABLE `sekilasinfo` (
  `id_sekilas` int(5) NOT NULL AUTO_INCREMENT,
  `info` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `tgl_posting` date NOT NULL,
  `gambar` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `aktif` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id_sekilas`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of sekilasinfo
-- ----------------------------
INSERT INTO `sekilasinfo` VALUES ('1', 'Anak yang mengalami gangguan tidur, cenderung memakai obat2an dan alkohol berlebih saat dewasa.', '2010-04-11', '', 'Y');
INSERT INTO `sekilasinfo` VALUES ('2', 'WHO merilis, 30 persen anak-anak di dunia kecanduan menonton televisi dan bermain komputer.', '0000-00-00', '', 'Y');
INSERT INTO `sekilasinfo` VALUES ('3', 'Menurut peneliti di Detroit, orang yang selalu tersenyum lebar cenderung hidup lebih lama.', '2010-04-11', '', 'Y');
INSERT INTO `sekilasinfo` VALUES ('4', 'Menurut United Stated Trade Representatives, 25% obat yang beredar di Indonesia adalah palsu.', '2010-04-11', '', 'Y');

-- ----------------------------
-- Table structure for sekolah
-- ----------------------------
DROP TABLE IF EXISTS `sekolah`;
CREATE TABLE `sekolah` (
  `id_sekolah` int(5) NOT NULL AUTO_INCREMENT,
  `nama_sekolah` varchar(100) NOT NULL,
  `sekolah_seo` varchar(100) NOT NULL,
  `alamat_sekolah` varchar(100) NOT NULL,
  `aktif` enum('Y','N') NOT NULL,
  PRIMARY KEY (`id_sekolah`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sekolah
-- ----------------------------
INSERT INTO `sekolah` VALUES ('1', 'SLTPN 1 Pacitan ', '', 'Jl. Gatot Subroto 8 Pacitan', 'Y');
INSERT INTO `sekolah` VALUES ('2', 'MTS N Pacitan', '', 'Jalan Patimura 16 Pacitan', 'Y');
INSERT INTO `sekolah` VALUES ('3', 'SLTPN 2 Pringkuku', 'sltpn-2-pringkuku', '', 'Y');
INSERT INTO `sekolah` VALUES ('4', 'SLTPN 2 Ngadirojo', 'sltpn-2-ngadirojo', '', 'Y');

-- ----------------------------
-- Table structure for sidebar
-- ----------------------------
DROP TABLE IF EXISTS `sidebar`;
CREATE TABLE `sidebar` (
  `id_sidebar` int(5) NOT NULL AUTO_INCREMENT,
  `nama_sidebar` varchar(50) NOT NULL,
  `code` text NOT NULL,
  `posisi` int(5) NOT NULL,
  `aktif` enum('Y','N') NOT NULL,
  PRIMARY KEY (`id_sidebar`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sidebar
-- ----------------------------
INSERT INTO `sidebar` VALUES ('1', 'cse', 'cse.php', '1', 'Y');
INSERT INTO `sidebar` VALUES ('2', 'facebook', 'facebook.php', '11', 'N');
INSERT INTO `sidebar` VALUES ('3', 'Tag Cloud', 'tag_cloud.php', '2', 'Y');
INSERT INTO `sidebar` VALUES ('4', 'Link Terkait', 'banner.php', '13', 'Y');
INSERT INTO `sidebar` VALUES ('5', 'agenda', 'agenda.php', '6', 'Y');
INSERT INTO `sidebar` VALUES ('7', 'Video', 'video.php', '2', 'Y');
INSERT INTO `sidebar` VALUES ('8', 'Berita terpopoler', 'berita_terpopuler.php', '9', 'Y');
INSERT INTO `sidebar` VALUES ('9', 'Coba Ah', 'coba.php', '1', 'N');
INSERT INTO `sidebar` VALUES ('10', 'Artikel Terpopuler', 'artikel_terpopuler.php', '8', 'N');
INSERT INTO `sidebar` VALUES ('11', 'Video Terpopuler', 'video_terpopuler.php', '10', 'Y');

-- ----------------------------
-- Table structure for statistik
-- ----------------------------
DROP TABLE IF EXISTS `statistik`;
CREATE TABLE `statistik` (
  `ip` varchar(20) NOT NULL DEFAULT '',
  `tanggal` date NOT NULL,
  `hits` int(10) NOT NULL DEFAULT '1',
  `online` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of statistik
-- ----------------------------
INSERT INTO `statistik` VALUES ('127.0.0.2', '2009-09-11', '1', '1252681630');
INSERT INTO `statistik` VALUES ('127.0.0.1', '2013-01-22', '1', '1358865974');
INSERT INTO `statistik` VALUES ('127.0.0.1', '2013-01-23', '14', '1358913313');
INSERT INTO `statistik` VALUES ('127.0.0.1', '2013-01-24', '34', '1359046647');
INSERT INTO `statistik` VALUES ('127.0.0.1', '2013-01-25', '21', '1359051663');
INSERT INTO `statistik` VALUES ('::1', '2014-04-12', '1', '1397303636');
INSERT INTO `statistik` VALUES ('::1', '2014-04-19', '1', '1397877887');
INSERT INTO `statistik` VALUES ('::1', '2014-04-20', '2', '1397996058');
INSERT INTO `statistik` VALUES ('::1', '2014-04-21', '23', '1398099595');
INSERT INTO `statistik` VALUES ('::1', '2014-04-22', '15', '1398134483');
INSERT INTO `statistik` VALUES ('::1', '2014-04-24', '3', '1398346913');
INSERT INTO `statistik` VALUES ('::1', '2014-04-27', '1', '1398583150');
INSERT INTO `statistik` VALUES ('::1', '2014-04-28', '1', '1398700791');
INSERT INTO `statistik` VALUES ('::1', '2014-04-29', '2', '1398787607');
INSERT INTO `statistik` VALUES ('::1', '2014-04-30', '1', '1398866805');
INSERT INTO `statistik` VALUES ('::1', '2014-05-01', '1', '1398907424');
INSERT INTO `statistik` VALUES ('::1', '2014-05-03', '1', '1399096759');
INSERT INTO `statistik` VALUES ('::1', '2014-05-04', '1', '1399192407');
INSERT INTO `statistik` VALUES ('::1', '2014-05-09', '1', '1399599022');
INSERT INTO `statistik` VALUES ('::1', '2014-05-10', '2', '1399734054');
INSERT INTO `statistik` VALUES ('::1', '2014-05-11', '2', '1399803971');
INSERT INTO `statistik` VALUES ('::1', '2014-05-30', '1', '1401453788');
INSERT INTO `statistik` VALUES ('::1', '2014-06-02', '1', '1401710267');
INSERT INTO `statistik` VALUES ('::1', '2014-06-03', '1', '1401729064');
INSERT INTO `statistik` VALUES ('::1', '2014-06-04', '68', '1401888891');
INSERT INTO `statistik` VALUES ('::1', '2014-08-13', '43', '1407948219');
INSERT INTO `statistik` VALUES ('::1', '2014-08-14', '189', '1408023252');
INSERT INTO `statistik` VALUES ('::1', '2014-08-15', '80', '1408121798');
INSERT INTO `statistik` VALUES ('::1', '2014-08-16', '88', '1408207815');
INSERT INTO `statistik` VALUES ('127.0.0.1', '2014-08-16', '1', '1408162903');
INSERT INTO `statistik` VALUES ('::1', '2014-08-17', '179', '1408288371');
INSERT INTO `statistik` VALUES ('::1', '2014-08-18', '105', '1408372361');
INSERT INTO `statistik` VALUES ('::1', '2014-08-19', '200', '1408467556');
INSERT INTO `statistik` VALUES ('::1', '2014-08-20', '505', '1408552823');
INSERT INTO `statistik` VALUES ('127.0.0.1', '2014-08-20', '1', '1408529987');
INSERT INTO `statistik` VALUES ('::1', '2014-08-21', '276', '1408639892');
INSERT INTO `statistik` VALUES ('::1', '2014-08-22', '348', '1408719399');
INSERT INTO `statistik` VALUES ('::1', '2014-08-23', '109', '1408810207');
INSERT INTO `statistik` VALUES ('::1', '2014-08-24', '168', '1408892878');
INSERT INTO `statistik` VALUES ('::1', '2014-08-25', '43', '1408978150');
INSERT INTO `statistik` VALUES ('::1', '2014-08-26', '1', '1409013349');
INSERT INTO `statistik` VALUES ('::1', '2014-08-30', '13', '1409415664');
INSERT INTO `statistik` VALUES ('::1', '2014-08-31', '13', '1409493320');
INSERT INTO `statistik` VALUES ('::1', '2014-09-01', '2', '1409548023');
INSERT INTO `statistik` VALUES ('::1', '2014-09-02', '7', '1409673914');
INSERT INTO `statistik` VALUES ('::1', '2014-09-03', '13', '1409753803');
INSERT INTO `statistik` VALUES ('::1', '2014-09-04', '26', '1409840827');
INSERT INTO `statistik` VALUES ('::1', '2014-09-06', '74', '1409987297');
INSERT INTO `statistik` VALUES ('127.0.0.1', '2014-09-06', '1', '1409999053');
INSERT INTO `statistik` VALUES ('::1', '2014-09-07', '4', '1410045878');
INSERT INTO `statistik` VALUES ('::1', '2014-09-09', '5', '1410236093');
INSERT INTO `statistik` VALUES ('::1', '2014-09-15', '2', '1410781674');
INSERT INTO `statistik` VALUES ('::1', '2014-09-16', '7', '1410862378');
INSERT INTO `statistik` VALUES ('::1', '2014-09-17', '11', '1410956602');
INSERT INTO `statistik` VALUES ('::1', '2014-09-18', '13', '1411052252');
INSERT INTO `statistik` VALUES ('::1', '2014-09-21', '45', '1411315398');
INSERT INTO `statistik` VALUES ('::1', '2014-09-22', '82', '1411373769');
INSERT INTO `statistik` VALUES ('::1', '2014-09-23', '8', '1411486398');
INSERT INTO `statistik` VALUES ('::1', '2014-09-24', '6', '1411574327');
INSERT INTO `statistik` VALUES ('::1', '2014-09-25', '6', '1411661403');
INSERT INTO `statistik` VALUES ('::1', '2014-09-26', '3', '1411747775');
INSERT INTO `statistik` VALUES ('::1', '2014-09-27', '4', '1411828333');
INSERT INTO `statistik` VALUES ('::1', '2014-09-28', '5', '1411899631');
INSERT INTO `statistik` VALUES ('::1', '2014-09-29', '1', '1411956287');
INSERT INTO `statistik` VALUES ('::1', '2014-10-01', '5', '1412138993');
INSERT INTO `statistik` VALUES ('::1', '2014-10-04', '3', '1412429282');
INSERT INTO `statistik` VALUES ('::1', '2014-10-10', '5', '1412948724');
INSERT INTO `statistik` VALUES ('::1', '2015-01-01', '40', '1420129097');
INSERT INTO `statistik` VALUES ('::1', '2015-01-02', '12', '1420217678');
INSERT INTO `statistik` VALUES ('::1', '2015-01-03', '17', '1420304030');
INSERT INTO `statistik` VALUES ('::1', '2015-01-04', '106', '1420389660');
INSERT INTO `statistik` VALUES ('::1', '2015-01-05', '11', '1420476032');
INSERT INTO `statistik` VALUES ('::1', '2015-01-06', '7', '1420537235');
INSERT INTO `statistik` VALUES ('::1', '2015-01-07', '48', '1420644817');
INSERT INTO `statistik` VALUES ('::1', '2015-01-08', '11', '1420717801');
INSERT INTO `statistik` VALUES ('::1', '2015-01-09', '4', '1420788486');
INSERT INTO `statistik` VALUES ('::1', '2015-01-31', '154', '1422708825');
INSERT INTO `statistik` VALUES ('::1', '2015-02-01', '15', '1422800579');
INSERT INTO `statistik` VALUES ('::1', '2015-02-02', '56', '1422849077');
INSERT INTO `statistik` VALUES ('::1', '2016-01-04', '41', '1451878473');
INSERT INTO `statistik` VALUES ('::1', '2016-01-06', '37', '1452094645');
INSERT INTO `statistik` VALUES ('::1', '2016-01-10', '24', '1452393540');
INSERT INTO `statistik` VALUES ('::1', '2016-01-11', '4', '1452526545');
INSERT INTO `statistik` VALUES ('::1', '2016-01-12', '5', '1452639056');
INSERT INTO `statistik` VALUES ('::1', '2016-02-09', '2', '1455028272');
INSERT INTO `statistik` VALUES ('::1', '2016-03-12', '46', '1457754561');
INSERT INTO `statistik` VALUES ('::1', '2016-04-06', '1', '1459942905');
INSERT INTO `statistik` VALUES ('::1', '2016-07-17', '17', '1468741834');
INSERT INTO `statistik` VALUES ('::1', '2016-07-25', '10', '1469455384');
INSERT INTO `statistik` VALUES ('::1', '2016-07-27', '6', '1469639100');
INSERT INTO `statistik` VALUES ('::1', '2016-07-28', '1', '1469718549');
INSERT INTO `statistik` VALUES ('::1', '2016-08-01', '3', '1470047808');
INSERT INTO `statistik` VALUES ('::1', '2016-08-10', '72', '1470837264');
INSERT INTO `statistik` VALUES ('::1', '2016-08-11', '130', '1470920617');
INSERT INTO `statistik` VALUES ('::1', '2016-08-12', '23', '1471001482');

-- ----------------------------
-- Table structure for submenu
-- ----------------------------
DROP TABLE IF EXISTS `submenu`;
CREATE TABLE `submenu` (
  `id_sub` int(5) NOT NULL AUTO_INCREMENT,
  `nama_sub` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `link_sub` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `id_menu` int(5) NOT NULL,
  `id_submenu` int(5) NOT NULL,
  `aktif` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id_sub`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of submenu
-- ----------------------------
INSERT INTO `submenu` VALUES ('11', 'Visi & Misi', 'hal-visi-misi.html', '7', '0', 'Y');
INSERT INTO `submenu` VALUES ('12', 'Profil Singkat', 'hal-profil-singkat.html', '7', '0', 'Y');
INSERT INTO `submenu` VALUES ('13', 'Berita', 'semua-berita.html', '8', '0', 'Y');
INSERT INTO `submenu` VALUES ('14', 'Agenda', 'semua-agenda.html', '8', '0', 'Y');
INSERT INTO `submenu` VALUES ('15', 'Pengumuman', 'semua-pengumuman.html', '8', '0', 'Y');
INSERT INTO `submenu` VALUES ('16', 'Sejarah', 'hal-sejarah.html', '7', '0', 'Y');
INSERT INTO `submenu` VALUES ('17', 'Struktur Organisasi', 'hal-struktur.html', '7', '0', 'N');
INSERT INTO `submenu` VALUES ('25', 'Alur Pendaftaran', 'hal-alur-pendaftaran.html', '10', '0', 'N');
INSERT INTO `submenu` VALUES ('26', 'Formulir Pendaftaran', 'form-pendaftaran.html', '10', '0', 'Y');
INSERT INTO `submenu` VALUES ('20', 'Gedung', 'fasilitas-gedung.html', '9', '0', 'Y');
INSERT INTO `submenu` VALUES ('21', 'Pendidikan', 'fasilitas-pendidikan.html', '9', '0', 'Y');
INSERT INTO `submenu` VALUES ('22', 'Buku Tamu', 'bukutamu.html', '11', '0', 'Y');
INSERT INTO `submenu` VALUES ('23', 'Kontak', 'hubungi-kami.html', '11', '0', 'Y');
INSERT INTO `submenu` VALUES ('24', 'Tata Cara Pendaftaran', 'info-ppdb.html', '10', '0', 'Y');
INSERT INTO `submenu` VALUES ('27', 'Daftar Mahasiswa Terdaftar', 'hasil-seleksi.html', '10', '0', 'Y');
INSERT INTO `submenu` VALUES ('28', 'Artikel', 'semua-artikel.html', '8', '0', 'N');
INSERT INTO `submenu` VALUES ('29', 'Video', 'semua-video.html', '8', '0', 'N');
INSERT INTO `submenu` VALUES ('19', 'Olahraga', 'fasilitas-olahraga.html', '9', '0', 'Y');

-- ----------------------------
-- Table structure for tag
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `id_tag` int(5) NOT NULL AUTO_INCREMENT,
  `nama_tag` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `tag_seo` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `count` int(5) NOT NULL,
  PRIMARY KEY (`id_tag`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tag
-- ----------------------------
INSERT INTO `tag` VALUES ('1', 'Pendidikan', 'admin', 'pendidikan', '3');
INSERT INTO `tag` VALUES ('2', 'ppdb', 'admin', 'ppdb', '3');

-- ----------------------------
-- Table structure for tagline
-- ----------------------------
DROP TABLE IF EXISTS `tagline`;
CREATE TABLE `tagline` (
  `id_tagline` int(5) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `gambar` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_tagline`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of tagline
-- ----------------------------
INSERT INTO `tagline` VALUES ('1', 'Tagline', 'admin', 'tagline.jpg');
INSERT INTO `tagline` VALUES ('2', 'Tagline2', 'admin', 'tagline2.jpg');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `google_id` decimal(21,0) NOT NULL,
  `password` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `no_telp` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `foto` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `level` varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT 'user',
  `blokir` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `id_session` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `google_link` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `google_picture_link` varchar(150) COLLATE latin1_general_ci NOT NULL,
  `id_user` int(5) NOT NULL AUTO_INCREMENT,
  `vimeo` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `twitter` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `facebook` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `pinterest` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `linkedin` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `kontributor` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('admin', '109259042428517677618', '21232f297a57a5a743894a0e4a801fc3', 'jojo', 'sundala@yahoo.co.id', '081234123504', '49favicon.png', 'admin', 'N', 'uo96piql5qlgkuk22kuodl7ot0', '', '', '1', '', '', '', '', '', 'Y');

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video` (
  `id_video` int(5) NOT NULL AUTO_INCREMENT,
  `id_playlist` int(5) NOT NULL,
  `username` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `jdl_video` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `video_seo` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `keterangan` text COLLATE latin1_general_ci NOT NULL,
  `gbr_video` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `video` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `youtube` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `dilihat` int(7) NOT NULL DEFAULT '1',
  `hari` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `tagvid` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_video`)
) ENGINE=MyISAM AUTO_INCREMENT=157 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of video
-- ----------------------------
INSERT INTO `video` VALUES ('147', '49', 'admin', 'Taylor Dayne-Tell It To My Heart', 'taylor-daynetell-it-to-my-heart', 'I feel the night explode<br />\r\nWhen we&#39;re together<br />\r\nEmotion overload<br />\r\nIn the heat of pleasure<br />\r\n<br />\r\nTake me I&#39;m yours<br />\r\nInto your arms<br />\r\nNever let me go<br />\r\nTonight I really need to know<br />\r\n<br />\r\nTell it to my heart<br />\r\nTell me I&#39;m the only one<br />\r\nIs this really love or just a game?<br />\r\nTell it to my heart<br />\r\nI can feel my body rock<br />\r\nEvery time you call my name<br />\r\n<br />\r\nThe passion&#39;s so complete<br />\r\nIt&#39;s never ending<br />\r\nAs long as I receive<br />\r\nThe message you&#39;re sending<br />\r\n<br />\r\nBody to body<br />\r\nSoul to soul<br />\r\nAlways feel you near<br />\r\nSo say the words I long to hear<br />\r\n<br />\r\nTell it to my heart<br />\r\nTell me I&#39;m the only one<br />\r\nIs this really love or just a game?<br />\r\nTell it to my heart<br />\r\nI can feel my body rock<br />\r\nEvery time you call my name<br />\r\n<br />\r\nLove, love on the run<br />\r\nBreakin&#39; us down<br />\r\nThough we keep holdin&#39; on<br />\r\nI don&#39;t want to lose<br />\r\nNo, I can&#39;t let you go<br />\r\n<br />\r\nTell it to my heart<br />\r\nTell me I&#39;m the only one<br />\r\nIs this really love or just a game?<br />\r\nTell it to my heart<br />\r\nI can feel my body rock<br />\r\nEvery time you call my name<br />\r\n<br />\r\nTell it to my heart<br />\r\nTell me from the start<br />\r\nTell it to my heart<br />\r\nTell it to my heart<br />\r\nTell me from the start<br />\r\nTell it to my heart<br />\r\nNever make it stop<br />\r\nOh, take it to the heart<br />\r\n<br />\r\nTell it to my heart<br />\r\nTell me I&#39;m the only one<br />\r\nIs this really love or just a game?<br />\r\nTell it to my heart<br />\r\nI can feel my body rock<br />\r\nEvery time you call my name<br />\r\nTell it to my heart<br />\r\nTell me I&#39;m the only one<br />\r\nIs this really love or just a game?<br />\r\nTell it to my heart\r\n', '831329taylor_dayne.jpg', '', 'http://www.youtube.com/embed/Ud6sU3AclT4', '15', 'Sabtu', '2012-02-04', '12:11:28', '');
INSERT INTO `video` VALUES ('148', '50', 'admin', 'Shania Twain-Youre Still The One ', 'shania-twainyoure-still-the-one-', 'When I first saw you, I saw love<br />\r\nAnd the first time you touched me, I felt love<br />\r\nAnd after all this time, you&#39;re still the one I love<br />\r\n<br />\r\nLooks like we made it<br />\r\nLook how far we&#39;ve come my baby<br />\r\nWe mighta took the long way<br />\r\nWe knew we&#39;d get there someday<br />\r\n<br />\r\nThey said, &quot;I bet they&#39;ll never make it&quot;<br />\r\nBut just look at us holding on<br />\r\nWe&#39;re still together still going strong<br />\r\n<br />\r\n(You&#39;re still the one)<br />\r\nYou&#39;re still the one I run to<br />\r\nThe one that I belong to<br />\r\nYou&#39;re still the one I want for life<br />\r\n<br />\r\n(You&#39;re still the one)<br />\r\nYou&#39;re still the one that I love<br />\r\nThe only one I dream of<br />\r\nYou&#39;re still the one I kiss good night<br />\r\n<br />\r\nAin&#39;t nothin&#39; better<br />\r\nWe beat the odds together<br />\r\nI&#39;m glad we didn&#39;t listen<br />\r\nLook at what we would be missin&#39;<br />\r\n<br />\r\nThey said, &quot;I bet they&#39;ll never make it&quot;<br />\r\nBut just look at us holding on<br />\r\nWe&#39;re still together still going strong<br />\r\n<br />\r\n(You&#39;re still the one)<br />\r\nYou&#39;re still the one I run to<br />\r\nThe one that I belong to<br />\r\nYou&#39;re still the one I want for life<br />\r\n<br />\r\n(You&#39;re still the one)<br />\r\nYou&#39;re still the one that I love<br />\r\nThe only one I dream of<br />\r\nYou&#39;re still the one I kiss good night<br />\r\nYou&#39;re still the one<br />\r\n<br />\r\n(You&#39;re still the one)<br />\r\nYou&#39;re still the one I run to<br />\r\nThe one that I belong to<br />\r\nYou&#39;re still the one I want for life<br />\r\n<br />\r\n(You&#39;re still the one)<br />\r\nYou&#39;re still the one that I love<br />\r\nThe only one I dream of<br />\r\nYou&#39;re still the one I kiss good night<br />\r\n<br />\r\nI&#39;m so glad we made it<br />\r\nLook how far we&#39;ve come my baby\r\n', '854shania_twain.jpg', '', 'http://www.youtube.com/embed/KNZH-emehxA', '26', 'Sabtu', '2012-02-04', '12:31:16', '');
INSERT INTO `video` VALUES ('146', '49', 'admin', 'New Kids On The Block-Step by Step', 'new-kids-on-the-blockstep-by-step', 'Step by step, ooh baby<br />\r\nGonna get to you girl<br />\r\nStep by step, girl<br />\r\n<br />\r\nStep by step, ooh baby<br />\r\nGonna get to you girl<br />\r\nStep by step, ooh baby<br />\r\nReally want you in my world<br />\r\n<br />\r\nStep, hey girl<br />\r\nIn your eyes, I see a picture of me all the time<br />\r\nStep and girl<br />\r\nWhen you smile, you&#39;ve got to know that you drive me wild<br />\r\n<br />\r\nStep by step, ooh baby<br />\r\nYou&#39;re always on my mind<br />\r\nStep by step, ooh girl<br />\r\nI really think it&#39;s just a matter of time<br />\r\n<br />\r\nStep by step, ooh baby<br />\r\nGonna get to you girl<br />\r\nStep by step, ooh baby<br />\r\nReally want you in my world<br />\r\n<br />\r\nStep, hey girl<br />\r\nCan&#39;t you see, I&#39;ve got to have you all just for me<br />\r\nStep and girl<br />\r\nYes, it&#39;s true, no one else will ever do<br />\r\n<br />\r\nStep by step, ooh baby<br />\r\nYou&#39;re always on my mind<br />\r\nStep by step, ooh girl<br />\r\nI really think it&#39;s just a matter of time<br />\r\n<br />\r\nStep by step, ooh baby<br />\r\nReally want you in my world<br />\r\nStep by step, ooh baby<br />\r\nGonna get to you girl<br />\r\n<br />\r\nStep, step, step, step by step<br />\r\nStep one<br />\r\nWe can have lots of fun<br />\r\nStep two<br />\r\nThere&#39;s so much we can do<br />\r\nStep three<br />\r\nIt&#39;s just you and me<br />\r\nStep four<br />\r\nI can give you more<br />\r\nStep five<br />\r\nDon&#39;t you know that the time is right<br />\r\n<br />\r\nStep by step<br />\r\nDon&#39;t you know I need you<br />\r\nStep by step<br />\r\nYes, I do, girl<br />\r\n<br />\r\nStep by step, ooh baby<br />\r\nYou&#39;re always on my mind<br />\r\nStep by step, ooh girl<br />\r\nI really think it&#39;s just a matter of time<br />\r\n<br />\r\nStep by step, ooh baby<br />\r\nGonna get to you girl<br />\r\nStep by step, ooh baby<br />\r\nReally want you in my world<br />\r\n<br />\r\nStep by step, ooh baby<br />\r\nGonna get to you girl<br />\r\nStep by step, ooh girl<br />\r\nReally want you in my world<br />\r\n<br />\r\nStep by step, ooh baby<br />\r\nGonna get to you girl<br />\r\nStep by step, ooh baby<br />\r\n', '532379nkotb.jpg', '', 'http://www.youtube.com/embed/ES_kxPkgm9U', '12', 'Sabtu', '2012-02-04', '11:39:50', '');
INSERT INTO `video` VALUES ('145', '49', 'admin', 'Debbie Gibson - Electric Youth ', 'debbie-gibson--electric-youth-', 'Zappin&#39; it to you, the pressure&#39;s everywhere<br />\r\nGoin&#39; right through you<br />\r\nThe fever&#39;s in the air, oh, yeah, it&#39;s there<br />\r\nDon&#39;t underestimate the power of a lifetime ahead<br />\r\n<br />\r\nElectric youth, feel the power<br />\r\nYou see the energy comin&#39; up, coming on strong<br />\r\nThe future only belongs to the future itself<br />\r\nAnd the future is electric youth<br />\r\nIt&#39;s true you can&#39;t fight it, live by it<br />\r\nThe next generation, it&#39;s electric<br />\r\n<br />\r\nWe&#39;ve got the most time to make the world go round<br />\r\nOh, can you spare a dime?<br />\r\nPlace your bet on our sound, come back to town<br />\r\nDon&#39;t lose sight of potential mastermind<br />\r\nRemember when you were young<br />\r\n<br />\r\nElectric youth, feel the power<br />\r\nYou see the energy comin&#39; up, coming on strong<br />\r\nThe future only belongs in the hands of itself<br />\r\nAnd the future is electric youth<br />\r\nIt&#39;s true you can&#39;t fight it, live by it<br />\r\nThe next generation, it&#39;s electric<br />\r\n<br />\r\nWe do what comes naturally<br />\r\nYou see now, wait for the possibility<br />\r\nDon&#39;t you see a strong resemblance to yourself?<br />\r\nDon&#39;t you think what we say is important?<br />\r\nWhatever it may be, the fun is gonna start with me<br />\r\nI&#39;m bringing it back<br />\r\n<br />\r\nElectric youth, feel the power<br />\r\nYou see the energy comin&#39; up, coming on strong<br />\r\nThe future only belongs to the future itself<br />\r\nAnd the future is, electric youth<br />\r\nIt&#39;s true you can&#39;t fight it, live by it<br />\r\nThe next generation, it&#39;s electrifying<br />\r\n<br />\r\nElectric youth, feel the power<br />\r\nYou see the energy comin&#39; up, coming on strong<br />\r\nThe future only belongs in the hands of itself<br />\r\nAnd the future is electric youth<br />\r\nIt&#39;s true you can&#39;t fight it, live by it<br />\r\nThe next generation, it&#39;s electric<br />\r\nIt&#39;s electric, it&#39;s electric<br />\r\n<br />\r\nElectric youth, feel the power<br />\r\nYou see the energy comin&#39; up, coming on strong<br />\r\nThe future only belongs to the future itself<br />\r\nAnd the future is electric youth<br />\r\nIt&#39;s true you can&#39;t fight it, live by it<br />\r\nThe next generation<br />\r\n<br />\r\nInflation, flirtation<br />\r\nRelaxation, elation<br />\r\nGeneration of<br />\r\nAn electric youth\r\n', '952941debbie-gibson.jpg', '', 'http://www.youtube.com/embed/QW9-VGcNZ5M', '23', 'Sabtu', '2012-02-04', '11:03:09', '');
INSERT INTO `video` VALUES ('143', '49', 'admin', 'A Shoulder To Cry On-Tommy Page', 'a-shoulder-to-cry-ontommy-page', 'Life is full of lots of up and downs,<br />\r\nAnd the distance feels further when you&#39;re headed for the ground,<br />\r\nAnd there is nothing more painful than to let you&#39;re feelings take<br />\r\nyou down,<br />\r\nIt&#39;s so hard to know the way you feel inside,<br />\r\nWhen there&#39;s many thoughts and feelings that you hide,<br />\r\nBut you might feel better if you let me walk with you<br />\r\nby your side,<br />\r\n<br />\r\nAnd when you need a shoulder to cry on,<br />\r\nWhen you need a friend to rely on,<br />\r\nWhen the whole world is gone,<br />\r\nYou won&#39;t be alone, cause I&#39;ll be there,<br />\r\nI&#39;ll be your shoulder to cry on,<br />\r\nI&#39;ll be there,<br />\r\nI&#39;ll be a friend to rely on,<br />\r\nWhen the whole world is gone,<br />\r\nyou won&#39;t be alone, cause I&#39;ll be there.<br />\r\n<br />\r\nAll of the times when everything is wrong<br />\r\nAnd you&#39;re feeling like<br />\r\nThere&#39;s no use going on<br />\r\nYou can&#39;t give it up<br />\r\nI hope you work it out and carry on<br />\r\nSide by side,<br />\r\nWith you till the end<br />\r\nI&#39;ll always be the one to firmly hold your hand<br />\r\nno matter what is said or done<br />\r\nour love will always continue on<br />\r\n<br />\r\nEveryone needs a shoulder to cry on<br />\r\neveryone needs a friend to rely on<br />\r\nWhen the whole world is gone<br />\r\nyou won&#39;t be alone cause I&#39;ll be there<br />\r\nI&#39;ll be your shoulder to cry on<br />\r\nI&#39;ll be there<br />\r\nI&#39;ll be the one you rely on<br />\r\nwhen the whole world&#39;s gone<br />\r\nyou won&#39;t be alone<br />\r\ncause I&#39;ll be there!<br />\r\n<br />\r\nAnd when the whole world is gone<br />\r\nYou&#39;ll always have my shoulder to cry on.... \r\n', '437103tommy_page.jpg', '', 'http://www.youtube.com/embed/UgpQ0H7xwkI', '44', 'Sabtu', '2012-02-04', '10:34:29', '');
INSERT INTO `video` VALUES ('142', '49', 'admin', 'Like A Prayer - Madonna', 'like-a-prayer--madonna', 'Life is a mystery<br />\r\nEveryone must stand alone<br />\r\nI hear you call my name<br />\r\nAnd it feels like home<br />\r\n<br />\r\nWhen you call my name it&#39;s like a little prayer<br />\r\nI&#39;m down on my knees, I wanna take you there<br />\r\nIn the midnight hour I can feel your power<br />\r\nJust like a prayer you know I&#39;ll take you there<br />\r\n<br />\r\nI hear your voice<br />\r\nIt&#39;s like an angel sighing<br />\r\nI have no choice, I hear your voice<br />\r\nFeels like flying<br />\r\n<br />\r\nI close my eyes<br />\r\nOh God I think I&#39;m falling<br />\r\nOut of the sky, I close my eyes<br />\r\nHeaven help me<br />\r\n<br />\r\nWhen you call my name it&#39;s like a little prayer<br />\r\nI&#39;m down on my knees, I wanna take you there<br />\r\nIn the midnight hour I can feel your power<br />\r\nJust like a prayer you know I&#39;ll take you there<br />\r\n<br />\r\nLike a child<br />\r\nYou whisper softly to me<br />\r\nYou&#39;re in control just like a child<br />\r\nNow I&#39;m dancing<br />\r\n<br />\r\nIt&#39;s like a dream<br />\r\nNo end and no beginning<br />\r\nYou&#39;re here with me it&#39;s like a dream<br />\r\nLet the choir sing<br />\r\n<br />\r\nWhen you call my name it&#39;s like a little prayer<br />\r\nI&#39;m down on my knees, I wanna take you there<br />\r\nIn the midnight hour I can feel your power<br />\r\nJust like a prayer you know I&#39;ll take you there<br />\r\n<br />\r\nWhen you call my name it&#39;s like a little prayer<br />\r\nI&#39;m down on my knees, I wanna take you there<br />\r\nIn the midnight hour I can feel your power<br />\r\nJust like a prayer you know I&#39;ll take you there<br />\r\n<br />\r\nLife is a mystery<br />\r\nEveryone must stand alone<br />\r\nI hear you call my name<br />\r\nAnd it feels like home<br />\r\n<br />\r\nJust like a prayer, your voice can take me there<br />\r\nJust like a muse to me, you are a mystery<br />\r\nJust like a dream, you are not what you seem<br />\r\nJust like a prayer, no choice your voice can take me there<br />\r\n<br />\r\nJust like a prayer, I&#39;ll take you there<br />\r\nIt&#39;s like a dream to me<br />\r\nJust like a prayer, I&#39;ll take you there<br />\r\nIt&#39;s like a dream to me<br />\r\n<br />\r\nJust like a prayer, I&#39;ll take you there<br />\r\nIt&#39;s like a dream to me<br />\r\nJust like a prayer, I&#39;ll take you there<br />\r\nIt&#39;s like a dream to me<br />\r\n<br />\r\nJust like a prayer, your voice can take me there<br />\r\nJust like a muse to me, you are a mystery<br />\r\nJust like a dream, you are not what you seem<br />\r\nJust like a prayer, no choice your voice can take me there<br />\r\n<br />\r\nJust like a prayer, your voice can take me there<br />\r\nJust like a muse to me, you are a mystery<br />\r\nJust like a dream, you are not what you seem<br />\r\nJust like a prayer, no choice your voice can take me there<br />\r\nYour voice can take me there<br />\r\nLike a prayer<br />\r\n<br />\r\nJust like a prayer<br />\r\nJust like a prayer, your voice can take me there<br />\r\nJust like a prayer<br />\r\nJust like a prayer, your voice can take me there\r\n', '581115madonna-like-a-prayer.jpg', '', 'http://www.youtube.com/embed/cSVbwwsLPqw', '11', 'Sabtu', '2012-02-04', '10:23:49', '');
INSERT INTO `video` VALUES ('144', '49', 'admin', 'Milli Vanilli-Baby Dont Forget My Number', 'milli-vanillibaby-dont-forget-my-number', 'Babe, don&#39;t be shy<br />\r\nWhen you&#39;re holding my hand<br />\r\n&#39;Cause this time goes back<br />\r\nYou got to understand it&#39;s you<br />\r\n<br />\r\nBa, ba, ba, baby in your eyes<br />\r\nI see it so clearly that our love, it&#39;s so strong<br />\r\nAnd you never go wrong<br />\r\nI got the best for you so I&#39;m waiting down<br />\r\n<br />\r\nIf you need someone<br />\r\nBaby, call my line<br />\r\nCall me anytime<br />\r\nI&#39;ll be there for you, you, you<br />\r\n<br />\r\nI&#39;ve been searching high, high, high<br />\r\nI&#39;ve been searching low<br />\r\n<br />\r\nBa, ba, ba, ba, baby<br />\r\nDon&#39;t forget my number<br />\r\nBaby, don&#39;t be stronger than a thunder<br />\r\nBa, ba, ba, ba, baby<br />\r\nDon&#39;t forget my number<br />\r\nLove will see you through<br />\r\n<br />\r\nI&#39;ve been searching high<br />\r\nI&#39;ve been searching low<br />\r\n<br />\r\nI want to spend spend my life with you<br />\r\nBa, ba, ba, ba, ba, ba, ba, ba<br />\r\nMy desper youth<br />\r\nBa, ba, ba, ba, ba, ba, ba, ba<br />\r\nLove will see you through<br />\r\n<br />\r\nBa, ba, baby in your eyes<br />\r\nI see it so clearly\r\n', '447998milli-vanilli.jpg', '', 'http://www.youtube.com/embed/saPp0jr7Go0', '28', 'Sabtu', '2012-02-04', '10:44:56', '');
INSERT INTO `video` VALUES ('140', '49', 'admin', 'Forever Young - Alphaville', 'forever-young--alphaville', 'Lets dance in style, lets dance for a while.<br />\r\nHeaven can wait were only watching the skies.<br />\r\nHoping for the best but expecting the worst.<br />\r\nAre you gonna drop the bomb or not?<br />\r\n<br />\r\nLet us die young or let us live forever.<br />\r\nWe dont have the power but we never say never.<br />\r\nSitting in a sandpit, life is a short trip.<br />\r\nThe music&#39;s for the sad men.<br />\r\n<br />\r\nCan you imagine when this race is won.<br />\r\nTurn our golden faces into the sun.<br />\r\nPraising our leaders were getting in tune.<br />\r\nThe musics played by the madmen.<br />\r\n<br />\r\nForever young, I want to be forever young.<br />\r\nDo you really want to live forever? forever and ever.<br />\r\n<br />\r\nForever young, I want to be forever young.<br />\r\nDo you really want to live forever? for ever young.<br />\r\n<br />\r\nSome are like water, some are like the heat.<br />\r\nSome are a melody and some are the beat.<br />\r\nSooner or later they all will be gone.<br />\r\nWhy dont they stay young?<br />\r\n<br />\r\nIts so hard to get old without a cause.<br />\r\nI dont want to perish like a fading horse.<br />\r\nYouth is like diamonds in the sun.<br />\r\nAnd diamonds are forever.<br />\r\n<br />\r\nSo many adventures couldnt happen today.<br />\r\nSo many songs we forgot to play.<br />\r\nSo many dreams are swinging out of the blue.<br />\r\nWe let them come true.<br />\r\n<br />\r\nForever young, I want to be forever young.<br />\r\nDo you really want to live forever? forever and ever.<br />\r\n<br />\r\nForever young, I want to be forever young.<br />\r\nDo you really want to live forever? forever and ever.<br />\r\n<br />\r\nForever young, I want to be forever young.<br />\r\nDo you really want to live forever? forever.\r\n', '976013alphaville-forever-young.jpg', '', 'http://www.youtube.com/embed/t1TcDHrkQYg', '27', 'Sabtu', '2012-02-04', '09:03:23', '');
INSERT INTO `video` VALUES ('149', '50', 'admin', 'Meja - All About The Money', 'meja--all-about-the-money', 'Some times I find another world inside my mind\r\nWhen I realize all the crazy things we do\r\nIt makes me feel ashamed to be alive\r\nI wanna run away and hide\r\nIt&#39;s all about the money, it&#39;s all about the dum dum\r\nAnd I don&#39;t think it&#39;s funny to see us fade away\r\nIt&#39;s all about the money, it&#39;s all about the\r\nI think we got it all wrong anyway\r\nStrange ways of showing how much we really care\r\nWhen in face we don&#39;t seem to care at all\r\nThis pretty world is getting out of hand\r\nSo how come we fail to understand?\r\nIt&#39;s all about the money, it&#39;s all about the dum dum\r\nAnd I don&#39;t think it&#39;s funny to see us fade away\r\nIt&#39;s all about the money, it&#39;s all about the\r\nI think we got it all wrong anyway\r\n', '793426meja.jpg', '', 'http://www.youtube.com/embed/YcXMhwF4EtQ', '100', 'Sabtu', '2012-02-04', '13:04:19', '');
INSERT INTO `video` VALUES ('156', '56', 'admin', 'Borobudur', 'borobudur', 'Borobudur adalah nama sebuah candi Buddha yang terletak di Borobudur, Magelang, Jawa Tengah, Indonesia. Lokasi candi adalah kurang lebih 100 km di sebelah barat daya Semarang, 86 km di sebelah barat Surakarta, dan 40 km di sebelah barat laut Yogyakarta. Candi berbentuk stupa ini didirikan oleh para penganut agama Buddha Mahayana sekitar tahun 800-an Masehi pada masa pemerintahan wangsa Syailendra. Monumen ini terdiri atas enam teras berbentuk bujur sangkar yang diatasnya terdapat tiga pelataran melingkar, pada dindingnya dihiasi dengan 2.672 panel relief dan aslinya terdapat 504 arca Buddha.[1] Stupa utama terbesar teletak di tengah sekaligus memahkotai bangunan ini, dikelilingi oleh tiga barisan melingkar 72 stupa berlubang yang didalamnya terdapat arca buddha tengah duduk bersila dalam posisi teratai sempurna dengan mudra (sikap tangan) Dharmachakra mudra (memutar roda dharma).<br />\r\n', '523559borobudur-temple.jpg', '', 'https://www.youtube.com/embed/4cxWt3cXyZo', '7', 'Kamis', '2012-11-22', '23:33:29', '');
