<?php
namespace controler;

use lib\MVC\controler\base;
use \lib\Fcm;

class ajax extends base
{
    public function __construct()
    {
        parent::__construct();
        $this->model("tabel");
    }

    public function asrama()
    {
        $data = $this->model->listAsrama();
        $this->sendToJson($data);
    }

    public function anggota()
    {
        $req = $this->getBody();
        $data = $this->model->listAnggota($req['asrama'], $req['kamar']);
        $this->sendToJson($data);
    }

    public function kamar()
    {
        $req = $this->getBody();
        $data = $this->model->listKamar($req['asrama']);
        $this->sendToJson($data);
    }

    public function sendNotif()
    {
        $data = $this->getBody();
        $fcm = new Fcm();
        $token = '';
        $group_name = '';
        switch ($data['filter']) {
            case 'dorm':
                $token = $fcm->getToken("asr" . $data['asrama']);
                $group_name = "asr" . $data['asrama'];
                break;
            case 'room':
                $token = $fcm->getToken("asr" . $data['asrama'] . "kmr" . $data['kamar']);
                $group_name = "asr" . $data['asrama'] . "kmr" . $data['kamar'];
                break;
            case 'person':
                $token = $this->model->getFcmToken($data['person']);
                $group_name = $data['person'];
                break;
            default:
                $token = $fcm->getToken("all");
                $group_name = "all";
                break;
        }
        if ($token) {
            $payload = array(
                "title" => $data['title'],
                "body" => $data['msg'],
            );
            $fcm->sendNotif($token,$payload);
        }
                    $this->model->insertNotif(array(
                        "title" => $data['title'],
                        "description" => $data['msg'],
                        "group_name" => $group_name
                    ));
    }

    public function feedData()
    {
        $req = $this->getBody();
        $datum = $req["kamar"];
        foreach ($datum as $data) {
            $this->model->feedData("account", $data);

        }
    }
    public function insertKamar()
    {
        $data = $this->getBody();
        $this->model->insertKamar($data);
        $this->sendResponse(array("status" => "success"));   
    }
}
