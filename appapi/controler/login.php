<?php
namespace controler; //penting!!
use lib\Jwt; //penting!!
use lib\MVC\controler\base;
//penting!!
class login extends base
{
    public function __construct()
    {
        parent::__construct(); //penting!!
        $this->model("user"); //panggil model
    }

    public function login()
    {
        $data = $this->getBody();
        $user = $this->model->checkUser($data['nim']);
        $response = array();
        if($user){
            if(password_verify($data['password'],$user['password'])){
                $payload = array(
                    "nim" => $data['nim']
                );
                $token = Jwt::encode($payload,"rusunawa");
                $response = array(
                    "status" => "success",
                    "data" => array(
                        "nim" => $user['nim'],
                        "token" => $token
                    )
                );
            }else{
                $response = array(
                    "status" => "error",
                    "msg" => "Password anda salah"
                );
            }
        }else{
            $response = array(
                "status" => "error",
                "msg" => "Nim anda salah atau tidak terdaftar"
            );
        }
        $this->sendToJson($response);
    }


    
}
