<?php
namespace controler; //penting!!
use lib\Fcm; //penting!!
use lib\Jwt;
use lib\MVC\controler\base;

//penting!!
class home extends base
{

    public $user;

    public function __construct()
    {
        parent::__construct(); //penting!!
        $this->model("tabel"); //panggil model
        $this->checkAuth();
    }

    public function checkAuth()
    {
        $headers = getallheaders();
        if (isset($headers['Authorization']) || isset($headers['authorization'])) {
            list($token) = isset($headers['Authorization']) ? sscanf($headers['Authorization'], "Bearer %s") : sscanf($headers['authorization'], "Bearer %s");
            if ($token == null) {
                http_response_code(401);
                die();
            } else {
                try {

                    $payload = Jwt::decode($token, "rusunawa");

                } catch (\UnexpectedValueException $ec) {
                    http_response_code(401);
                    die();
                }
                $this->user = array(
                    "nim" => $payload->nim,
                );

            }
        } else {
            http_response_code(401);
            die();
        }
    }
    public function user()
    {
        // $nim = $this->urlparams[3];
        $data['data'] = $this->model->userdata($this->user['nim'])[0];
        $this->sendToJson($data);
    }

    public function saveToken()
    {
        $req = $this->getBody();
        $token = $req['token'];
        $fcm = new Fcm();
        $userAsrama = $this->model->getUserAsrama($this->user['nim'])[0];
        $asrama = "asr" . $userAsrama['id_asrama'];
        $kamar = "asr" . $userAsrama['id_asrama'] . "kmr" . $userAsrama['no_kamar'];
        $asramaToken = $fcm->getToken($asrama);
        $kamarToken = $fcm->getToken($kamar);
        $all = $fcm->getToken("all");
        if ($all) {
            $fcm->addToGroup("all", $token, $all);
        } else {
            $fcm->createGroup("all", $token);
        }
        if ($asramaToken) {
            $fcm->addToGroup($asrama, $token, $asramaToken);
        } else {
            $fcm->createGroup($asrama, $token);
        }
        if ($kamarToken) {
            $fcm->addToGroup($kamar, $token, $kamarToken);
        } else {
            $fcm->createGroup($kamar, $token);
        }
        $this->model->insertFcm($this->user['nim'], $token);
    }

    public function removeToken()
    {
        $req = $this->getBody();
        $token = $req['token'];
        $fcm = new Fcm();
        $userAsrama = $this->model->getUserAsrama($this->user['nim'])[0];
        $asrama = "asr" . $userAsrama['id_asrama'];
        $kamar = "asr" . $userAsrama['id_asrama'] . "kmr" . $userAsrama['no_kamar'];
        $asramaToken = $fcm->getToken($asrama);
        $kamarToken = $fcm->getToken($kamar);
        $all = $fcm->getToken("all");
        if ($all) {
            $fcm->removeFromGroup($token, "all", $all);
        }
        if ($asramaToken) {
            $fcm->removeFromGroup($token, $asrama, $asramaToken);
        }
        if ($kamarToken) {
            $fcm->removeFromGroup($token, $kamar, $kamarToken);
        }
        $this->model->removeFcm($this->user['nim']);
    }

    public function getNotif()
    {
        $userAsrama = $this->model->getUserAsrama($this->user['nim'])[0];
        $asrama = "asr" . $userAsrama['id_asrama'];
        $kamar = "asr" . $userAsrama['id_asrama'] . "kmr" . $userAsrama['no_kamar'];
        $nim = $this->user['nim'];
        $notif = $this->model->getNotif($asrama,$kamar,$nim);
        $result = array();
        foreach ($notif as  $value) {
            $dbDate = $value['date'];
            $schedule_date = new \DateTime($dbDate, new \DateTimeZone('Asia/Jakarta'));
            $schedule_date->setTimeZone(new \DateTimeZone('Asia/Makassar'));
            $date = $schedule_date->format('Y-m-d H:i:s');
            $value['date'] = $date;
            $result[] = $value;

        }
        $this->sendToJson($result);
    }

    public function updatePassword()
    {
        $data = $this->getBody();
        $user = $this->model->checkUser($this->user['nim']);
        $response = array();
        if (password_verify($data['old_password'],$user['password'])) {
            $this->model->updatePass($this->user['nim'],password_hash($data['new_password'],PASSWORD_DEFAULT));
            $response = array(
                "status" => "success"
            );
        } else {
            $response = array(
                "status" => "error",
                "msg" => "Password lama anda salah"
            );
        }
        $this->sendToJson($response);
         
    }

    public function tambah()
    {
        $data = array("nama" => "rahmat", "kelas" => "XII TKJ 2");
        $this->model->tambah($data);
        $this->index();
    }
    public function ubah()
    {
        $data = array("kelas" => "XII TKJ 1");
        $this->model->ubah($data);
    }
    public function hapus()
    {
        $this->model->hapus("nama", "rahmat");
        redirect(index);
    }
}
