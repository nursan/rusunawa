<?php
namespace lib;

/**
 *
 */
class Fcm
{

  public function sendNotif($to,$data)
  {
    $curl = curl_init();
    $data['sound'] = "default";
    $payload = array(
      "to" => $to,
      "priority" => "high",
      "notification" => $data,
      "data" => $data
    );
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($payload),
      CURLOPT_HTTPHEADER => array(
        "authorization: key=AAAA865HFS4:APA91bFTa5AnIO0iXxME-WR7MN5AAzztxsJnzjTQ17vxvTDrxyAACicL-GV8sI_Zptp1QE5biB8wRKFG0bVbm13jRn4yCbmJ9VN5rwfG6qqYvTaRISStQLQ7_QGbrqRznmvIeVV-3jYEP7k1hZ_svXgRM1Lw0z24FA",
        "content-type: application/json",
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      throw new Exception("Error Processing Request : ".$err, 1);
    } else {
      return true;
    }
  }

  public function createGroup($id,$token)
  {
    $data = array(
      "operation" => "create",
      "notification_key_name" => $id,
      "registration_ids" => [$token],
    );
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://android.googleapis.com/gcm/notification",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($data),
      CURLOPT_HTTPHEADER => array(
        "authorization: key=AAAA865HFS4:APA91bFTa5AnIO0iXxME-WR7MN5AAzztxsJnzjTQ17vxvTDrxyAACicL-GV8sI_Zptp1QE5biB8wRKFG0bVbm13jRn4yCbmJ9VN5rwfG6qqYvTaRISStQLQ7_QGbrqRznmvIeVV-3jYEP7k1hZ_svXgRM1Lw0z24FA",
        "content-type: application/json",
        "project_id: 1046600946990"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      throw new Exception("Error Processing Request ".$err, 1);
    } else {
       return json_decode($response,true);
    }
  }

  public function addToGroup($id,$token,$groupToken)
  {
    $data = array(
      "operation" => "add",
      "notification_key_name" => $id,
      "notification_key" => $groupToken,
      "registration_ids" => [$token]
    );

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://android.googleapis.com/gcm/notification",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($data),
      CURLOPT_HTTPHEADER => array(
        "authorization: key=AAAA865HFS4:APA91bFTa5AnIO0iXxME-WR7MN5AAzztxsJnzjTQ17vxvTDrxyAACicL-GV8sI_Zptp1QE5biB8wRKFG0bVbm13jRn4yCbmJ9VN5rwfG6qqYvTaRISStQLQ7_QGbrqRznmvIeVV-3jYEP7k1hZ_svXgRM1Lw0z24FA",
        "content-type: application/json",
        "project_id: 1046600946990"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      throw new Exception("Error Processing Request ".$err, 1);
    } else {
      return json_decode($response,true);
    }
  }

  public function getToken($idJadwal)
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://android.googleapis.com/gcm/notification?notification_key_name=".$idJadwal,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "authorization: key=AAAA865HFS4:APA91bFTa5AnIO0iXxME-WR7MN5AAzztxsJnzjTQ17vxvTDrxyAACicL-GV8sI_Zptp1QE5biB8wRKFG0bVbm13jRn4yCbmJ9VN5rwfG6qqYvTaRISStQLQ7_QGbrqRznmvIeVV-3jYEP7k1hZ_svXgRM1Lw0z24FA",
        "content-type: application/json",
        "project_id: 1046600946990"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      throw new Exception("Error Processing Request".$err, 1);
    } else {
      $res = json_decode($response,true);
      return (isset($res['error'])) ? false : $res['notification_key'];
    }
  }

  public function removeFromGroup($token,$groupName,$groupToken)
  {
    $data = array(
      "operation" => "remove",
      "notification_key_name" => $groupName,
      "notification_key" => $groupToken,
      "registration_ids" => [$token]
    );
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://android.googleapis.com/gcm/notification",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($data),
      CURLOPT_HTTPHEADER => array(
        "authorization: key=AAAA865HFS4:APA91bFTa5AnIO0iXxME-WR7MN5AAzztxsJnzjTQ17vxvTDrxyAACicL-GV8sI_Zptp1QE5biB8wRKFG0bVbm13jRn4yCbmJ9VN5rwfG6qqYvTaRISStQLQ7_QGbrqRznmvIeVV-3jYEP7k1hZ_svXgRM1Lw0z24FA",
        "content-type: application/json",
        "project_id: 1046600946990"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      throw new Exception("Error Processing Request ".$err, 1);
    } else {
      return null;
    }
  }
}



?>
