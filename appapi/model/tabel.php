<?php
namespace model; //penting!!
use lib\MVC\model\model;
//penting!!

class tabel extends model
{
    public function __construct()
    {
        parent::__construct(); //penting!!
    }
    public function userdata($nim)
    {
        $this->query("SELECT pendaftaran.nim,pendaftaran.nama,asrama.nama_asrama,asrama.pembina,anggota_asrama.no_kamar,pembayaran.tanggal_transaksi,pembayaran.jumlah_bayar,pembayaran.jumlah_bulan FROM `anggota_asrama` INNER JOIN pendaftaran ON anggota_asrama.id_pendaftar=pendaftaran.nim INNER JOIN asrama ON asrama.id_asrama=anggota_asrama.id_asrama INNER JOIN pembayaran ON anggota_asrama.id_pendaftar=pembayaran.id_mahasiswa WHERE pendaftaran.nim=" . $nim);
        return $this->get();
    }
    public function pembayaran($nim)
    {
        $this->query("SELECT pembayaran.tanggal_transaksi,pembayaran.jumlah_bayar,pembayaran.jumlah_bulan FROM `pembayaran` INNER JOIN anggota_asrama ON pembayaran.id_mahasiswa=anggota_asrama.id_pendaftar WHERE pendaftaran.nim=" . $nim);
        return $this->get();
    }

    public function listAsrama()
    {
        $this->select('id_asrama,nama_asrama,jumlah_kamar');
        $this->from('asrama');
        return $this->get();
    }

    public function listAnggota($asrama, $kamar)
    {
        $this->query("SELECT anggota_asrama.id_pendaftar,pendaftaran.nama FROM `anggota_asrama` INNER JOIN pendaftaran ON anggota_asrama.id_pendaftar=pendaftaran.nim WHERE anggota_asrama.id_asrama=" . $asrama . " AND anggota_asrama.no_kamar=" . $kamar);
        return $this->get();
    }

    public function getUserAsrama($nim)
    {
        $this->query("SELECT id_asrama,no_kamar FROM `anggota_asrama` WHERE id_pendaftar=" . $nim);
        return $this->get();
    }

    public function insertFcm($nim, $fcm)
    {
        $this->set(array("device_token" => $fcm));
        $this->where("nim", $nim);
        $this->into('account');
        $this->update();
    }

    public function removeFcm($nim)
    {
        $this->set(array("device_token" => null));
        $this->where("nim", $nim);
        $this->into('account');
        $this->update();
    }

    public function getFcmToken($nim)
    {
        $this->query("SELECT device_token FROM `account` WHERE nim=" . $nim);
        $result = $this->get();
        if ($result == array() || $result[0]['device_token'] == null) {
            return false;
        } else {
            return $result[0]['device_token'];
        }
    }

    public function insertNotif($data)
    {
        $this->into('notif');
        $this->set($data);
        $this->insert();
    }

    public function getNotif($asrama, $kamar, $nim)
    {
        $this->query("SELECT title,description,date FROM `notif` WHERE group_name=" . $nim . " OR group_name='" . $asrama . "' OR group_name='" . $kamar . "' OR group_name='all'");
        return $this->get();
    }

    public function checkUser($nim)
    {
        $this->select("nim,password");
        $this->where('nim', $nim);
        $this->from('account');
        $result = $this->get();
        if ($result == array()) {
            return false;
        } else {
            return $result[0];
        }
	}
	
	public function updatePass($nim,$pass)
	{
		$this->set(array("password" => $pass));
		$this->into('account');
		$this->where('nim',$nim);
		$this->update();
    }
    
    public function feedData($tabel,$data)
    {
        $this->into($tabel);
        $this->set($data);
        $this->insert();
    }

    public function listKamar($idAsrama)
    {
        $this->query("SELECT DISTINCT no_kamar,id_asrama FROM `anggota_asrama` WHERE id_asrama=".$idAsrama);
        return $this->get();
    }

    public function tambah($p)
    {
        $this->into("data");
        $this->set($p);

        $this->insert();
    }
    public function hapus($p1, $p2)
    {
        $this->into("data");
        $this->where($p1, $p2);

        $this->delete();
    }
    public function ubah($p)
    {

        $this->into("data");
        $this->set($p);
        $this->where("nama", "rahmat");

        $this->update();

    }

    public function insertKamar($data)
    {
        $this->into("kamar");
        $this->set($data);
        $this->insert();
    }
}
