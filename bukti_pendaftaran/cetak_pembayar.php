<?php
define('FPDF_FONTPATH', 'fpdf/font/');
require('fpdf/fpdf.php');

include "../config/koneksi.php";
include "../config/library.php";
$pdf=new FPDF('L','mm','A4');
$pdf->AddPage();
$pdf->SetFont('Arial','',18);
//ambil Gambar Header
//$pdf->Image("../user/images/kop.png", 10, 3, '80', 'left');
//Judul Laporan PDF
$pdf->SetFont('Arial','B','18');
$pdf->Cell(240,20,'KWITANSI PEMBAYARAN ',0,0,'C');

$pdf->Ln(24);
$id=$_GET['id'];
$jml=$_GET['jml'];
$harga=$_GET['harga'];
$sql = mysqli_query($conn,"
SELECT
a.*,c.fakultas as fakultas_,d.jurusan as jurusan_
FROM
pendaftaran a
INNER JOIN
tbl_fakultas c
ON a.fakultas=c.id_fakultas
INNER JOIN tbl_jurusan d
ON d.id_jurusan=a.jurusan
where a.nim=$id");
$r=mysqli_fetch_array($sql);

$pdf->SetFont('Arial','B','12');
$pdf->SetFillColor(192,192,192); // Warna sel tabel header
$pdf->Cell(80,10,'NIM',1,0,'L', 1);
$pdf->Cell(80,10,$r['nim'],1,0,'L', 1);
$pdf->Ln();
$pdf->Cell(80,10,'Nama',1,0,'L', 1);
$pdf->Cell(80,10,$r['nama'],1,0,'L', 1);
$pdf->Ln();
$pdf->Cell(80,10,'Fakultas',1,0,'L', 1);
$pdf->Cell(80,10,$r['fakultas_'],1,0,'L', 1);
$pdf->Ln();
$pdf->Cell(80,10,'Jurusan',1,0,'L', 1);
$pdf->Cell(80,10,$r['jurusan_'],1,0,'L', 1);
$pdf->Ln();
$pdf->Cell(80,10,'No Telp',1,0,'L', 1);
$pdf->Cell(80,10,$r['telp'],1,0,'L', 1);
$pdf->Ln();
$pdf->Cell(80,10,'Jumlah Bulan',1,0,'L', 1);
$pdf->Cell(80,10,$jml,1,0,'L', 1);
$pdf->Ln();
$pdf->Cell(80,10,'Total Bayar',1,0,'L', 1);
$pdf->Cell(80,10,'RP. '.number_format($harga),1,0,'L', 1);
$pdf->Ln(20);

$pdf->Cell(140,7,'TTD Bag. Keuangan',0,0,'R');
$pdf->Ln(20);
$pdf->Cell(140,7,'Daeng Sattu',0,0,'R');
/*
$sql = mysqli_query($conn,"SELECT * FROM pendaftaran");
$no = 1;
while($r=mysqli_fetch_array($sql)){
  $lahir   = tgl_indo($r['tgl_lahir']);

  $cell[0]=$r['nama'];
  $cell[1]=$r['tempat'];
  $cell[2]=$r['tgl_lahir'];
  $cell[3]=$r['jurusan'];
  $cell[4]=$r['fakultas'];
  $cell[5]=$r['telp'];
 	$no++;
  $pdf->Ln();
  $pdf->Cell(25,7,'KOSUIN/',1,0,'L'); $pdf->Cell(-25,7,$r['id_pendaftaran'],0,0,'C'); $pdf->Cell(38,7,'/2016',0,0,'C'); $pdf->Cell(-13,7,'',0,0,'C');
  $pdf->Cell(50,7,$r['nama'],1,0,'L');
  $pdf->Cell(50,7,$r['tempat'],1,0,'L'); $pdf->Cell(-30,7,$lahir,0,0,'C');$pdf->Cell(30,7,'',0,0,'C');
  $pdf->Cell(50,7,$r['fakultas'],1,0,'L');
  $pdf->Cell(50,7,$r['jurusan'],1,0,'L');
  $pdf->Cell(45,7,$r['telp'],1,0,'L');
}*/

$pdf->Output();

// header('location:../user/media.php?module=pembayaran');
?>
