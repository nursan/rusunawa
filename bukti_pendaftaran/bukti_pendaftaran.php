<?php
define('FPDF_FONTPATH', 'fpdf/font/');
require('fpdf/fpdf.php');

include "../config/koneksi.php";
include "../config/library.php";
$sql = mysqli_query($conn,"SELECT * FROM pendaftaran WHERE  id_pendaftaran='$_GET[id]'");

$i=0;
$pdf=new FPDF('P','mm','A5');
while($r=mysqli_fetch_array($sql))
{
$lahir   = tgl_indo($r['tgl_lahir']);


$pdf->AddPage();
$pdf->SetFont('Arial','',12);
//ambil Gambar Header
//$pdf->Image("../images/sukses.jpg", 10, 3, '130', 'left');
//Judul Laporan PDF
$pdf->SetFont('Arial','B','11');
$pdf->Cell(130,80,'BUKTI PENDAFTARAN RUMAH UINAM',0,0,'C');

$pdf->Ln(50);
$pdf->SetFont('Arial','',7);

$pdf->Cell(50,10,'NIM (No. Induk Siswa)',1,0,'L');
$pdf->Cell(4,10,':',1,0,'L');
$pdf->Cell(70,10,$r['nim'],1,0,'L');

$pdf->Ln();
$pdf->Cell(50,10,'Nama',1,0,'L');
$pdf->Cell(4,10,':',1,0,'L');
$pdf->Cell(70,10,$r['nama'],1,0,'L');

$pdf->Ln();
$pdf->Cell(50,10,'Tempat Lahir',1,0,'L');
$pdf->Cell(4,10,':',1,0,'L');
$pdf->Cell(70,10,$r['tempat'],1,0,'L');

$pdf->Ln();
$pdf->Cell(50,10,'Tanggal Lahir',1,0,'L');
$pdf->Cell(4,10,':',1,0,'L');
$pdf->Cell(70,10,$lahir ,1,0,'L');

$pdf->Ln();
$pdf->Cell(50,10,'Jenis Kelamin',1,0,'L');
$pdf->Cell(4,10,':',1,0,'L');
$pdf->Cell(70,10,$r['jenis_kelamin'],1,0,'L');

$pdf->Ln();
$pdf->Cell(50,10,'Agama',1,0,'L');
$pdf->Cell(4,10,':',1,0,'L');
$pdf->Cell(70,10,$r['agama'],1,0,'L');


$pdf->Ln();
$pdf->Cell(50,10,'Email',1,0,'L');
$pdf->Cell(4,10,':',1,0,'L');
$pdf->Cell(70,10,$r['email'],1,0,'L');

$pdf->Ln();
$pdf->Cell(50,10,'Alamat',1,0,'L');
$pdf->Cell(4,10,':',1,0,'L');
$pdf->Cell(70,10,$r['alamat'],1,0,'L');

$pdf->Ln();
$pdf->Cell(50,10,'Nama Wali',1,0,'L');
$pdf->Cell(4,10,':',1,0,'L');
$pdf->Cell(70,10,$r['wali'],1,0,'L');

$pdf->Ln();
$pdf->Cell(50,10,'No Telp',1,0,'L');
$pdf->Cell(4,10,':',1,0,'L');
$pdf->Cell(70,10,$r['telp'],1,0,'L');
$pdf->Ln(9);
$pdf->Text(10,200,'Dicetak pada tanggal: ' . date( 'd-m-Y, H:i:s'),1,0,'L');
}

$pdf->Output();
?>
